# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")
# Question 1: Telegram: "Hail Zalando!!" --> Date: 30th Aug 1 AM


def solution(A):
    # write your code in Python 3.6
    size = len(A)
    running_sum = 0
    counter = 0
    for i in range(size):
        expected = ((i + 1) * (i + 2)) / 2
        running_sum += A[i]
        if running_sum == expected:
            counter += 1
    return counter

"""
Problem one: Trains
"""


def get_inputs():
    """Read inputs_trains.txt file and return array of inputs."""
    inputs = []
    with open('inputs_trains.txt', 'r') as file_obj:
        inputs = file_obj.read().split('\n')
    return inputs


class Graph(object):
    """
    Graph object with nodes and edges.
    E.g., (N)--[E]-->(N)
    """

    # To store edges between 2 nodes. Structure would be:
    # edge_to = {
    #   'NODE': {
    #       'CONNECTED_TO_NODE_1': DISTANCE_1,
    #       'CONNECTED_TO_NODE_2': DISTANCE_2
    #   }
    # }
    edge_to = {}

    def add_edge(self, node_1, node_2, distance):
        """
        Add edge between given 2 nodes along with given distance.
        Args:
            node_1: String, node name.
            node_2: String, another node name.
            distance: Int, distance between node_1 and node_2.
        """
        if not self.edge_to.get(node_1):
            self.edge_to[node_1] = {}
        self.edge_to[node_1][node_2] = distance

    def get_distance(self, route):
        """
        Return distance of given route.
        Args:
            route: String, route in the format A-B-C.
        Returns:
            Return distance (int) of given route.
        """
        nodes = route.split('-')
        last_node = nodes[0]
        distance = 0
        for node in nodes[1:]:
            node_distance = self.edge_to.get(last_node, {}).get(node)
            if node_distance:
                distance += node_distance
                last_node = node
                continue
            return 'NO SUCH ROUTE'
        return distance

    def get_maxstops_trips(self, start_node, end_node, max_stops, stops=1):
        """
        Recursive function to find number of available trips between given
        start and end destination within given maximum stops.
        Args:
            start_node: String, Start of node e.g, A
            end_node: String, End of node e.g, B
            max_stops: Int, to find available trips within maximum stops.
            stops: Int, This is recursive function, to track the stops or
                levels.
        Returns:
            Return number of available trips (int).
        """
        # Use DFS to go deeper to the graph and stop when you find your
        # destination within maximum stops.
        if stops > max_stops:
            return 0
        trips = 0
        for node in self.edge_to.get(start_node, {}).keys():
            # Found a match, increment the number of trips.
            if node == end_node:
                trips += 1
            trips += self.get_maxstops_trips(
                node, end_node, max_stops, stops + 1)
        return trips

    def get_exactstops_trips(
            self, start_node, end_node, exact_stops, stops=0):
        """
        Recursive function to find number of available trips between given
        start and end destination in given exact stops.
        Args:
            start_node: String, Start of node e.g, A
            end_node: String, End of node e.g, B
            exact_stops: Int, to find available trips in given exact stops.
            stops: Int, This is recursive function, to track the stops or
                levels.
        Returns:
            Return number of available trips (int).
        """
        # Use DFS to go deeper to the graph and stop when you find your
        # destination in given exact stops
        if stops == exact_stops:
            return int(start_node == end_node)

        # If it is exceeding given exact stops, stop and return.
        if stops > exact_stops:
            return 0
        trips = 0
        for node in self.edge_to.get(start_node, {}).keys():
            trips += self.get_exactstops_trips(
                node, end_node, exact_stops, stops + 1)
        return trips

    def get_available_trips_num(
            self, start_node, end_node, max_distance, distance=0):
        """
        Recursive function to find number of available trips between given
        start and end destination within given maximum distance.
        Args:
            start_node: String, Start of node e.g, A
            end_node: String, End of node e.g, B
            max_distance: Int, to find available trips within maximum distance.
            distance: Int, This is recursive function, to track the distance.
        Returns:
            Return number of available trips (int).
        """
        # Use DFS to go deeper to the graphs and stop when you find your
        # destination in given maximum distance.
        trips = 0
        for node, dist in self.edge_to.get(start_node, {}).items():
            # If distance is exceeding given maximum distance, stop and return.
            if distance + dist >= max_distance:
                return 0
            # Found a node, increment trip counter.
            if node == end_node:
                trips += 1
            trips += self.get_available_trips_num(
                node, end_node, max_distance, distance + dist)
        return trips

    def get_shortest_distance(self, start_node, end_node):
        """
        Find the shortest distance between given 2 nodes. Use BFS to normalize
        all routes based on given nodes.
        Args:
            start_node: String, Start of node e.g, A
            end_node: String, End of node e.g, B
        Returns:
            Return shortest distance (int) if route found,
            else 'NO SUCH ROUTE'.
        """
        result = {}
        queue = [start_node]
        while len(queue):
            # Take first node from queue and process it.
            temp_node = queue.pop(0)

            # Loop through child nodes and update with shortest distance.
            children = self.edge_to[temp_node]
            for child_node, distance in children.items():
                temp_distance = distance + result.get(temp_node, 0)

                # If current child_node is already updated with optimized
                # distance or current distance is greater than new distance,
                # in both cases update new distance for child_node.
                if (result.get(child_node) is None or
                        result.get(child_node) > temp_distance):
                    result[child_node] = temp_distance

                    # If child_node already in queue, ignore else add it.
                    if child_node not in queue:
                        queue.append(child_node)

        # Result obj will hold node as key and distance as value.
        # If you find end_node in result obj, return distance else, no route
        # possible from start node to end node.
        return result.get(end_node) or 'NO SUCH ROUTE'


def lets_go(inputs):
    """
    Parse given inputs, prepare graph and find answers to all 10 questions.
    Args:
        inputs: List of input e.g, AB5
    """
    graph = Graph()
    for inp in inputs:
        if len(inp) < 3:
            continue
        node_1, node_2, distance = (inp[0], inp[1], int(inp[2:]))
        graph.add_edge(node_1, node_2, distance)
    print graph.get_distance('A-B-C')
    print graph.get_distance('A-D')
    print graph.get_distance('A-D-C')
    print graph.get_distance('A-E-B-C-D')
    print graph.get_distance('A-E-D')
    print graph.get_maxstops_trips('C', 'C', 3)
    print graph.get_exactstops_trips('A', 'C', 4)
    print graph.get_shortest_distance('B', 'B')
    print graph.get_available_trips_num('C', 'C', 30)


if __name__ == "__main__":
    lets_go(get_inputs())

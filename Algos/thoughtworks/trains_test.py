"""
Unit test for trains.py
"""
import unittest
import trains


class TrainTest(unittest.TestCase):

    """Unittest for all available method in train.Graph"""

    def setUp(self):
        """Setup Graph object to reuse in all available tests."""
        self.graph = trains.Graph()
        self.graph.add_edge('A', 'B', 10)
        self.graph.add_edge('B', 'C', 5)
        self.graph.add_edge('A', 'C', 4)
        self.graph.add_edge('C', 'B', 20)

    def test_get_distance(self):
        self.assertEqual(self.graph.get_distance('A-B-C'), 15)

    def test_get_maxstops_trips(self):
        self.assertEqual(self.graph.get_maxstops_trips('B', 'B', 4), 2)

    def test_get_exactstops_trips(self):
        self.assertEqual(self.graph.get_exactstops_trips('C', 'C', 4), 1)

    def test_get_shortest_distance(self):
        self.assertEqual(self.graph.get_shortest_distance('A', 'C'), 4)

    def test_get_available_trips_num(self):
        self.assertEqual(self.graph.get_available_trips_num('A', 'C', 40), 3)

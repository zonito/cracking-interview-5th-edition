
from typing import List


class Solution:
    def binarySearch(self, arr: List[int], target: int) -> int:
        if len(arr) == 0:
            return -1

        start = 0
        end = len(arr) - 1

        while start <= end:
            mid = (start + end) // 2
            if arr[mid] == target:
                return mid
            elif arr[mid] < target:
                start = mid + 1
            else:
                end = mid - 1

        return -1


import logging
import functools


class Retry(object):
    """retry for fail cases"""

    times = 4

    def __call__(self, func):
        """A Decorator method."""

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            i = 1
            while i <= times:
                try:
                    return func(*args, **kwargs)
                except HTTPException as http_error:
                    logging.error(http_error)
                time.sleep(2 ** i)
                i += 1
        return wrapper


def retry(func):
    """A Decorator method."""

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        while True:
            try:
                return func(*args, **kwargs)
            except HTTPException as http_error:
                logging.error(http_error)
    return wrapper


@retry
def get_foo(identifier):
    # makes an HTTP request
    # occassionally fails with an HTTPException

    # UserID <- Profile
    # Return from cache (expires in 5 minutes)
    return ...

get_foo = retry(get_foo)

# get_foo("spam") -> raises HTTPException
# get_foo("spam") -> return some result

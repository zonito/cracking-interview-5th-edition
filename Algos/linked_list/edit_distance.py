"""
Check if edit distance between two strings is one
An edit between two strings is one of the following changes.

Add a character
Delete a character
Change a character
Given two string s1 and s2, find if s1 can be converted to s2 with exactly one
edit. Expected time complexity is O(m+n) where m and n are lengths of two
strings.

Examples:

Input:  s1 = "geeks", s2 = "geek"
Output: yes
Number of edits is 1

Input:  s1 = "geeks", s2 = "geeks"
Output: no
Number of edits is 0

Input:  s1 = "geaks", s2 = "geeks"
Output: yes
Number of edits is 1

Input:  s1 = "peaks", s2 = "geeks"
Output: no
Number of edits is 2
"""


def get_distance(string_1, string_2, distance):
    """Return True if distance between 2 strings is <= given distance."""
    len_1 = len(string_1)
    len_2 = len(string_2)
    i = 0
    j = 0
    diff = 0
    while i < len_1 and j < len_2:
        if string_1[i] != string_2[j]:
            diff += 1
            if len_1 > len_2:
                i += 1
            elif len_1 < len_2:
                j += 1
            else:
                i += 1
                j += 1
            if diff > distance:
                return diff
        else:
            i += 1
            j += 1
    if i < len_1 or j < len_2:
        diff += 1
    return diff


def show(string_1, string_2, distance):
    """Show required output."""
    diff = get_distance(string_1, string_2, distance)
    if diff != distance:
        print "no"
    else:
        print "yes"
    print "Number of edits is %d" % diff

show('geeks', 'geek', 1)
show('geeks', 'geeks', 1)
show('geaks', 'geeks', 1)
show('peaks', 'geek', 1)
show('love', 'lve', 1)

"""
https://leetcode.com/problems/add-two-numbers
"""

# Definition for singly-linked list.


class ListNode(object):

    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):

    linked_list = None

    def append_to_ll(self, data):
        if not self.linked_list:
            self.linked_list = ListNode(data)
            return
        current = self.linked_list
        while current.next:
            current = current.next
        current.next = ListNode(data)
        return

    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        remainder = 0
        while l1 or l2:
            l1_val = l1.val if l1 else 0
            l2_val = l2.val if l2 else 0
            total = l1_val + l2_val + remainder
            remainder = 0
            if total > 9:
                total = total % 10
                remainder = 1
            self.append_to_ll(total)
            l1 = l1.next if l1 else None
            l2 = l2.next if l2 else None
        if remainder:
            self.append_to_ll(remainder)
        return self.linked_list

LIST1 = ListNode(2)
LIST1.next = ListNode(4)
LIST1.next.next = ListNode(3)
LIST2 = ListNode(5)
LIST2.next = ListNode(6)
LIST2.next.next = ListNode(4)

LIST1 = ListNode(5)
LIST2 = ListNode(5)
RESULT = Solution().addTwoNumbers(LIST1, LIST2)
while RESULT:
    print RESULT.val
    RESULT = RESULT.next

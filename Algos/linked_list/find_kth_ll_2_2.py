"""
Implement an algorithm to find the kth to last element of a singly linked list
"""
import basic_linked_list as ll


def find_last_kth(head, kth_index):
    """Return kth data from last in the singly linked list."""
    total_nodes = ll.Node.length(head)
    if total_nodes < kth_index:
        return None
    counter = 0
    index = total_nodes - kth_index
    while head.next_node:
        if counter == index:
            return head.data
        counter += 1
        head = head.next_node
    return None


def find_last_kth_recur(head, kth_index):
    """Recursive approach."""
    if not head:
        return 0
    i = find_last_kth_recur(head.next_node, kth_index) + 1
    if i == kth_index:
        print head.data
    return i


def find_last_kth_runner(head, kth_index):
    """Return kth data from last using 2 runner pointer method."""
    runner_1 = head
    runner_2 = head
    for _ in range(kth_index - 1):
        if not runner_1:
            return
        runner_1 = runner_1.next_node
    while runner_1.next_node:
        runner_1 = runner_1.next_node
        if runner_1:
            runner_2 = runner_2.next_node
    return runner_2.data

NODE = ll.Node(1)
NODE.append_to_tail(2)
NODE.append_to_tail(3)
NODE.append_to_tail(4)
NODE.append_to_tail(5)

print find_last_kth(NODE, 3)
find_last_kth_recur(NODE, 3)
print find_last_kth_runner(NODE, 3)

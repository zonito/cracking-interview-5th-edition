"""
Implement a function to check if a linked list is a palindrome.
"""

import basic_linked_list


def is_palindrome(head):
    """Return True, if given linked list is a palindrome, otherwise False."""
    basic_linked_list.Node.display(head)
    if not head or not head.next_node:
        return True
    first_value = head.data
    last_value = head.next_node.data
    shrink_head = basic_linked_list.Node(head.next_node.data)
    head = head.next_node
    if head.next_node:
        while head.next_node.next_node:
            head = head.next_node
            shrink_head.append_to_tail(head.data)
        last_value = head.next_node.data
        if first_value == last_value:
            return is_palindrome(shrink_head)
    return first_value == last_value


NODE = basic_linked_list.Node('n')
NODE.append_to_tail('a')
NODE.append_to_tail('m')
NODE.append_to_tail('a')
NODE.append_to_tail('n')
basic_linked_list.Node.display(NODE)
print is_palindrome(NODE)

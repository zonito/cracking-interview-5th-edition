function Node(element) {
  this.element = element;
  this.next = null;
}

function LList() {
  this.head = new Node('head');
  this.find = function (element) {
    var node = this.head;
    while (node) {
      if (node.element === element) {
        return true;
      }
      node = node.next;
    }
    return false;
  };
  this.insert = function (element, after) {
    var node = this.head,
      temp;
    while (node) {
      if (node.element === after) {
        temp = node.next;
        node.next = new Node(element);
        node.next.next = temp;
        return true;
      }
      node = node.next;
    }
    return false;
  };
  this.remove = function (element) {
    var node = this.head,
      prev = null;
    while (node) {
      if (node.element === element) {
        prev.next = node.next;
        return node.element;
      }
      prev = node;
      node = node.next;
    }
  };
  this.display = function () {
    var node = this.head;
    while (node) {
      console.log(node.element);
      node = node.next;
    }
  };
}

var llist = new LList();
llist.insert('love', 'head');
llist.insert('kumar', 'love');
llist.insert('sharma', 'kumar');
llist.display();
console.log(llist.remove('sharma'));
llist.display();
console.log(llist.find('love'));

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function genStuData(arr) {
  for (var i = 0; i < arr.length; ++i) {
    var num = "";
    for (var j = 1; j <= 9; ++j) {
      num += Math.floor(Math.random() * 10);
    }
    num += getRandomInt(50, 100);
    arr[i] = num;
  }
}

var numStudents = 10;
var arrSize = 97;
var idLen = 9;
var students = new Array(numStudents);
genStuData(students);
console.log("Student data: \n");
for (var i = 0; i < students.length; ++i) {
  console.log(students[i].substring(0, 8) + " " +
    students[i].substring(9));
}

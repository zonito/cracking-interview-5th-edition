"""
Write code to remove duplicates from an unsorted linked list.
FOLLOW UP
How would you solve this problem if a temporary buffer is not allowed?
"""

import basic_linked_list


def remove_duplicates(head):
    """Remove duplication from given linked list."""
    if not head:
        return
    node = head
    data_buffer = {node.data: True}
    while node.next_node:
        if data_buffer.get(node.next_node.data):
            node.next_node = node.next_node.next_node
        else:
            data_buffer[node.next_node.data] = True
            node = node.next_node

NODE = basic_linked_list.Node(1)
NODE.append_to_tail(2)
NODE.append_to_tail(3)
NODE.append_to_tail(2)
NODE.append_to_tail(5)
NODE.append_to_tail(6)
NODE.append_to_tail(2)
NODE.append_to_tail(7)
basic_linked_list.Node.display(NODE)
remove_duplicates(NODE)
basic_linked_list.Node.display(NODE)

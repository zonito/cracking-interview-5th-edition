"""Binary Tree implementation."""


class Node(object):
    """Node class"""
    data = None
    left = None
    count = 0
    right = None

    def __init__(self, data):
        self.data = data


class BTree(object):
    """Binary Tree implementation class."""
    root = None

    def insert(self, data):
        """Insert data in tree."""
        node = Node(data)
        if not self.root:
            self.root = node
            return
        root = self.root
        parent = None
        while root:
            parent = root
            if root.data == data:
                root.count += 1
                break
            if root.data > data:
                root = root.left
                if not root:
                    parent.left = node
                    break
            else:
                root = root.right
                if not root:
                    parent.right = node
                    break

    def in_order(self, root):
        """Inorder of binary tree. Left - Center - Right"""
        if root:
            self.in_order(root.left)
            print root.data
            self.in_order(root.right)

    def pre_order(self, root):
        """Preorder of binary tree. Center - Left - Right."""
        if root:
            print root.data
            self.pre_order(root.left)
            self.pre_order(root.right)

    def post_order(self, root):
        """Postorder of binary tree. Left - Right - Center."""
        if root:
            self.post_order(root.left)
            self.post_order(root.right)
            print root.data

    @staticmethod
    def get_min(root):
        """Return min value in tree."""
        minimum = root
        while root:
            minimum = root
            root = root.left
        return minimum

    def get_max(self):
        """Return max value from current tree."""
        root = self.root
        maxi = root.data
        while root:
            maxi = root.data
            root = root.right
        return maxi

    def find(self, data):
        """Return Node if given data found, else None"""
        root = self.root
        while root and root.data != data:
            if root.data > data:
                root = root.left
            else:
                root = root.right
        return root

    def display(self, root):
        """Show binary tree."""
        result = [None, None, None]
        if root:
            result[0] = root.data
            result[1] = self.display(root.left)
            result[2] = self.display(root.right)
        # print result, any()
        if any(result):
            return result
        return None

    def remove(self, data):
        """Remove given data."""
        if not self.find(data):
            return
        self.root = self.remove_node(self.root, data)

    def remove_node(self, node, data):
        """Remove data from given node."""
        if not node:
            return
        if data == node.data:
            if not node.left and not node.right:
                return
            if not node.left:
                return node.right
            if not node.right:
                return node.left
            temp = self.get_min(node.right)
            node.data = temp.data
            node.right = self.remove_node(node.right, temp.data)
        elif data < node.data:
            node.left = self.remove_node(node.left, data)
        else:
            node.right = self.remove_node(node.right, data)
        return node


BTREE = BTree()
BTREE.insert(23)
BTREE.insert(45)
BTREE.insert(16)
BTREE.insert(37)
BTREE.insert(3)
BTREE.insert(99)
BTREE.insert(22)
BTREE.in_order(BTREE.root)
BTREE.pre_order(BTREE.root)
BTREE.post_order(BTREE.root)
print BTREE.get_min(BTREE.root).data
print BTREE.get_max()
print BTREE.find(3)
BTREE.remove(3)
print BTREE.display(BTREE.root)
BTREE.remove(23)
print BTREE.display(BTREE.root)

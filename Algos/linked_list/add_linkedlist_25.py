"""
You have two numbers represented by a linked list, where each node contains a
single digit. The digits are stored in reverse order, such that the Ts digit is
at the head of the list. Write a function that adds the two numbers and returns
the sum as a linked list.
EXAMPLE
Input: (7-> 1 -> 6) + (5 -> 9 -> 2).That is, 617 + 295.
Output: 2 -> 1 -> 9.That is, 912.
FOLLOW UP
Suppose the digits are stored in forward order. Repeat the above problem.
EXAMPLE
Input: (6 -> 1 -> 7) + (2 -> 9 -> 5).That is, 617 + 295.
Output: 9 -> 1 -> 2.That is, 912.
"""

import basic_linked_list


def add_lnk_list(first_list, second_list):
    """
    Return resultant linked list which is addition of given two linked lists.
    """
    if not first_list:
        return second_list
    if not second_list:
        return first_list
    remainder = 0
    result = None
    while first_list or second_list:
        first_value = 0
        if first_list:
            first_value = first_list.data
        second_value = 0
        if second_list:
            second_value = second_list.data

        # Addition
        sum_values = first_value + second_value + remainder
        remainder, value = (
            (sum_values // 10, sum_values % 10)
            if sum_values >= 10 else (0, sum_values))

        # Add it to result node
        if result:
            result.append_to_tail(value)
        else:
            result = basic_linked_list.Node(value)
        first_list = first_list.next_node
        second_list = second_list.next_node
    if remainder:
        result.append_to_tail(remainder)
    return result


def add_list_forward(first_list, second_list):
    """What if we have forwards number in given lists."""
    if not first_list or not second_list:
        return
    first_length = basic_linked_list.Node.length(first_list)
    second_length = basic_linked_list.Node.length(second_list)
    reverse_first = basic_linked_list.Node(first_list.data)
    reverse_second = basic_linked_list.Node(second_list.data)
    print first_length, second_length
    counter = 0
    while first_list.next_node or second_list.next_node:
        counter += 1
        if counter < first_length:
            first_list = first_list.next_node
            reverse_first = reverse_first.append_to_head(first_list.data)
        else:
            reverse_first.append_to_tail(0)
        # Update second reverse list
        if counter < second_length:
            second_list = second_list.next_node
            reverse_second = reverse_second.append_to_head(second_list.data)
        else:
            reverse_second.append_to_tail(0)
    # basic_linked_list.Node.display(reverse_first)
    # basic_linked_list.Node.display(reverse_second)
    add_list = add_lnk_list(reverse_first, reverse_second)
    # basic_linked_list.Node.display(add_list)
    reverse = basic_linked_list.Node(add_list.data)
    while add_list.next_node:
        add_list = add_list.next_node
        reverse = reverse.append_to_head(add_list.data)
    return reverse

FIRST_LIST = basic_linked_list.add_some_values(5, True, special_number=9)
SECOND_LIST = basic_linked_list.add_some_values(5, True, special_number=9)
basic_linked_list.Node.display(FIRST_LIST)
basic_linked_list.Node.display(SECOND_LIST)
basic_linked_list.Node.display(add_lnk_list(FIRST_LIST, SECOND_LIST))
print '-' * 10
basic_linked_list.Node.display(add_list_forward(FIRST_LIST, SECOND_LIST))
SHORT_LIST = basic_linked_list.add_some_values(3, True, special_number=9)
basic_linked_list.Node.display(SHORT_LIST)
basic_linked_list.Node.display(add_list_forward(FIRST_LIST, SHORT_LIST))

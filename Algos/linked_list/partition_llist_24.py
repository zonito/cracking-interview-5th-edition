"""
Write code to partition a linked list around a value x, such that all nodes
less than x come before all nodes greater than or equal to x.
"""

import basic_linked_list


def partition(head, value):
    """Return linked list with properly partition from given value."""
    if not head or not head.next_node:
        return head
    first_half = None
    second_half = None
    node = head
    while node:
        if node.data < value:
            if first_half:
                first_half.append_to_tail(node.data)
            else:
                first_half = basic_linked_list.Node(node.data)
        else:
            if second_half:
                second_half.append_to_tail(node.data)
            else:
                second_half = basic_linked_list.Node(node.data)
        node = node.next_node

    # Create empty linked list which will merge first and second linked list.
    merged_node = None
    while first_half:
        if merged_node:
            merged_node.append_to_tail(first_half.data)
        else:
            merged_node = basic_linked_list.Node(first_half.data)
        first_half = first_half.next_node

    # Merge second half in that same linked list.
    while second_half:
        if merged_node:
            merged_node.append_to_tail(second_half.data)
        else:
            merged_node = basic_linked_list.Node(second_half.data)
        second_half = second_half.next_node
    return merged_node

NODE = basic_linked_list.add_some_values(10, True)
basic_linked_list.Node.display(NODE)
basic_linked_list.Node.display(partition(NODE, 5))

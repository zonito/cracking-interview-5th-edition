"""
Implement an algorithm to delete a node in the middle of a singly linked
list, given only access to that node.
EXAMPLE
Input: the node c from the linked list a->b->c->d->e
Result: nothing is returned, but the new linked list looks like a- >b- >d->e
"""

import basic_linked_list as ll


def delete_middle(head):
    """Delete node from the middle of given linked list."""
    first_counter = 0
    node = head
    operating_node = None
    while node.next_node:
        first_counter += 1
        if first_counter % 2 == 0:
            if not operating_node:
                operating_node = head
            else:
                operating_node = operating_node.next_node
        node = node.next_node
    operating_node.next_node = operating_node.next_node.next_node
    return head

NODE = ll.add_some_values(11)
ll.Node.display(NODE)
ll.Node.display(delete_middle(NODE))

# In this problem, you are not given access to the head of the linked list. You only have
# access to that node. The solution is simply to copy the data from the next node over to
# the current node, and then to delete the next node.
# The code below implements this algorithm.

# public static boolean deleteNode(LinkedListNode n) {
#     if (n == null || n.next == null) {
#         return false; // Failure
#     }
#     LinkedListNode next = n.next;
#     n.data = next.data;
#     n.next = next.next;
#     return true;
# }

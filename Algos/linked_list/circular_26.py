"""
Given a circular linked list, implement an algorithm which returns the node at
the beginning of the loop.
DEFINITION
Circular linked list: A (corrupt) linked list in which a node's next pointer
points to an earlier node, so as to make a loop in the linked list.
EXAMPLE
Input: A - > B - > C - > D - > E - > C [the same C as earlier]
Output: C
"""

import basic_linked_list


def find_beginning(head):
    """Return beginning of the given circular list."""
    slow_runner = head
    fast_runner = head
    while fast_runner.next_node:
        # print slow_runner.data, fast_runner.data
        slow_runner = slow_runner.next_node
        fast_runner = fast_runner.next_node.next_node
        if slow_runner == fast_runner:
            break
    # print 'collision: ', slow_runner.data, fast_runner.data
    if not fast_runner:
        return None
    slow_runner = head
    while slow_runner != fast_runner:
        # print slow_runner.data, fast_runner.data
        slow_runner = slow_runner.next_node
        fast_runner = fast_runner.next_node
    return fast_runner

NODE = basic_linked_list.Node('A')
NODE.append_to_tail('B')
NODE.append_to_tail('C')
NODE.append_to_tail('D')
NODE.append_to_tail('E')
NODE.append_to_tail('C')
NODE.append_to_tail('D')


def make_circular(head):
    """Make Circular."""
    node = head
    while node.next_node:
        node = node.next_node
    node.next_node = head

make_circular(NODE)
FORW = basic_linked_list.Node('1')
FORW.append_to_tail('2')
FORW.append_to_tail('3')
TEMP = FORW
while TEMP.next_node:
    TEMP = TEMP.next_node
TEMP.next_node = NODE

# basic_linked_list.Node.display(FORW, 10)
print find_beginning(FORW).data

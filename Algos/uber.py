# This is the text editor interface.
# Anything you type or change here will be seen by the other person in
# real time.

"""
L-systems are used to model naturally occurring patterns in nature such as growth of trees, growth of multicellular organisms, fractals etc. by recursively applying a set of simple grammar rules over a set of variables starting with an Axiom.

For example: Growth of Algae in L-systems would be modelled as.
variables : A B
constants : none
axiom  : A {starting character string}
rules  : (A → AB), (B → A)

which produces:
n = 0 : S=A
n = 1 : S=AB
n = 2 : S=ABA
n = 3 : S=ABAAB
n = 4 : S=ABAABABA
n = 5 : S=ABAABABAABAAB

Given the upper bound ‘n’, code a class D for L-systems with variables, axiom and rules. Write a function ‘produce’ that takes ‘D’ and ‘n’ and returns the L-systems string ‘S’. How can you optimize your L-system?

Test it using the following L-system (Cantor fractal) where n=3:

variables : A B
constants : none
start  : A
rules  : (A → ABA), (B → BBB)

Space = (No of Variables)^n
Time = n*(no of variables)
"""


def get_L_system_str(variables, rules, axiom, n):
    """
    Return L System string from given details.
    rules = dictionary - Example: {'A': 'AB', 'B': 'A'}
    variables = list - Example: [A, B]
    axiom = string - Example: A
    """
    resultant_str = axiom
    for _ in range(n):
        temp_list = []
        # Linear
        for char in resultant_str:
            temp_list.append(rules[char])
        resultant_str = ''.join(temp_list)
    return resultant_str


def get_recurssive(resultant_str, length, rules):
    temp_list = []
    step = length - 1 if length > 1 else 1
    for i in range(0, len(resultant_str), step):
        if len(resultant_str) > i + step:
            string = resultant_str[i: i + step]
        else:
            # Last loop
            string = resultant_str[i:]
        if rules.get(string):
            temp_list.append(rules[string])
        else:
            temp_rule = get_recurssive(string, length - 1, rules)
            temp_list.extend(temp_rule[string])
    rules[resultant_str] = ''.join(temp_list)
    return rules


def get_L_system_str2(variables, rules, axiom, n):
    """
    Return L System string from given details.
    rules = dictionary - Example: {'A': 'AB', 'B': 'A'}
    variables = list - Example: [A, B]
    axiom = string - Example: A
    """
    resultant_str = axiom
    last_length = len(axiom)
    for _ in range(n):
        rules = get_recurssive(resultant_str, last_length, rules)
        last_length = len(resultant_str)
        resultant_str = rules[resultant_str]
    return resultant_str


print get_L_system_str(['A', 'B'], {'A': 'AB', 'B': 'A'}, 'A', 5)
print get_L_system_str2(['A', 'B'], {'A': 'AB', 'B': 'A'}, 'A', 5)
print get_L_system_str2(['A', 'B'], {'A': 'ABA', 'B': 'BBB'}, 'A', 3)

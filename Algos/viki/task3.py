def solution(n):
    d = [0] * 30
    l = 0
    while (n > 0):
        d[l] = n % 2
        n //= 2
        l += 1
    print d, len(d)
    print l
    # d = 'codilitycodilityco'
    # l = len(d)
    # d = 'abracadabracadabra'
    # l = len(d)
    for p in range(1, 1 + l // 2):
        ok = True
        for i in range(l - p):
            print p, i, i + p, d[i], d[i + p], d[i] != d[i + p]
            if d[i] != d[i + p]:
                ok = False
                break
        if ok:
            return p
    return -1

# print solution(955)
# print solution(1651)
# print solution(253)
print solution(1000000000)

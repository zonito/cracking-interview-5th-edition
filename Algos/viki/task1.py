
# Result:
# https://www.dropbox.com/s/12vsssy87mmirg0/Screenshot%202019-04-09%2001.01.20.png?dl=0
# https://app.codility.com/c/feedback/GC3SQW-5DD/

# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")


def solution(A):
    # write your code in Python 3.6
    if not A:
        return 0
    face_dict = {}
    pairs_stats = {}
    pairs = {1: 6, 2: 5, 3: 4, 4: 3, 5: 2, 6: 1}
    pairs_num = {1: 1, 2: 2, 3: 3, 4: 3, 5: 2, 6: 1}
    for face in A:
        face_dict.setdefault(face, 0)
        face_dict[face] += 1
    for face in face_dict:
        total = face_dict.get(pairs[face], 0) + face_dict[face]
        moves = 0
        if not face_dict.get(pairs[face], 0):
            pairs_stats[pairs_num[face]] = dict(total=total, moves=moves)
            continue
        if face_dict[pairs[face]] > face_dict[face]:
            pairs_stats[pairs_num[face]] = dict(
                total=total, moves=face_dict[face] * 2)
            continue
        pairs_stats[pairs_num[face]] = dict(
            total=total, moves=face_dict[pairs[face]] * 2)
    # print pairs_stats, face_dict
    min_moves = len(A) + 1
    for pair in pairs_stats:
        moves = pairs_stats[pair]['moves']
        for other_pair in pairs_stats:
            if other_pair == pair:
                continue
            moves += pairs_stats[other_pair]['total']
        if moves < min_moves:
            min_moves = moves
    return min_moves

print solution([1, 2, 3])
print solution([1, 1, 6])
print solution([1, 6, 2, 3])
print solution([1, 6, 2, 5, 2, 5, 3, 4, 3, 4, 3, 4, 3, 4, 2, 5, 6, 1, 3, 3, 3, 3])



# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(A):
    # write your code in Python 3.6
    if not A:
        return 0
    sorted_ranks = sorted(A)
    soldiers = 0
    last_rank = sorted_ranks[0]
    temp_soldiers = 1
    for rank in sorted_ranks[1:]:
        if rank == last_rank:
            temp_soldiers += 1
            continue
        if last_rank + 1 == rank:
            soldiers += temp_soldiers
        # Reset
        last_rank = rank
        temp_soldiers = 1
    return soldiers

print solution([3, 4, 3, 0, 2, 2, 3, 0, 0])
print solution([4, 2, 0])
print solution([4, 4, 3, 3, 1, 0])
print solution([3, 4, 3, 3, 3, 5, 6, 7, 0, 2, 2, 3, 0, 0, 1000000000])

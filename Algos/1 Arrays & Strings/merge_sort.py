
import random


class Sorting(object):

    def merge_sort(self, array, size=None):
        if not size:
            size = len(array)
        if size <= 1:
            return array
        mid_index = size / 2
        left = self.merge_sort(array[:mid_index])
        right = self.merge_sort(array[mid_index:])
        result = []
        left_counter = right_counter = 0
        ln_left = len(left)
        ln_right = len(right)
        while left_counter < ln_left and right_counter < ln_right:
            if left[left_counter] < right[right_counter]:
                result.append(left[left_counter])
                left_counter += 1
            else:
                result.append(right[right_counter])
                right_counter += 1
        if left_counter == ln_left:
            result += right[right_counter:]
        elif right_counter == ln_right:
            result += left[left_counter:]
        return result

    def quick_sort(self, array):
        if len(array) <= 1:
            return array
        pivot = array[-1]
        greater = []
        smaller = []
        for val in array[:-1]:
            if val > pivot:
                greater.append(val)
            else:
                smaller.append(val)
        return self.quick_sort(smaller) + [pivot] + self.quick_sort(greater)

# _ARR = range(1000)
# random.shuffle(_ARR)
# # print _ARR
# Sorting().quick_sort(_ARR)
# Sorting().merge_sort(_ARR)
print Sorting().quick_sort([4, 2, 4, 6, 1, 0, 6, 8, 9, 7])
print Sorting().merge_sort([4, 2, 4, 6, 1, 0, 6, 8, 9, 7])
print Sorting().merge_sort([4, 2, 4])
print Sorting().merge_sort([4, 2, -4])
print Sorting().merge_sort([4, 2])
print Sorting().merge_sort([4])
print Sorting().merge_sort([])

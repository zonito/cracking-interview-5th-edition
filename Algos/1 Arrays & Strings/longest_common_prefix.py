class Solution(object):

    def longestCommonPrefix(self, strs):
        """
        :type strs: List[str]
        :rtype: str
        """
        if not strs:
            return ''
        min_ln = min([len(s) for s in strs])
        output = []
        for i in range(min_ln):
            char = strs[0][i]
            status = True
            for word in strs[1:]:
                if word[i] != char:
                    status = False
                    break
            if not status:
                break
            output.append(char)
        return ''.join(output)

print Solution().longestCommonPrefix(['flower', 'flow', 'flight'])
print Solution().longestCommonPrefix([])

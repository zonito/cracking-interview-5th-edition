"""
The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)

P   A   H   N
A P L S I I G
Y   I   R
And then read line by line: "PAHNAPLSIIGYIR"

Write the code that will take a string and make this conversion given a number of rows:

string convert(string s, int numRows);
Example 1:

Input: s = "PAYPALISHIRING", numRows = 3
Output: "PAHNAPLSIIGYIR"
Example 2:

Input: s = "PAYPALISHIRING", numRows = 4
Output: "PINALSIGYAHRPI"
Explanation:

P     I    N
A   L S  I G
Y A   H R
P     I
"""


class Solution(object):

    def convert(self, s, numRows):
        """
        :type s: str
        :type numRows: int
        :rtype: str
        """
        if numRows <= 1 or len(s) <= 1:
            return s
        matrix = []
        for _ in range(numRows):
            matrix.append('')
        row_index = 0
        is_reverse = True
        for char in s:
            matrix[row_index] += char
            if row_index == numRows - 1 or row_index == 0:
                is_reverse = not is_reverse
            row_index += -1 if is_reverse else 1
        converted_string = ''
        for row_index in range(numRows):
            converted_string += matrix[row_index]
        return ''.join(converted_string)


print(Solution().convert('PAYPALISHIRING', 3))
print(Solution().convert('PAYPALISHIRING', 4))
print(Solution().convert('A', 1))
print(Solution().convert('A', 2))

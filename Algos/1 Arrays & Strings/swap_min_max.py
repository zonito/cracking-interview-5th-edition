"""Swapping minimum and maximum value of an array."""


def get_min_index(arr):
    """Return index of minimum value from given array."""
    min_index = 0
    for index, value in enumerate(arr):
        if value < arr[min_index]:
            min_index = index
    return min_index


def get_max_index(arr):
    """Return index of maximum value from given array."""
    max_index = 0
    for index, value in enumerate(arr):
        if value > arr[max_index]:
            max_index = index
    return max_index


def swap_elements(arr, min_index, max_index):
    """Swap value from given min and max index in given array."""
    temp = arr[min_index]
    arr[min_index] = arr[max_index]
    arr[max_index] = temp


def swap_min_max(arr):
    """Return swapped array."""
    if len(arr) < 2:
        return arr
    min_index = get_min_index(arr)
    max_index = get_max_index(arr)
    swap_elements(arr, min_index, max_index)
    return arr


print swap_min_max([3, 2, 1, 5, 4]) == [3, 2, 5, 1, 4]

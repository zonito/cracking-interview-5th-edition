"""
https://leetcode.com/problems/median-of-two-sorted-arrays
"""


class Solution(object):

    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """
        result = []
        len1 = len(nums1)
        len2 = len(nums2)
        total_len = len1 + len2
        is_odd = total_len % 2
        index1 = index2 = 0
        half_total = total_len / 2 or total_len
        while index1 < len1 and index2 < len2:
            if nums1[index1] <= nums2[index2]:
                result.append(nums1[index1])
                index1 += 1
            else:
                result.append(nums2[index2])
                index2 += 1
            if index1 + index2 > half_total:
                break
        if index1 + index2 <= half_total:
            if index1 == len1:
                result += nums2[index2:half_total - index1 + index2 + 1]
            elif index2 == len2:
                result += nums1[index1:half_total - index2 + index1 + 1]
        result = result[:half_total + 1]
        if is_odd:
            return result[-1] * 1.0 if result else 0.0
        return (result[-1] + result[-2]) / 2.0 if len(result) >= 2 else 0.0

print Solution().findMedianSortedArrays([1, 3], [2])  # 2.0
print Solution().findMedianSortedArrays([1, 2], [3, 4])  # 2.5
print Solution().findMedianSortedArrays([], [1])  # 1.0
print Solution().findMedianSortedArrays([], [])  # 0.0
print Solution().findMedianSortedArrays([2], [])  # 2.0
print Solution().findMedianSortedArrays([3], [-2, -1])  # -1.0
print Solution().findMedianSortedArrays(
    [1, 2, 3, 4, 10], [5, 6, 7, 8, 9])  # 5.5
print Solution().findMedianSortedArrays([2], [1, 3, 4])  # -2.5

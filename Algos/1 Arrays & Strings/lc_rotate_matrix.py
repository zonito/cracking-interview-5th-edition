from typing import List


def rotate_matrix(matrix: List[List[int]]) -> List[List[int]]:
    size = len(matrix)
    if size and len(matrix[0]) != size:
        return "Error"
    for layer in range(int(size / 2)):
        last = size - layer - 1  # 4 - 0 - 1 = 3,2
        for i in range(layer, last):
            # Layer = 0,1     i = 0,1,2,3,1
            offset = i - layer  # 1 -1 = 0
            temp = matrix[layer][i]  # 1,2,3,4,6
            matrix[layer][i] = matrix[last - offset][layer]  # 13,9,5,1,7
            # 16,15,14,13,11
            matrix[last - offset][layer] = matrix[last][last - offset]
            matrix[last][last - offset] = matrix[i][last]  # 4,8,12,16
            matrix[i][last] = temp
    return matrix


_MATRIX = [
    [1, 2, 3, 4, 5],
    [5, 6, 7, 8, 9],
    [9, 10, 11, 12, 13],
    [13, 14, 15, 16, 17],
    [17, 18, 19, 20, 21]
]
print(rotate_matrix(_MATRIX))

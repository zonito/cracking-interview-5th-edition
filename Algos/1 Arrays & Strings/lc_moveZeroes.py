class Solution(object):

    def moveZeroes(self, nums):
        """
        :type nums: List[int]
        :rtype: None Do not return anything, modify nums in-place instead.
        """
        ln_n = len(nums)
        for i in range(ln_n - 1):
            if nums[i] == 0 and nums[i + 1] != 0:
                k = i + 1
                while k != 0 and (nums[k - 1] == 0 or nums[k] == 0):
                    nums[k], nums[k - 1] = nums[k - 1], nums[k]
                    k -= 1
        return nums

print Solution().moveZeroes([0, 1, 0, 3, 12])

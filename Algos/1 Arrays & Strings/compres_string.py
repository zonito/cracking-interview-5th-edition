"""
Implement a method to perform basic string compression using the counts of
repeated characters. For example, the string aabcccccaaa would become
a2blc5a3. If the "compressed" string would not become smaller than the original
string, your method should return the original string.
"""


def compress(string):
    """Return compressed string, if not able to compress return original str."""
    if len(string) < 3:
        return string
    temp_char = string[0]
    total = 1
    result = [temp_char]
    for char in string[1:]:
        if char == temp_char:
            total += 1
        else:
            result.append(str(total))
            result.append(char)
            temp_char = char
            total = 1
    result.append(str(total))
    compressed_string = ''.join(result)
    return compressed_string if len(compressed_string) < len(string) else string

print compress("aabcccccaaa")
print compress("love")

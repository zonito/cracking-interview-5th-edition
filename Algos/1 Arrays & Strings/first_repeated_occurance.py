# from typing import List


def get_first_occurance(string: str) -> str:
    bit_int = 0
    for char in string:
        diff = ord(char) - 32
        if (bit_int & (1 << diff)) > 0:
            return char
        bit_int |= (1 << diff)
    return None


print(get_first_occurance("ABCA"))
print(get_first_occurance("LOVE SHARMA"))
print(get_first_occurance("HOW AREYOU?"))
print(get_first_occurance("ZONITO"))
print(get_first_occurance("XYZ"))


def get_first_non_occurance(string: str) -> str:
    bit_int = 0
    dub_bit_int = 0
    for char in string:
        diff = ord(char) - 32
        if (bit_int & (1 << diff)) > 0:
            if (dub_bit_int & (1 << diff)) == 0:
                dub_bit_int |= (1 << diff)
            continue
        bit_int |= (1 << diff)
    for char in string:
        diff = ord(char) - 32
        if dub_bit_int & (1 << diff) == 0:
            return char
    return None


print(get_first_non_occurance("ABCAA"))
print(get_first_non_occurance("LOVE SHARMA"))
print(get_first_non_occurance("HOW AREYOU?"))
print(get_first_non_occurance("ZONITOZ"))
print(get_first_non_occurance("XYZ"))

"""
Implement an algorithm to determine if a string has all unique characters.
What if you cannot use additional data structures?
"""


def is_unique_string(string):
    """Return True if string has all unique characters, otherwise False."""
    if len(string) > 256:
        return False
    char_dict = {}
    for char in string:
        if char_dict.get(char, None):
            return False
        char_dict[char] = True
    return True

print is_unique_string('love sharma')

# Python3 program to Rearrange
# an array so that arr[i] becomes
# arr[arr[i]]

# The function to rearrange an
# array in-place so that arr[i]
# becomes arr[arr[i]].

from typing import List


def rearrange(arr: List[int]) -> List[int]:
    # First step: Increase all values
    # by (arr[arr[i]] % size) * size
    size = len(arr)
    for i in range(0, size):
        # print('%s += (%s mod %s) * %s = %s' %
        #       (arr[i], arr[arr[i]], size, size, arr[i] +
        #        (arr[arr[i]] % size) * size))
        arr[i] = arr[i] + (arr[arr[i]] % size) * size
    # print(arr)
    # Second Step: Divide all values by size
    for i in range(0, size):
        arr[i] = int(arr[i] / size)
    return arr


print(rearrange([3, 2, 0, 1]))

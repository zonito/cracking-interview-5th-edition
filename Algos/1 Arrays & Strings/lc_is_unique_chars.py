

def is_unique_chars(string):
    ord_a = ord('a')
    checker = 0
    for char in string:
        val = ord(char) - ord_a
        # print val, 1 << val, checker, checker & (1 << val), char
        if checker & (1 << val) > 0:
            return False
        checker |= (1 << val)
    return True

print is_unique_chars('loove')


def single_number(nums):
    store = 0
    for num in nums:
        val = store & num
        # print num, store, val
        if val:
            store ^= num
            # print 'in: ', store
            continue
        store |= num
        # print 'out: ', store
    return store

print single_number([2, 2, 1])
print single_number([4, 1, 2, 1, 2, 3])
print single_number([4, 1, 3, 1, 3, 5])

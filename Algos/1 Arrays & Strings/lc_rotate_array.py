"""
  Rotate Array

Given an array, rotate the array to the right by k steps, where k is non-negative.

Example 1:

Input: [1,2,3,4,5,6,7] and k = 3
Output: [5,6,7,1,2,3,4]
Explanation:
rotate 1 steps to the right: [7,1,2,3,4,5,6]
rotate 2 steps to the right: [6,7,1,2,3,4,5]
rotate 3 steps to the right: [5,6,7,1,2,3,4]
Example 2:

Input: [-1,-100,3,99] and k = 2
Output: [3,99,-1,-100]
Explanation:
rotate 1 steps to the right: [99,-1,-100,3]
rotate 2 steps to the right: [3,99,-1,-100]
Note:

Try to come up as many solutions as you can, there are at least 3 different
ways to solve this problem.
Could you do it in-place with O(1) extra space?
"""


class Solution(object):

    def rotate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: None Do not return anything, modify nums in-place instead.
        """
        n_len = len(nums)
        for _ in range(k):
            temp = nums[-1]
            for index in range(n_len - 1, 0, -1):
                nums[index] = nums[index - 1]
            nums[0] = temp
        return nums

    def rotate_new(self, nums, k):
        k = k % len(nums)
        if k == 0:
            return
        nums[:k], nums[k:] = nums[-k:], nums[:-k]


print (Solution().rotate([1, 2, 3, 4, 5, 6, 7], 3))
print (Solution().rotate([-1, -100, 3, 99], 2))
print (Solution().rotate([1, 2], 3))


def left_rotate(arr, N):
    if not arr:
        return arr

    L = len(arr)
    rotated_array = []
    for i in range(N, N + L):
        rotated_array.append(arr[i % L])
    return rotated_array


def left_rotate(arr, N):
    if not arr:
        return arr
    return [arr[i % len(arr)] for i in range(N, N + len(arr))]

print (left_rotate([1, 2, 3, 4, 5, 6, 7], 3))
print (left_rotate([-1, -100, 3, 99], 2))
print (left_rotate([1, 2], 3))

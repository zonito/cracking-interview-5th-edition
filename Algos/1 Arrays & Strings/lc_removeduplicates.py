class Solution(object):

    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        unique_nums = 0
        last_num = None
        i = 0
        len_n = len(nums)
        while True:
            if i == len_n:
                break
            num = nums[i]
            if num != last_num:
                unique_nums += 1
                last_num = num
                i += 1
                continue
            len_n -= 1
            nums.pop(i)
        return unique_nums

print Solution().removeDuplicates([1, 1, 2])
print Solution().removeDuplicates([0, 0, 1, 1, 1, 2, 2, 3, 3, 4])

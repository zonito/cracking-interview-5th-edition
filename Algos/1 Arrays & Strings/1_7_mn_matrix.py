"""
Write an algorithm such that if an element in an MxN matrix is 0, its entire row
and column are set to 0.
"""


def find_and_set_zero(matrix):
    """
    Return the resultant matrix after finding zero and making its row and
    column zero.
    """
    n_length = len(matrix)
    if not n_length:
        return matrix
    m_length = len(matrix[0])
    new_matrix = [[None for _ in range(m_length)]
                  for _ in range(n_length)]
    for i in range(n_length):
        for j in range(m_length):
            if new_matrix[i][j] != 0:
                new_matrix[i][j] = matrix[i][j]
            if matrix[i][j] == 0:
                for k in range(n_length):
                    new_matrix[i][k] = 0
                for k in range(m_length):
                    new_matrix[k][j] = 0
    return new_matrix


def find_and_set_zero_2(matrix):
    """The Second way."""
    n_length = len(matrix)
    if not n_length:
        return matrix
    m_length = len(matrix[0])
    rows = [None] * n_length
    columns = [None] * m_length
    for i in range(n_length):
        for j in range(m_length):
            if matrix[i][j] == 0:
                rows[i] = True
                columns[j] = True
    for i in range(n_length):
        for j in range(m_length):
            if rows[i] or columns[j]:
                matrix[i][j] = 0
    return matrix


_MATRIX = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 0, 12],
    [13, 14, 15, 16]
]
print find_and_set_zero(_MATRIX)
print find_and_set_zero_2(_MATRIX)

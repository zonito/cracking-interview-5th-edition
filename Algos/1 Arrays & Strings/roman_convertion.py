class Solution(object):

    def intToRoman(self, num):
        """
        :type num: int
        :rtype: str
        """
        roman_map = {1: 'I', 5: 'V', 10: 'X',
                     50: 'L', 100: 'C', 500: 'D', 1000: 'M'}
        if roman_map.get(num):
            return roman_map.get(num)
        stack = []
        temp = num
        counter = 0
        while temp > 0:
            rem = temp % 10
            zeros = 10 ** counter
            rank_num = rem * zeros
            temp /= 10
            if rem % 5 == 0:
                stack.append(roman_map.get(rank_num, ''))
            elif rem in [4, 9]:
                stack.append(roman_map.get(rank_num + zeros, ''))
                stack.append(roman_map.get(zeros, ''))
            else:
                stack += [roman_map.get(zeros, '')] * (rem % 5)
                if rem > 5:
                    stack.append(roman_map.get(
                        rank_num + ((5 - rem) * zeros), ''))
            counter += 1
        stack.reverse()
        return ''.join(stack)

    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
        roman_map = {'I': 1, 'V': 5, 'X': 10,
                     'L': 50, 'C': 100, 'D': 500, 'M': 1000}
        if roman_map.get(s):
            return roman_map.get(s)
        ln_s = len(s)
        i = 0
        output = 0
        while i < ln_s:
            char_num = roman_map.get(s[i])
            if i < ln_s - 1:
                next_char_num = roman_map.get(s[i + 1])
                if next_char_num > char_num:
                    output += next_char_num - char_num
                    i += 2
                    continue
            output += char_num
            i += 1
        return output

print Solution().intToRoman(3)
print Solution().intToRoman(4)
print Solution().intToRoman(9)
print Solution().intToRoman(58)
print Solution().intToRoman(1994)
for i in range(100):
    roman = Solution().intToRoman(i)
    inte = Solution().romanToInt(roman)
    print i, ' ---> ', roman, ' ---> ', inte
    assert i == inte

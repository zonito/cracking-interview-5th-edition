"""
Given two strings, write a method to decide if one is a permutation of the other.
"""


def merge_sort(string):
    """Sort given string and return sorted string. Use Merge Sort Algo."""
    length = len(string)
    if length < 2:
        return string
    elif length == 2:
        if ord(string[0]) > ord(string[1]):
            return string[1] + string[0]
        return string
    first_half = merge_sort(string[:length / 2])
    second_half = merge_sort(string[length / 2:])
    first_length = len(first_half)
    second_length = len(second_half)
    total = first_length + second_length
    result = [None] * total
    first_count = 0
    second_count = 0
    offset = 0
    for i in range(total):
        index = i + offset
        if first_count == first_length:
            for j in range(total - index):
                result[index + j] = second_half[second_count]
                second_count += 1
            break
        elif second_count == second_length:
            for j in range(total - index):
                result[index + j] = first_half[first_count]
                first_count += 1
            break
        else:
            if ord(first_half[first_count]) == ord(second_half[second_count]):
                result[index] = first_half[first_count]
                offset += 1
                result[index + 1] = second_half[second_count]
                first_count += 1
                second_count += 1
            elif ord(first_half[first_count]) > ord(second_half[second_count]):
                result[index] = second_half[second_count]
                second_count += 1
            elif ord(first_half[first_count]) < ord(second_half[second_count]):
                result[index] = first_half[first_count]
                first_count += 1
    return ''.join(result)


def is_permutation(string1, string2):
    """Return True, if string2 is a permutation of string1."""
    # Validate
    if len(string1) != len(string2):
        return False

    # Sort both strings
    sorted_str1 = merge_sort(string1)
    sorted_str2 = merge_sort(string2)

    # Compare and return
    return sorted_str1 == sorted_str2

print is_permutation("Love", "Loev")
# print merge_sort('Lovesharma')

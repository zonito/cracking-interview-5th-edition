class Solution(object):

    def maxArea(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        ln_h = len(height)
        max_area = 0
        left = 0
        right = ln_h - 1
        while left < right:
            max_area = max(
                min(height[left], height[right]) * (right - left), max_area)
            if height[left] < height[right]:
                left += 1
            else:
                right -= 1
        return max_area

print Solution().maxArea([1, 8, 6, 2, 5, 4, 8, 3, 7])

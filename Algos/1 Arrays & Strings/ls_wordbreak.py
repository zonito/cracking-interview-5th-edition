class Solution(object):

    def checkWord(self, s, word_hash):
        # print s
        s_ln = len(s)
        status = False
        counter = 1
        while counter < s_ln:
            sub_str = s[:counter]
            # print sub_str
            if word_hash.get(sub_str):
                if word_hash.get(s[counter:]):
                    status = True
                    break
                status = self.checkWord(s[counter:], word_hash)
                if status:
                    break
            counter += 1
        return status

    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: bool
        """
        word_hash = {}
        for word in wordDict:
            if s == word:
                return True
            word_hash[word] = True
        # print word_hash
        return self.checkWord(s, word_hash)

print Solution().wordBreak('leetcode', ['leet', 'code'])
print Solution().wordBreak('applepenapple', ['apple', 'pen'])
print Solution().wordBreak('iamace', ['i', 'a', 'am', 'ace'])
print Solution().wordBreak('a', ['a'])
print Solution().wordBreak('catsandog', ['cats', 'dog', 'sand', 'and', 'cat'])
# Infinite Loop
# print Solution().wordBreak(
#     'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab',
#     ['a', 'aa', 'aaa', 'aaaa', 'aaaaa', 'aaaaaa', 'aaaaaaa', 'aaaaaaaa', 'aaaaaaaaa', 'aaaaaaaaaa'])

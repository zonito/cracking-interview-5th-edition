def reverse(num: int) -> int:
    max_int = 1 << 31
    min_int = -1 << 31
    if num > max_int or num < min_int:
        return 0
    new_x = 0
    is_neg = num < 0
    if is_neg:
        num *= -1
    while num > 0:
        new_x = (new_x * 10) + (num % 10)
        if new_x > max_int or new_x < min_int:
            return 0
        num = int(num / 10)
    return new_x * (-1 if is_neg else 1)


def is_palindrome(num: int) -> int:
    """
    :type num: int
    :rtype: bool
    """
    if num < 0:
        return 0
    return int(reverse(num) == num)


print(is_palindrome(121))
print(is_palindrome(-121))
print(is_palindrome(2147447412))
print(reverse(121))
print(reverse(-123))
print(reverse(2147447412))
print(reverse(-1146467285))
print(reverse(-1153072433))
print(-0)
# from sys import maxsize
# print(maxsize, 2**32, 1 << 31)

import math

# Newton method


class Solution1:
    # @param A : integer
    # @return an integer

    def sqrt(self, n):
        x = n
        y = (x + 1) // 2
        while y < x:
            x = y
            y = (x + n // x) // 2
        return x


# print Solution().sqrt(11)
# print Solution().sqrt(111)
# print Solution().sqrt(1111)
# print Solution().sqrt(11111)
# print Solution().sqrt(930675566)


class Solution(object):

    def get_sqrt_chain_count(self, start_range, end_range):
        if start_range > end_range:
            return 0
        sqrt_a = math.ceil(math.sqrt(start_range))
        sqrt_b = math.floor(math.sqrt(end_range))
        if sqrt_a > sqrt_b:
            return 0
        return 1 + self.get_sqrt_chain_count(sqrt_a, sqrt_b)


# print Solution().get_sqrt_chain_count(600, 624)

# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")


def solution(S):
    # write your code in Python 3.6
    size = len(S)
    steps = 0
    char = ''
    first_valid_index = 0
    for i in range(size):
        if S[i] == '1':
            first_valid_index = i
            break
    print(first_valid_index)
    for i in range(size - 1, first_valid_index - 1, -1):
        char = S[i]
        if char == '0':
            steps += 1
        elif char == '1':
            steps += 2
        print(char, steps, i)
    # if char == '1':
    # steps += 1
    return steps


print(solution('0111001'))
# print solution('111000')
# print solution('1000000111001')


def x(n):
    if n == 0:
        return 0
    if n % 2 == 0:
        return 1 + x(n / 2)
    return 1 + x(n - 1)


print(x(28))
print(x(4153))

print(int('0111001', 2))


def sqrt(num):
    prev = 1
    int_val = 1
    counter = 2
    while True:
        sqr = counter**2
        # print(counter, prev, num,  sqr)
        #       24 529   755   576
        #       25 576   755   625
        #       26 625   755   676
        #       27 676   755   729
        #       28 729 < 755 < 784
        if prev <= num < sqr:
            int_val = counter - 1  # 28 - 1 = 27
            break
        prev = sqr
        counter += 1
    # print(int_val, num, prev, num - prev, int_val * 2.)
    #       27       755  729   26          54.0
    return int_val + ((num - prev) / (int_val * 2.))


print('-' * 50)
for i in range(8, 25):
    print(math.sqrt(i), sqrt(i), abs(sqrt(i) - math.sqrt(i)),
          Solution1().sqrt(i))

"""
Assume you have a method isSubstring which checks if one word is a substring
of another. Given two strings, si and s2, write code to check Ifs2 is a rotation of si
using only onecalltoisSubstring (e.g., "waterbottLe" is a rotation of "erbottLewat").
"""


def is_substring(string_1, string_2, counter=0):
    """
    Return True, if string 2 is rotational word in string 1, otherwise False.
    """
    if counter + 1 == len(string_1) and len(string_1) != len(string_2) and len(string_1):
        return False
    if string_1 == string_2:
        return True
    counter += 1
    return is_substring(
        string_1, string_2[-1:] + string_2[:-1], counter)


def is_substring_2(string_1, string_2):
    """The other approach."""
    return string_2 in string_1 + string_1

print is_substring('waterbottle', 'ewaterbottl')
print is_substring_2('waterbottle', 'ewaterbottl')

"""
https://leetcode.com/problems/longest-substring-without-repeating-characters
"""


class Solution(object):

    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        hash_table = {}
        max_length = 0
        cur_length = 0
        index = 0
        s_len = len(s)
        while index < s_len:
            char = s[index]
            if char in hash_table and hash_table[char] is not None:
                max_length = max(cur_length, max_length)
                cur_length = 0
                index = hash_table[char] + 1
                hash_table = {}
                char = s[index]
            cur_length += 1
            hash_table[char] = index
            index += 1
        return max(cur_length, max_length)

print Solution().lengthOfLongestSubstring("abcabcbb")
print Solution().lengthOfLongestSubstring("bbbbb")
print Solution().lengthOfLongestSubstring("pwwkew")
print Solution().lengthOfLongestSubstring("asfwerskasdnaxdmsvasoidxuwencsdjk")
print Solution().lengthOfLongestSubstring(" ")
print Solution().lengthOfLongestSubstring("aab")
print Solution().lengthOfLongestSubstring("dvdf")

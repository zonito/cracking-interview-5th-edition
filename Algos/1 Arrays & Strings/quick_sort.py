class Sorting(object):

    def quick_sort(self, array):
        ln_a = len(array)
        if ln_a <= 1:
            return array
        pivot = array[0]
        lesser = []
        greater = []
        for i in range(1, ln_a):
            if array[i] > pivot:
                greater.append(array[i])
            else:
                lesser.append(array[i])
        return self.quick_sort(lesser) + [pivot] + self.quick_sort(greater)


print Sorting().quick_sort([4, 2, 4, 6, 1, 0, 6, 8, 9, 7])
print Sorting().quick_sort([4, 2, 4])
print Sorting().quick_sort([4, 2, -4])
print Sorting().quick_sort([4, 2])
print Sorting().quick_sort([4])
print Sorting().quick_sort([])

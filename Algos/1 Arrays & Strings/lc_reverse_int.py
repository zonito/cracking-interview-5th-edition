"""
Given a 32-bit signed integer, reverse digits of an integer.

Example 1:

Input: 123
Output: 321
Example 2:

Input: -123
Output: -321
Example 3:

Input: 120
Output: 21
Note:
Assume we are dealing with an environment which could only store integers
within the 32-bit signed integer range: [-231,231-1]. For the purpose of
this problem, assume that your function returns 0 when the reversed integer
overflows.
"""


class Solution(object):

    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        max_int = 2**31 - 1
        min_int = -2**31
        is_neg = x < 0
        x *= (-1 if is_neg else 1)
        value = 0
        while x != 0:
            last_digit = x % 10
            x = x // 10
            value = value * 10 + last_digit
            temp = value * (-1 if is_neg else 1)
            if temp > max_int or temp < min_int:
                return 0
        return value * (-1 if is_neg else 1)


print Solution().reverse(123)
print Solution().reverse(-123)
print Solution().reverse(120)
print Solution().reverse(1534236469)

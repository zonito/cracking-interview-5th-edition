class Solution(object):

    def firstUniqChar(self, s):
        """
        :type s: str
        :rtype: int
        """
        if not s:
            return -1
        hash_var = {}
        for char in s:
            if not hash_var.get(char):
                hash_var[char] = 1
                continue
            hash_var[char] += 1
        index = -1
        # print hash_var
        ln_s = len(s)
        for i in range(ln_s):
            if hash_var[s[i]] == 1:
                index = i
                break
        return index

print Solution().firstUniqChar('leetcode')
print Solution().firstUniqChar('loveleetcode')

"""
Write a method to replace all spaces in a string with'%20'. You may assume that
the string has sufficient space at the end of the string to hold the additional
characters, and that you are given the "true" length of the string.
(Note: if implementing in Java, please use a character array so that you can
perform this operation in place.)
EXAMPLE
Input: "Mr John Smith
Output: "Mr%20Dohn%20Smith"
"""

# Pseudo Code:
# Loop through string - char wise
# If space found - add %20 instead and continue loop


def get_space_encode(string):
    """Return space encoded string from given string."""
    result = []
    for char in string:
        if char == ' ':
            result.append('%20')
        else:
            result.append(char)
    return ''.join(result)


print get_space_encode("Mr John Smith")
print '%20'.join('Love Kumar Sharma'.split())
print 'Love Kumar Sharma'.replace(' ', '%20')

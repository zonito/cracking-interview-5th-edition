class SolutionOld(object):

    hash_table = None
    max_length = 0
    substring = None

    def __init__(self):
        self.hash_table = {}
        self.max_length = 0
        self.substring = ''

    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        if not s:
            return ''
        if len(s) == 1:
            return s
        # print s
        if len(s) <= self.max_length:
            # print s, len(s)
            return self.substring
        for i in range(len(s) / 2):
            # print s[i], s[-i - 1]
            if s[i] != s[-i - 1]:
                # print 'break'
                break
        else:
            # print 's: ', s
            return s
        left_out = right_out = ''
        left_str = s[1:]
        left_out = self.hash_table.get(left_str)
        if not left_out:
            left_out = self.longestPalindrome(left_str)
            self.hash_table[left_str] = left_out
        right_str = s[:-1]
        right_out = self.hash_table.get(right_str)
        if not right_out:
            right_out = self.longestPalindrome(right_str)
            self.hash_table[right_str] = right_out
        substring = left_out if len(left_out) > len(right_out) else right_out
        substring = substring or s
        self.max_length = max(len(substring), self.max_length)
        self.substring = substring
        return substring


class Solution:

    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        list_ = [1] * len(s)
        for i, _ in enumerate(s):
            count1 = count2 = 1
            if i + 1 < len(s) and s[i] == s[i + 1]:
                mid = 1
                count1 = 2
                # print 'x-1', i + 1, s[i], s[i + 1]
                while i - mid >= 0 and i + 1 + mid < len(s) and s[i - mid] == s[i + 1 + mid]:
                    # print mid, count1, i - mid, i + 1 + mid, s[i - mid], s[i
                    # + 1 + mid]
                    count1 += 2
                    mid += 1
                # print mid, count1, i - mid, i + 1 + mid, s[i - mid], s[i + 1
                # + mid]
            if i - 1 >= 0 and i + 1 < len(s) and s[i - 1] == s[i + 1]:
                mid = 1
                count2 = 1
                # print 'x-2', i - 1, i + 1, s[i - 1], s[i + 1]
                while i - mid >= 0 and i + mid < len(s) and s[i - mid] == s[i + mid]:
                    # print mid, count2, i - mid, i + mid, s[i - mid], s[i +
                    # mid]
                    count2 += 2
                    mid += 1
                # print mid, count2, i - mid, i + mid, s[i - mid], s[i + mid]
            list_[i] = max(count1, count2)
        max_ = index = 0
        for j, val in enumerate(list_):
            if max_ < val:
                max_ = val
                index = j
        if max_ % 2 == 0:
            return s[index - max_ // 2 + 1:index + 1 + max_ // 2]
        return s[index - (max_ + 1) // 2 + 1:index + (max_ + 1) // 2]

print Solution().longestPalindrome("babad")
print Solution().longestPalindrome("cbbd")
print Solution().longestPalindrome("a")
print Solution().longestPalindrome("ba")
print Solution().longestPalindrome("bb")
print Solution().longestPalindrome(
    "babaddtattarrattatddetartrateedredividerb")
print Solution().longestPalindrome(
    "zudfweormatjycujjirzjpyrmaxurectxrtqedmmgergwdvjmjtstdhcihacqnothgttgq"
    "fywcpgnuvwglvfiuxteopoyizgehkwuvvkqxbnufkcbodlhdmbqyghkojrgokpwdhtdrwm"
    "vdegwycecrgjvuexlguayzcammupgeskrvpthrmwqaqsdcgycdupykppiyhwzwcplivjnn"
    "vwhqkkxildtyjltklcokcrgqnnwzzeuqioyahqpuskkpbxhvzvqyhlegmoviogzwuiqahi"
    "ouhnecjwysmtarjjdjqdrkljawzasriouuiqkcwwqsxifbndjmyprdozhwaoibpqrthpcj"
    "phgsfbeqrqqoqiqqdicvybzxhklehzzapbvcyleljawowluqgxxwlrymzojshlwkmzwpix"
    "gfjljkmwdtjeabgyrpbqyyykmoaqdambpkyyvukalbrzoyoufjqeftniddsfqnilxlplse"
    "lqatdgjziphvrbokofvuerpsvqmzakbyzxtxvyanvjpfyvyiivqusfrsufjanmfibgrkwt"
    "iuoykiavpbqeyfsuteuxxjiyxvlvgmehycdvxdorpepmsinvmyzeqeiikajopqedyopirm"
    "hymozernxzaueljjrhcsofwyddkpnvcvzixdjknikyhzmstvbducjcoyoeoaqruuewclzq"
    "qqxzpgykrkygxnmlsrjudoaejxkipkgmcoqtxhelvsizgdwdyjwuumazxfstoaxeqqxoqe"
    "zakdqjwpkrbldpcbbxexquqrznavcrprnydufsidakvrpuzgfisdxreldbqfizngtrilnb"
    "qboxwmwienlkmmiuifrvytukcqcpeqdwwucymgvyrektsnfijdcdoawbcwkkjkqwzffnuq"
    "ituihjaklvthulmcjrhqcyzvekzqlxgddjoir")
print Solution().longestPalindrome(
    "anugnxshgonmqydttcvmtsoaprxnhpmpovdolbidqiyqubirkvhwppcdyeouvgedccipsv"
    "nobrccbndzjdbgxkzdbcjsjjovnhpnbkurxqfupiprpbiwqdnwaqvjbqoaqzkqgdxkfczd"
    "kznqxvupdmnyiidqpnbvgjraszbvvztpapxmomnghfaywkzlrupvjpcvascgvstqmvuvei"
    "iixjmdofdwyvhgkydrnfuojhzulhobyhtsxmcovwmamjwljioevhafdlpjpmqstguqhrhv"
    "sdvinphejfbdvrvabthpyyphyqharjvzriosrdnwmaxtgriivdqlmugtagvsoylqfwhjpm"
    "jxcysfujdvcqovxabjdbvyvembfpahvyoybdhweikcgnzrdqlzusgoobysfmlzifwjzlaz"
    "uepimhbgkrfimmemhayxeqxynewcnynmgyjcwrpqnayvxoebgyjusppfpsfeonfwnbsdon"
    "ucaipoafavmlrrlplnnbsaghbawooabsjndqnvruuwvllpvvhuepmqtprgktnwxmflmmbi"
    "fbbsfthbeafseqrgwnwjxkkcqgbucwusjdipxuekanzwimuizqynaxrvicyzjhulqjshts"
    "qswehnozehmbsdmacciflcgsrlyhjukpvosptmsjfteoimtewkrivdllqiotvtrubgkfca"
    "cvgqzxjmhmmqlikrtfrurltgtcreafcgisjpvasiwmhcofqkcteudgjoqqmtucnwcocsoi"
    "qtfuoazxdayricnmwcg")
print Solution().longestPalindrome(
    "ibvjkmpyzsifuxcabqqpahjdeuzaybqsrsmbfplxycsafogotliyvhxjtkrbzqxlyfwujz"
    "hkdafhebvsdhkkdbhlhmaoxmbkqiwiusngkbdhlvxdyvnjrzvxmukvdfobzlmvnbnilnsy"
    "rgoygfdzjlymhprcpxsnxpcafctikxxybcusgjwmfklkffehbvlhvxfiddznwumxosomfb"
    "gxoruoqrhezgsgidgcfzbtdftjxeahriirqgxbhicoxavquhbkaomrroghdnfkknyigslu"
    "qebaqrtcwgmlnvmxoagisdmsokeznjsnwpxygjjptvyjjkbmkxvlivinmpnpxgmmorkase"
    "bngirckqcawgevljplkkgextudqaodwqmfljljhrujoerycoojwwgtklypicgkyaboqjfi"
    "vbeqdlonxeidgxsyzugkntoevwfuxovazcyayvwbcqswzhytlmtmrtwpikgacnpkbwgfmp"
    "avzyjoxughwhvlsxsgttbcyrlkaarngeoaldsdtjncivhcfsaohmdhgbwkuemcembmlwbw"
    "quxfaiukoqvzmgoeppieztdacvwngbkcxknbytvztodbfnjhbtwpjlzuajnlzfmmujhcgg"
    "pdcwdquutdiubgcvnxvgspmfumeqrofewynizvynavjzkbpkuxxvkjujectdyfwygnfsuk"
    "vzflcuxxzvxzravzznpxttduajhbsyiywpqunnarabcroljwcbdydagachbobkcvudkodd"
    "ldaucwruobfylfhyvjuynjrosxczgjwudpxaqwnboxgxybnngxxhibesiaxkicinikzzmo"
    "nftqkcudlzfzutplbycejmkpxcygsafzkgudy")
print Solution().longestPalindrome(
    "civilwartestingwhetherthatnaptionoranynartionsoconceivedandsodedicated"
    "canlongendureWeareqmetonagreatbattlefiemldoftzhatwarWehavecometodedicp"
    "ateaportionofthatfieldasafinalrestingplaceforthosewhoheregavetheirlive"
    "sthatthatnationmightliveItisaltogetherfangandproperthatweshoulddothisB"
    "utinalargersensewecannotdedicatewecannotconsecratewecannothallowthisgr"
    "oundThebravelmenlivinganddeadwhostruggledherehaveconsecrateditfarabove"
    "ourpoorponwertoaddordetractTgheworldadswfilllittlenotlenorlongremember"
    "whatwesayherebutitcanneverforgetwhattheydidhereItisforusthelivingrathe"
    "rtobededicatedheretotheulnfinishedworkwhichtheywhofoughtherehavethusfa"
    "rsonoblyadvancedItisratherforustobeherededicatedtothegreattdafskremain"
    "ingbeforeusthatfromthesehonoreddeadwetakeincreaseddevotiontothatcausef"
    "orwhichtheygavethelastpfullmeasureofdevotionthatweherehighlyresolvetha"
    "tthesedeadshallnothavediedinvainthatthisnationunsderGodshallhaveanewbi"
    "rthoffreedomandthatgovernmentofthepeoplebythepeopleforthepeopleshallno"
    "tperishfromtheearth")

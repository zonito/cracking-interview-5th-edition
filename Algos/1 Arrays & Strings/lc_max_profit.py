from typing import List
import sys


class Solution:

    def maxProfit(self, prices: List[int]):
        min_price = sys.maxsize
        max_profit = 0
        for price in prices:
            if price < min_price:
                min_price = price
            elif (price - min_price) > max_profit:
                max_profit = price - min_price
        return max_profit


# print(sys.maxsize)
print(Solution().maxProfit([7, 1, 5, 3, 6, 4]))
print(Solution().maxProfit([7, 6, 4, 3, 1]))
print(Solution().maxProfit([7, 6, 4, 3, 11, 2]))
print(Solution().maxProfit([2, 1, 2, 1, 0, 1, 2]))

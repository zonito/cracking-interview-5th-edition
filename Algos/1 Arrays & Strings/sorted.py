class LargerNumKey(str):

    def __lt__(x, y):
        print x, y
        return x + y > y + x


class Solution:

    def largestNumber(self, nums):
        largest_num = ''.join(sorted(map(str, nums), key=LargerNumKey))
        return '0' if largest_num[0] == '0' else largest_num

print Solution().largestNumber(['990', '900', '90', '9', '9990'])

print '-'
x = '99' * 100000000 + '9'
print '--'
y = '99' * 100000000 + '8'
print '---'
print x > y

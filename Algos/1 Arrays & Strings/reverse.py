"""
Implement a function void reverse(char* str) in C or C++ which reverses a
null terminated string.
"""


def get_reverse(string):
    """Return reverse string from given string."""
    if not isinstance(string, str):
        return string
    length = len(string)
    if length < 2:
        return string
    reverse_chars = [None] * length
    counter = length - 1
    for char in string:
        reverse_chars[counter] = char
        counter -= 1
    return ''.join(reverse_chars)


print get_reverse("Love sharma")

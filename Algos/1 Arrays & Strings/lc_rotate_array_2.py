from typing import List


class Solution(object):

    def rotate(self, nums: List[int], k: int) -> None:
        k = k % len(nums)
        if k == 0:
            return
        nums[:k], nums[k:] = nums[-k:], nums[:-k]

    def reverse(self, nums: List[int], start: int, end: int):
        while start < end:
            nums[start], nums[end] = nums[end], nums[start]
            start += 1
            end -= 1

    def rotate_2(self, nums: List[int], k: int) -> None:
        if k == 0:
            return
        k = k % len(nums)
        self.reverse(nums, 0, len(nums) - 1)
        self.reverse(nums, 0, k - 1)
        self.reverse(nums, k, len(nums) - 1)


for test_case in [
    ([1, 2, 3, 4, 5, 6, 7], 3),
    ([-1, -100, 3, 99], 2),
    ([1, 2], 3)
]:
    nums_1 = test_case[0][::]
    nums_2 = test_case[0][::]
    Solution().rotate(nums_1, test_case[1])
    Solution().rotate_2(nums_2, test_case[1])
    assert nums_1 == nums_2

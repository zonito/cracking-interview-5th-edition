"""
Given an array of scores that are non-negative integers. Player 1 picks one of
the numbers from either end of the array followed by the player 2 and then
player 1 and so on. Each time a player picks a number, that number will not be
available for the next player. This continues until all the scores have been
chosen. The player with the maximum score wins.

Given an array of scores, predict whether player 1 is the winner. You can
assume each player plays to maximize his score.

Example 1:
Input: [1, 5, 2]
Output: False
Explanation: Initially, player 1 can choose between 1 and 2.
If he chooses 2 (or 1), then player 2 can choose from 1 (or 2) and 5. If
player 2 chooses 5, then player 1 will be left with 1 (or 2).
So, final score of player 1 is 1 + 2 = 3, and player 2 is 5.
Hence, player 1 will never be the winner and you need to return False.
Example 2:
Input: [1, 5, 233, 7]
Output: True
Explanation: Player 1 first chooses 1. Then player 2 have to choose between 5
and 7. No matter which number player 2 choose, player 1 can choose 233.
Finally, player 1 has more score (234) than player 2 (12), so you need to
return True representing player1 can win.
Note:
1 <= length of the array <= 20.
Any scores in the given array are non-negative integers and will not
exceed 10,000,000.
If the scores of both players are equal, then player 1 is still the winner.
"""

# pylint: disable=C0103,C0111


class Solutionx(object):
    """..."""

    def FindWinner(self, nums, turn, p1, p2):
        """..."""
        print turn, nums, p1, p2
        if len(nums) == 1:
            if turn == '2':
                p2 += nums[0]
            else:
                p1 += nums[0]
            # print '--', turn, nums, p1, p2
            if p1 >= p2:
                # print 'Winner: 1 - %s' % p1
                return True
            # print 'Winner: 2 - %s' % p2
            return False
        elif len(nums) == 2:
            max_num = max(nums)
            min_num = min(nums)
            if turn == '2':
                p2 += max_num
                p1 += min_num
            else:
                p1 += max_num
                p2 += min_num
            print '==', turn, nums, p1, p2
            if p1 >= p2:
                print 'Winner: 1 - %s' % p1
                return True
            print 'Winner: 2 - %s' % p2
            return False
        left_p2 = right_p2 = p2
        left_p1 = right_p1 = p1
        if turn == '2':
            left_p2 += nums[0]
            right_p2 += nums[-1]
        else:
            left_p1 += nums[0]
            right_p1 += nums[-1]
        left_winner = self.FindWinner(
            nums[1:], '2' if turn == '1' else '1', left_p1, left_p2)
        right_winner = self.FindWinner(
            nums[:-1], '2' if turn == '1' else '1', right_p1, right_p2)
        print left_winner, right_winner
        return left_winner or right_winner

    def PredictTheWinner(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        return self.FindWinner(nums, '1', 0, 0)


class Solution(object):

    def PredictTheWinner(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        n = len(nums)
        memo = [[None for _ in range(n)] for _ in range(n)]
        return self.winner(nums, 0, n - 1, memo) >= 0

    def winner(self, nums, start, end, memo):
        if start == end:
            return nums[start]
        if memo[start][end] is not None:
            return memo[start][end]
        num_a = nums[start] - self.winner(nums, start + 1, end, memo)
        num_b = nums[end] - self.winner(nums, start, end - 1, memo)
        memo[start][end] = max(num_a, num_b)
        # print memo, num_a, num_b, start, end
        return memo[start][end]


# print Solution().PredictTheWinner([0])
# print Solution().PredictTheWinner([1, 5, 2])
# print Solution().PredictTheWinner([2, 4, 55, 6, 8])
print Solution().PredictTheWinner([1, 5, 233, 7])

"""
Given an image represented by an NxN matrix, where each pixel in the image is 4
bytes, write a method to rotate the image by 90 degrees. Can you do this in place?
"""


def rotate90(matrix):
    """Rotate matrix 90 degrees."""
    length = len(matrix)
    new_matrix = [[None for i in range(length)] for j in range(length)]
    for i in range((length // 2) + 1):
        ie_index = length - i - 1
        for j in range(length):
            if i > 0 and j > 0 and i < length and j < length:
                if new_matrix[i][j] is not None:
                    break
                new_matrix[i][j] = matrix[i][j]
                continue
            je_index = length - j - 1
            new_matrix[i][j] = matrix[je_index][i]
            new_matrix[i][je_index] = matrix[j][i]
            new_matrix[ie_index][je_index] = matrix[j][ie_index]
            new_matrix[ie_index][j] = matrix[je_index][ie_index]
    del matrix
    return new_matrix

_MATRIX = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
    [13, 14, 15, 16]
]
X = rotate90(_MATRIX)
print X
Y = rotate90(X)
print Y

from typing import List


def longest_sum(array: List[int]) -> int:  # O(N)
    size = len(array)  # O(N)
    max_so_far = curr_max = array[0]  # O(C)
    for i in range(1, size):  # O(N)
        curr_max = max(array[i], curr_max + array[i])  # O(C)
        max_so_far = max(max_so_far, curr_max)  # O(C)
    return max_so_far


print(longest_sum([-2, 1, -3, 4, -1, 2, 1, -5, 4]))
print(longest_sum([-13, -3, -25, -20, -3, -16, -23, -12, -5, -22, -15, -4,
                   -7]))

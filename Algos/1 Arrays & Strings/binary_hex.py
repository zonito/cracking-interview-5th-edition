
def digit_to_value(char):
    """Return value from given character."""
    ord_char = ord(char)
    if ord_char >= 48 and ord_char < 59:
        return ord_char - 48
    if ord_char >= 65 and ord_char < 71:
        return 10 + ord_char - 65
    if ord_char >= 97 and ord_char < 103:
        return 10 + ord_char - 97
    return -1


def convert_to_base(str_number, base):
    """Return base number for given number to the given base."""
    if base < 2 or (base > 10 and base != 16):
        return -1
    number = 0
    count = 0
    for char in str_number:
        temp = digit_to_value(char)
        if temp < 0 or temp >= base:
            return -1
        exp = len(str_number) - count - 1
        number += (temp * (base ** exp))
        count += 1
    return number


def compare_bin_to_hex(binary, hexa):
    """Return True if binary and hexadecimal number is same, otherwise False."""
    num1 = convert_to_base(binary, 2)
    num2 = convert_to_base(hexa, 16)
    # print num1, num2
    if num1 < 0 or num2 < 0:
        return False
    return num1 == num2


print compare_bin_to_hex('1010', 'A')
print compare_bin_to_hex('1011', 'b')

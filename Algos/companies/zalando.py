# you can write to stdout for debugging purposes, e.g.
# We are interested in a substring of a string

# The strings coming in look like 'abcbcbabcabbabcabbcbabbabbcbca'
# 'a','b','c'
# We are looking for the longest substring that contains 2 distinct characters


def check_counter(char_dict):
    total = 0
    for key, value in char_dict.items():
        if value:
            total += 1
    # if total > 2:
    #     print(char_dict)
    #     print(total)
    return total


def max_substring_two_distinct(string):
    ln_str = len(string)
    if ln_str <= 2:
        return string
    max_length = 2
    char_counter = {}
    sub_string = ''
    final_string = ''
    # print(final_string)
    for char in string:
        if not char_counter.get(char):
            char_counter[char] = 0
        char_counter[char] += 1
        sub_string += char  # abb
        counter = 0
        while check_counter(char_counter) > 2:
            # Remove chars from front.
            sub_char = sub_string[counter]
            if char_counter.get(sub_char):
                char_counter[sub_char] -= 1
            counter += 1
        # cab --> counter = 1 --> [1:] --> ab
        sub_string = sub_string[counter:]
        if len(sub_string) > max_length:
            final_string = sub_string
            max_length = len(final_string)
            # print(final_string)
    return final_string


print(max_substring_two_distinct('aaabbababaccacbacaca'))
print('aaabbababa')


def max_alternative(string):
    return max((word for group in (string.split(char) for char in 'abc') for word in group), key=len)

print(max_alternative('aaabbababaccacbacaca'))

# Is this linear?
# measure the time as a fucniton of input length for
# max_substring_two_distinct.
import time


def timeit(len_string):
    start = time.time()
    max_alternative('aaabbababaccacbacaca' * int(len_string / 20))
    print('Performance for length %s: %s' % (len_string, time.time() - start))

[timeit(100), timeit(1000), timeit(10000), timeit(100000)]
# Task 2:
# There is a sensor sending datapoints to a server every second
# We are interested in the all time mean value of all data received
# Complete the following code

# EG: Sensor so far produced: 1, 5, 6, 6, 12
# Correct all time mean: 6
import random
_SUM = 0
_LEN = 0
while False:
    new_data = random.randint(1, 100)
    _LEN += 1
    _SUM += new_data
    print('All time mean is {all_time_mean}'.format(
        all_time_mean=_SUM / (_LEN * 1.0)))


# The median is such a value that half of the points are less than, and half are more than it.
# EG: Sensor so far produced: 9,3,4,48,11,20,88,99 --> 3,4,9,11,20,48,88,99,100
# Correct Median:


import random
_VALUES = []
while False:
    new_data = random.randint(1, 100)
    _VALUES.append(new_data)
    _VALUES = sorted(_VALUES)
    print('All time median is {all_time_median}'.format(
        all_time_median=_VALUES[len(_VALUES) / 2]))

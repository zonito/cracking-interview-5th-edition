// Binary tree

"use strict";

function Node(data) {
  this.data = data;
  this.left = null;
  this.count = 1;
  this.right = null;
}

function BTree() {
  this.root = null;
  this.insert = function (data) {
    var node = new Node(data);
    if (!this.root) {
      this.root = node;
      return;
    }
    var root = this.root;
    var parent;
    while (root) {
      parent = root;
      if (root.data === data) {
        root.count += 1;
        break;
      }
      if (root.data > data) {
        root = root.left;
        if (root === null) {
          parent.left = node;
          break;
        }
      } else {
        root = root.right;
        if (root === null) {
          parent.right = node;
          break;
        }
      }
    }
  };
  this.remove = function (data) {
    this.root = this.removeNode(this.root, data);
  };
  this.removeNode = function (node, data) {
    if (!node) {
      return null;
    }
    if (data === node.data) {
      // Leaf node
      if (!node.left && !node.right) {
        return null;
      }
      // has one node - no left node
      if (!node.left) {
        return node.right;
      }
      // no right node
      if (!node.right) {
        return node.left;
      }
      // node has two children
      var tempNode = this.getMin(node.right);
      node.data = tempNode.data;
      node.right = this.removeNode(node.right, tempNode.data);
    } else if (data < node.data) {
      node.left = this.removeNode(node.left, data);
      return node;
    } else {
      node.right = this.removeNode(node.right, data);
      return node;
    }
  };
  this.inorder = function (root) {
    if (root) {
      this.inorder(root.left);
      console.log(root.data);
      this.inorder(root.right);
    }
  };
  this.preOrder = function (root) {
    if (root) {
      console.log(root.data);
      this.preOrder(root.left);
      this.preOrder(root.right);
    }
  };
  this.postOrder = function (root) {
    if (root) {
      this.postOrder(root.left);
      this.postOrder(root.right);
      console.log(root.data);
    }
  };
  this.getMin = function () {
    var root = this.root;
    var min = root;
    while (root) {
      min = root;
      root = root.left;
    }
    return min;
  };
  this.getMax = function () {
    var root = this.root;
    var max = root.data;
    while (root) {
      max = root.data;
      root = root.right;
    }
    return max;
  };
  this.find = function (data) {
    var root = this.root;
    while (root && root.data !== data) {
      if (root.data > data) {
        root = root.left;
      } else {
        root = root.right;
      }
    }
    return root || null;
  };
  this.display = function (root) {
    var result = [null, null, null];
    if (root) {
      result[0] = root.data;
      result[1] = this.display(root.left);
      result[2] = this.display(root.right);
    }
    if (result.some(function (item) {
        return item !== null;
      })) {
      return result;
    }
    return null;
  };
}

var btree = new BTree();
btree.insert(23);
btree.insert(45);
btree.insert(16);
btree.insert(37);
btree.insert(3);
btree.insert(99);
btree.insert(22);
// console.log(btree.display(btree.root));
// console.log(btree.inorder(btree.root));
// btree.preOrder(btree.root);
// btree.postOrder(btree.root);
// console.log(btree.getMin());
// console.log(btree.getMax());
// console.log(btree.find(23));
// console.log(btree.inorder(btree.root));
btree.remove(23);
// console.log(btree.root);
console.log(btree.inorder(btree.root));

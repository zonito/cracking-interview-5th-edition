// Prototype pattern

"use strict";
var someCar = {
  drive: function () {
    return true;
  },
  name: 'Mazda'
};

var otherCar = Object.create(someCar);
console.log(otherCar.name);


// Inheritance 1
var vehicle = {
  getModel: function () {
    return this.model;
  }
};

var counter = 0;
var car = Object.create(vehicle, {
  id: {
    value: ++counter,
    enumerable: true
  },
  model: {
    value: 'Mazda 3',
    enumerable: true
  }
});
console.log(car.getModel());

// Inheritance 2
var vehiclePrototype = {
  init: function (model) {
    this.model = model;
  },
  getModel: function () {
    return this.model;
  }
};

vehicle = function (model) {
  function F() {
    return;
  }
  F.prototype = vehiclePrototype;
  var f = new F();
  f.init(model);
  return f;
};

console.log(vehicle('Celerio').getModel());

// Inheritance 3
var beget = (function () {
  function F() {}
  return function (proto) {
    F.prototype = proto;
    return new F();
  };
}());

var f = beget(vehiclePrototype);
f.init('Maruti');
console.log(f.getModel());

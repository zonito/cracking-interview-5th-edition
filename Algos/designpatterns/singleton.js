// Singleton Pattern

// Everything public
var mySingleton1 = {
  key1: "Hello",
  key2: "World",
  console: function () {
    "use strict";
    return "Hello World";
  }
};
console.log(mySingleton1);

// Something public
var mySingleton2 = function () {
  "use strict";
  var privateObj = 1;
  return {
    key1: "Hello",
    privateObj: privateObj + 1
  };
};
console.log(mySingleton2().privateObj);

// On demand instantiation, in order to save memory resource
var mySingleton3 = function () {
  "use strict";
  var instance;

  function init() {
    return {
      hello: "Hello World"
    };
  }
  return {
    getInstance: function () {
      if (!instance) {
        instance = init();
      }
      return instance;
    }
  };
};
console.log(mySingleton3().getInstance().hello);

// Application of Singleton
var mySingleton4 = (function () {
  "use strict";
  // options: an object containing configuration options for the singleton
  // e.g var options = { name: 'test', pointX: 5};
  function Singleton(options) {
    // set options to the options supplied or an empty object if none provided.
    options = options || {};
    // set the name parameter
    this.name = 'mySingleton4';
    // set the value of pointX
    this.pointX = options.pointX || 6;
    // set the value of pointY
    this.pointY = options.pointY || 10;
  }
  // this is our instance holder
  var instance;
  // this is an emulation of static variables and methods
  var _static = {
    name: 'mySingleton4',
    // This is a method for getting an instance
    // It returns a singleton instance of a singleton object
    getInstance: function (options) {
      if (instance === undefined) {
        instance = new Singleton(options);
      }
      return instance;
    }
  };
  return _static;
}());
var singletonTest = mySingleton4.getInstance({
  pointX: 5
});
console.log(singletonTest.pointX); // outputs 5

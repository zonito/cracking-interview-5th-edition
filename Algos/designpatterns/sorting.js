"use strict";

function CArray(numElements) {
  this.dataStore = [];
  this.pos = 0;
  this.numElements = numElements;
  this.insert = function (element) {
    this.dataStore[this.pos++] = element;
  };
  this.toString = function () {
    var retstr = "";
    for (var i = 0; i < this.dataStore.length; ++i) {
      retstr += this.dataStore[i] + " ";
      if (i > 0 && i % 10 == 0) {
        retstr += "\n";
      }
    }
    return retstr;
  };
  this.clear = function () {
    for (var i = 0; i < this.dataStore.length; ++i) {
      this.dataStore[i] = 0;
    }
  };
  this.setData = function () {
    for (var i = 0; i < this.numElements; ++i) {
      this.dataStore[i] = Math.floor(Math.random() * (this.numElements + 1));
    }
  };
  this.swap = function (arr, index1, index2) {
    var temp = arr[index1];
    arr[index1] = arr[index2];
    arr[index2] = temp;
  };
  for (var i = 0; i < numElements; ++i) {
    this.dataStore[i] = i;
  }
  this.bubblesort = function () {
    var numElements = this.dataStore.length;
    var outer, inner;
    for (outer = numElements; outer >= 2; outer -= 1) {
      for (inner = 0; inner <= outer - 1; inner += 1) {
        if (this.dataStore[inner] > this.dataStore[inner + 1]) {
          this.swap(this.dataStore, inner, inner + 1);
        }
      }
    }
  };
  this.selectionSort = function () {
    var total = this.dataStore.length;
    var outer, inner, min;
    for (outer = 0; outer < total - 1; outer += 1) {
      min = outer;
      for (inner = outer + 1; inner < total; inner += 1) {
        if (this.dataStore[inner] < this.dataStore[min]) {
          min = inner;
        }
      }
      this.swap(this.dataStore, outer, min);
    }
  };
  this.insertionSort = function () {
    var total = this.dataStore.length;
    var outer, inner;
    for (outer = 1; outer < total; outer += 1) {
      for (inner = outer - 1; inner >= 0; inner -= 1) {
        if (this.dataStore[inner] > this.dataStore[inner + 1]) {
          this.swap(this.dataStore, inner, inner + 1);
        }
      }
    }
  };
  this.gaps = [5, 3, 1];
  this.shellSort = function () {
    var gap, i, j, temp;
    for (gap = 0; gap < this.gaps.length; gap += 1) {
      for (i = this.gaps[gap]; i < this.dataStore.length; i += 1) {
        temp = this.dataStore[i];
        for (j = i; j >= this.gaps[gap] && this.dataStore[j - this.gaps[gap]] > temp; j -= this.gaps[gap]) {
          this.dataStore[j] = this.dataStore[j - this.gaps[gap]];
        }
        this.dataStore[j] = temp;
      }
    }
  };
  this.mergeSort = function (arr) {
    var total = arr.length;
    if (total < 2) {
      return arr;
    }
    var mid = Math.floor(total / 2);
    var left = this.mergeSort(arr.slice(0, mid));
    var right = this.mergeSort(arr.slice(mid));
    var result = [],
      left_counter = 0,
      right_counter = 0;
    while (left.length > left_counter && right.length > right_counter) {
      if (left[left_counter] > right[right_counter]) {
        result.push(right[right_counter]);
        right_counter += 1;
      } else {
        result.push(left[left_counter]);
        left_counter += 1;
      }
    }
    while (left.length > left_counter) {
      result.push(left[left_counter]);
      left_counter += 1;
    }
    while (right.length > right_counter) {
      result.push(right[right_counter]);
      right_counter += 1;
    }
    return result;
  };
  this.quickSort = function (arr) {
    if (arr.length === 0) {
      return [];
    }
    var pivot = arr[0];
    delete arr[0];
    var lesser = [],
      greater = [],
      i;
    arr.forEach(function (element) {
      if (element >= pivot) {
        greater.push(element);
      } else {
        lesser.push(element);
      }
    });
    return this.quickSort(lesser).concat(pivot, this.quickSort(greater));
  };
}

var numElements = 10;
var myNums = new CArray(numElements);
myNums.setData();
console.log(myNums.toString());
// myNums.bubblesort();
// myNums.selectionSort();
// myNums.dataStore = "EADHB".split("");
// myNums.insertionSort();
// myNums.shellSort();
myNums.dataStore = myNums.mergeSort(myNums.dataStore);
// myNums.dataStore = myNums.quickSort(myNums.dataStore);
console.log(myNums.toString());

function pop() {
  return this.dataStore[--this.top];
}

function push(val) {
  this.dataStore[this.top++] = val;
}

function peek() {
  return this.dataStore[this.top - 1];
}

function Stack() {
  this.dataStore = [];
  this.top = 0;
  this.pop = pop;
  this.push = push;
  this.peek = peek;
}
/***
 * Multiple base conversion from base 2 through base 9.
 */
function mulBase(num, base) {
  if (base >= 2 && base <= 9) {
    var stack = new Stack();
    while (num > 0) {
      stack.push(num % base);
      num = Math.floor(num / base);
    }
    var result = "";
    while (stack.top) {
      result += stack.pop();
    }
    return result;
  }
  return "";
}

console.log(mulBase(32, 2));
console.log(mulBase(125, 8));

// Observer pattern
// Topic publisher / subscriber pattern
// 'One or more observers are interested in the state of a subject and
// register their interest with the subject by attaching themselves. When
// something changes in our subject that the observer may be interested in,
// a notify message is sent which calls the update method in each observer.
// When the observer is no longer interested in the subject's state, they can
// simply detach themselves.'

"use strict";
var pubsub = {};

(function (obj) {
  var topics = {},
    token = 0;
  obj.publish = function (topic, data) {
    // console.log(topics);
    if (!topics.hasOwnProperty(topic)) {
      return;
    }
    var subscribers = topics[topic];
    subscribers.forEach(function (object) {
      object.callback(topic, data);
    });
  };
  obj.subscribe = function (topic, callback) {
    if (!topics.hasOwnProperty(topic)) {
      topics[topic] = [];
    }
    topics[topic].push({
      callback: callback,
      token: ++token
    });
    console.log(topics);
    return {
      topic: topic,
      callback: callback,
      token: token
    };
  };
  obj.unsubscribe = function (object) {
    if (!topics.hasOwnProperty(object.topic)) {
      return;
    }
    var subscribers = topics[object.topic];
    subscribers = subscribers.filter(function (sub) {
      return sub.token !== object.token;
    });
    if (!subscribers.length) {
      delete topics[object.topic];
    } else {
      topics[object.topic] = subscribers;
    }
    console.log(topics);
  };
}(pubsub));

var callback = function (topic, data) {
  console.log("1. Topic: " + topic + ", Data: " + data);
};

var object1 = pubsub.subscribe('love', callback);
var object2 = pubsub.subscribe('love', callback);
var object3 = pubsub.subscribe('zonito', callback);
pubsub.publish('love', 'Hi All');
pubsub.unsubscribe(object1);
pubsub.unsubscribe(object2);
// pubsub.unsubscribe(object3);
pubsub.publish('love', 'Hi Remaining');
pubsub.publish('zonito', 'Hi Remaining');

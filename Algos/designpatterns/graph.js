// The Graph

"use strict";

function Vertex(label) {
  this.label = label;
  this.wasVisited = false;
}

function Graph(no_of_vertices) {
  this.vertices = no_of_vertices;
  this.edges = 0;
  this.marked = [];
  this.adj = [];
  var index;
  for (index = 0; index < this.vertices; index += 1) {
    this.marked[index] = false;
    this.adj[index] = [];
  }
  this.addEdge = function (v1, v2) {
    this.adj[v1].push(v2);
    this.adj[v2].push(v1);
    this.edges += 1;
  };
  this.showGraph = function () {
    var i, j;
    for (i = 0; i < this.vertices; i += 1) {
      console.log(i + " -> ");
      for (j = 0; j < this.vertices; j += 1) {
        if (this.adj[i][j] !== undefined) {
          console.log(this.adj[i][j]);
        }
      }
    }
  };
  this.dfs = function (vertex) {
    this.marked[vertex] = true;
    if (this.adj[vertex] !== undefined) {
      console.log("Visited vertex: " + vertex);
    }
    var that = this;
    this.adj[vertex].forEach(function (otherVertex) {
      if (!that.marked[otherVertex]) {
        that.dfs(otherVertex);
      }
    });
  };
  this.edgeTo = [];
  this.bfs = function (vertex) {
    var queue = [];
    this.marked[vertex] = true;
    queue.push(vertex);
    var that = this;
    while (queue.length) {
      var vert = queue.shift();
      if (this.adj[vert] !== undefined) {
        console.log("visited vertex: " + vert);
      }
      this.adj[vert].forEach(function (otherVertex) {
        if (!that.marked[otherVertex]) {
          that.edgeTo[otherVertex] = vert;
          that.marked[otherVertex] = true;
          queue.push(otherVertex);
        }
      });
    }
  };
  this.hasPathTo = function (vertex) {
    return this.marked[vertex];
  };
  this.pathTo = function (vertex) {
    var source = 0;
    if (!this.hasPathTo(vertex)) {
      return undefined;
    }
    var path = [],
      i;
    for (i = vertex; i !== source; i = this.edgeTo[i]) {
      path.push(i);
    }
    path.push(source);
    return path;
  };
}

var graph = new Graph(5);
graph.addEdge(0, 1);
graph.addEdge(0, 2);
graph.addEdge(1, 3);
graph.addEdge(2, 4);
// graph.showGraph();
// graph.dfs(0);
graph.bfs(0);
console.log(graph.edgeTo);
var vertex = 3;
var paths = graph.pathTo(vertex);
console.log(paths.reverse().join('-'));
// console.log(graph.adj);
// console.log(graph.marked);

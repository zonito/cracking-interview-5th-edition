"""Euclid's algorithm to find gcd and lcm of 2 numbers."""


def get_gcd(num1, num2):
    """Recursive function to find gcd of given 2 numbers."""
    return get_gcd(num2, num1 % num2) if num2 else num1


def get_lcm(num1, num2):
    """Return lcm of given 2 numbers"""
    return (num1 * num2) / get_gcd(num1, num2)


NUM1 = 350
NUM2 = 125
LCM = get_lcm(NUM1, NUM2)
print(get_gcd(NUM1, NUM2), LCM)
# To find: ax - by = 0
print(LCM / NUM1, LCM / NUM2)

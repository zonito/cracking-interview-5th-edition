"""Please use this Google doc to code during your interview. To free your hands for coding, we recommend that you use a headset or a phone with speaker option.

1) Assume you have two identical strings except one of them has an inserted character. How do you find which character was inserted? For example: "acdXf" “cdXfa” and "acdf", the inserted character is 'X'.

acdXf     acdf
a     a
c     c
d     d
X !f
f=f
"""


def get_inserted_char(word1, word2):
    """Return single inserted character in any given words."""
    # Case to handle empty string
    if not word1:
        return word2
    if not word2:
        return word1

    # Take the long word to start loop with
    long_word, short_word = (
        (word1, word2) if len(word1) > len(word2) else (word2, word1))
    counter = 0
    inserted_chars = []
    for char in long_word:
        if len(short_word) > (counter + 1) and char == short_word[counter]:
            counter += 1
        else:
            inserted_chars.append(char)
    return inserted_chars


print get_inserted_char('acdXf', 'acdf')  # [X]
print get_inserted_char('acdX', 'acd')  # [X]
print get_inserted_char('acdXxxf', 'acdf')  # [xxx]

print get_inserted_char('adcf', 'acdf')

# cdXfda      acddf  # Hasing
hash[X] = 1
hash[c] = 0
hash[d] = 0
hash[f] = 0
hash[a] = 0
# acddfx      acddf  # Sorting

"""
Write a function
    bool fancy_shuffle(char * s);

  which rearranges characters in the string given as input, in such a way that
  no same character occurs twice in a row(that is, next to each other).
  If such rearrangement is not possible, the function should return false.

  'ABBA' -> 'ABAB' or 'BABA'
  'AA' -> impossible


ABBA

hash[z]=10=9=8=7=6=5
hash[x]=5=4=3=2=1=0
Hash[A]=2=1=0
hash[B]=2=1=0

[z, x, a, b]

ABAB

zzzzzzzzzzxxxxxaabb

zxzxzazazbzb

"""
hash = {}


def store_it(string):
    """store given stirng in hash"""
    for char in string:
        if char not in hash:
            hash[char] = 0
        hash[char] += 1


def make_string():
    """Return required string from hash"""
    # Sort it
    list = hash.items()  # [(z, 10), (a, 2), (b, 2), (x,5), (y,4), (c, 2)]
    list.sorted(lambda x: x[1])  # Sort based on 2nd value.
    result = []
    is_complete = false
    prev = None
cur_index = 0
next_index = 1
done_indexes = []
    while not is_complete:
        cur_val = list[cur_index][0]
        next_val = list[next_index][0]
    result.append(cur_val)
result.append(next_val)

# Reduce it
list[cur_index][1] -= 1
list[next_index][1] -= 1

# Switch indexes - once the values are completed.
    if list[cur_index][1] == 0:
        done_indexes.append(cur_index)
        cur_index += 1
        while cur_index in done_indexes:
            cur_index += 1
if list[next_index][1] == 0:
    done_indexes.append(next_index)
    next_index += 1
    while next_index in done_indexes:
        next_index += 1
        if cur_val == prev:
            print “impossible”
            break

for obj in list:
if obj[1] > 0:
print “impossible”
break

    return ''.join(result)

store_it('abba')
print make_string()

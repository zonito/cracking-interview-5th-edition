// // Something something

"use strict";
var x = function (y, z) {
  console.log(arguments);
  return y + z;
};
// console.log(x.apply(2, [1, 2, 3]));

// Inheritance variety

var Person = function (firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.gender = 'male';
};

var person = new Person('Clark', 'Kent');
// console.log(person);

var SuperHero = function (firstName, lastName, powers) {
  Person.call(this, firstName, lastName);
  this.powers = powers;
};

// SuperHero.prototype = Object.create(Person.prototype);
var superhero = new SuperHero('Super', 'man', ['flight']);
console.log(superhero);

var myArr = [100],
  i;
myArr.forEach(function () {
  // console.log('in...');
});
for (i = 0; i < 20; i += 1) {
  myArr.push(i);
}
// console.log(myArr);
// console.log(myArr.slice(0, 4));
// console.log(myArr.splice(3, 3));
// console.log(myArr);
var nums = [2, 3, 4, 5];
var newnum = 1;
var N = nums.length;
for (i = N; i > 0; i -= 1) {
  nums[i] = nums[i - 1];
}
nums[0] = newnum;
nums.unshift(-1, 0);
// console.log(nums.shift());
// console.log(nums);
// var counter = 0;
myArr = [3, 1, 2, 100, 4, 200];
myArr.sort(function (num1, num2) {
  // console.log(counter, num1, num2);
  // counter += 1;
  return num1 - num2;
});
// console.log(myArr);

// console.log('------------------');

function isEven(num) {
  // console.log(num);
  return num % 2 === 0;
}
nums = [2, 4, 7, 8, 10];
var even = nums.every(isEven);
if (even) {
  // console.log("all numbers are even");
} else {
  // console.log("not all numbers are even");
}

Array.matrix = function (numrows, numcols, initial) {
  var arr = [];
  for (var i = 0; i < numrows; ++i) {
    var columns = [];
    for (var j = 0; j < numcols; j++) {
      columns[j] = initial
    }
    arr[i] = columns;
  }
  return arr;
};

console.log(Array.matrix(5, 5, 0));

function weekTemps() {
  this.dataStore = [];
  this.add = add;
  this.average = average;
  this.total = function () {
    return this.dataStore.length;
  }
}

function add(temp) {
  this.dataStore.push(temp);
}

function average() {
  var total = 0;
  for (var i = 0; i < this.dataStore.length; ++i) {
    total += this.dataStore[i];
  }
  return total / this.dataStore.length;
}
var thisWeek = new weekTemps();
thisWeek.add(52);
thisWeek.add(55);
thisWeek.add(61);
thisWeek.add(65);
thisWeek.add(55);
thisWeek.add(50);
thisWeek.add(52);
thisWeek.add(49);
console.log(thisWeek.average()); // displays 54.875
console.log(thisWeek.total()); // displays 8

console.log(typeof thisWeek);
console.log(thisWeek instanceof Person);
console.log(thisWeek instanceof weekTemps);

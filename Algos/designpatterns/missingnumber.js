"use strict";

function missingNumber(arr) {
  var result = [],
    len = arr.length,
    diff;
  var push = function (diff, item, lastItem) {
    if (diff === 2) {
      result.push(lastItem);
    } else if (diff > 2) {
      result.push(item + "-" + lastItem);
    }
  };
  arr.forEach(function (item, index) {
    if (index < len) {
      diff = arr[index + 1] - item;
      push(diff, item + 1, arr[index + 1] - 1);
    }
  });
  diff = 100 - arr[len - 1];
  push(diff, arr[len - 1] + 1, 99);
  return result.join(",");
}
console.log(missingNumber([0, 1, 3, 8, 43, 50, 76, 98]));


class Graph(object):
    """A Graph object."""
    marked = []
    adj = []
    edge_to = []

    def __init__(self, num_of_vertices):
        for _ in range(num_of_vertices):
            self.marked.append(False)
            self.adj.append([])
            self.edge_to.append(None)

    def add_edge(self, vertex_1, vertex_2):
        """Add adjacencies"""
        self.adj[vertex_1].append(vertex_2)
        self.adj[vertex_2].append(vertex_1)

    def show_graph(self):
        """Show graph."""
        print self.adj

    def dfs(self, vertex):
        """Depth first search, starts from given vertex."""
        self.marked[vertex] = True
        if self.adj[vertex]:
            print 'Visited vertex: %d' % vertex
        for other_vertex in self.adj[vertex]:
            if not self.marked[other_vertex]:
                self.dfs(other_vertex)

    def bfs(self, vertex):
        """Breath first search."""
        queue = [vertex]
        self.marked[vertex] = True
        while queue:
            vert = queue.pop(0)
            if self.adj[vertex]:
                print 'Visited vertex: %d' % vert
            for other_vertex in self.adj[vert]:
                if not self.marked[other_vertex]:
                    self.edge_to[other_vertex] = vert
                    self.marked[other_vertex] = True
                    queue.append(other_vertex)

    def path_to(self, vertex):
        """Return path to given vertex."""
        source = 0
        if not self.marked[vertex]:
            return []
        path = []
        i = vertex
        while i != source:
            path.append(i)
            i = self.edge_to[i]
        path.append(source)
        return path


GRAPH = Graph(5)
GRAPH.add_edge(0, 1)
GRAPH.add_edge(0, 2)
GRAPH.add_edge(1, 3)
GRAPH.add_edge(2, 4)
GRAPH.show_graph()
# GRAPH.dfs(0)
GRAPH.bfs(0)
print GRAPH.edge_to
PATH = GRAPH.path_to(3)
PATH.reverse()
print '->'.join(map(str, PATH))

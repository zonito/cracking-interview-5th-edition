function isPrime(num) {
  if (num < 2) {
    return false;
  }
  var sqrt = Math.round(Math.sqrt(num)),
    i;
  // console.log(sqrt);
  for (i = 2; i <= sqrt; i++) {
    // console.log(i, num % i);
    if (num % i === 0) {
      return false;
    }
  }
  return true;
}

console.log(isPrime(323));

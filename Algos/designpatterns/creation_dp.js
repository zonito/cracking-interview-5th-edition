/*global this */

// Creation design pattern

// 3 ways to create an object
var newObject1 = {};
var newObject2 = Object.create(null);
var newObject3 = new Object(); // lint: Expected `Object.create(null)`

// 4 ways to assign
newObject1.key1 = 'Hello';
newObject1['key2'] = ' World'; // lint: better written in dot notation.
Object.defineProperty(newObject1, 'key3', {
  value: '!',
  writable: true,
  enumerable: true,
  configurable: true
});
Object.defineProperties(newObject1, {
  key4: {
    value: '!',
    writable: true,
    enumerable: true,
    configurable: false
  },
  key5: {
    value: '!',
    writable: false,
    enumerable: false,
    configurable: false
  }
});

// My rough
var strings = [];
Object.keys(newObject1).forEach(function (item) {
  'use strict';
  strings.push(newObject1[item]);
});

console.log(strings.join(''));
console.log(newObject2);
console.log(newObject3);

// Constructors

// 1.Basic Constructor
// Problem: inheritance is difficult
function Car(model, year, miles) {
  'use strict';
  this.model = model;
  this.year = year;
  this.miles = miles;
  // Problem: redefined for each of the new object created, should be shared.
  this.toString = function () {
    return this.model + " has done " + this.miles + " miles";
  };
}

var civic = new Car('Honda Civic', 2009, 2000);
var mondeo = new Car('Ford Mondeo', 2005, 5000);
console.log(civic.toString());
console.log(mondeo.toString());

// 2. Constructor with prototype
function Animal(name, type) {
  'use strict';
  this.name = name;
  this.type = type;
}
// Problem Solved: toString function is shared with all Animal objects.
Animal.prototype.toString = function () {
  'use strict';
  return 'I have ' + this.type + ', we call it ' + this.name;
};

var cat = new Animal('Tom', 'Cat');
var mouse = new Animal('Jerry', 'Mouse');
console.log(cat.toString());
console.log(mouse.toString());

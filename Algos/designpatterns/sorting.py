"""

Sortings.
URL: http://www.sorting-algorithms.com/

"""

# pylint: disable=W0613, C0111
import copy
import random


def generate_values(total, number=100):
    """Return random values."""
    return [random.randint(1, number) for _ in range(total)]

VALUES = generate_values(10)


def swap(obj, index1, index2):
    temp = obj[index1]
    obj[index1] = obj[index2]
    obj[index2] = temp


def bubble(lst):
    total = len(lst)
    for outer in range(total, 1, -1):
        for inner in range(outer - 1):
            if lst[inner] > lst[inner + 1]:
                swap(lst, inner, inner + 1)
    return lst

print 'bubble: ', bubble(copy.deepcopy(VALUES))
print 'bubble: ', bubble(['a', 'c', 'b'])


def selection(lst):
    total = len(lst)
    for outer in range(total):
        mini = outer
        for inner in range(outer + 1, total):
            if lst[inner] < lst[mini]:
                mini = inner
        swap(lst, outer, mini)
    return lst

print 'selection: ', selection(copy.deepcopy(VALUES))
print 'selection: ', selection(['a', 'c', 'b'])


def quick(lst):
    if len(lst) <= 1:
        return lst
    pivot = lst[0]
    left = []
    right = []
    for obj in lst[1:]:
        if obj > pivot:
            right.append(obj)
        else:
            left.append(obj)
    return quick(left) + [pivot] + quick(right)

print 'quick: ', quick(copy.deepcopy(VALUES))
print 'quick: ', quick(['a', 'c', 'b'])


def merge(lst):
    lst_len = len(lst)
    if lst_len <= 1:
        return lst
    mid = lst_len // 2
    left = merge(lst[0:mid])
    right = merge(lst[mid:])
    len_left = len(left)
    len_right = len(right)
    result = []
    left_counter = 0
    right_counter = 0
    for i in range(len_left + len_right):
        if right_counter < len_right and left_counter < len_left:
            if right[right_counter] >= left[left_counter]:
                result.append(left[left_counter])
                left_counter += 1
            elif left[left_counter] > right[right_counter]:
                result.append(right[right_counter])
                right_counter += 1
    for i in range(len_left - left_counter):
        result.append(left[left_counter + i])
    for i in range(len_right - right_counter):
        result.append(right[right_counter + i])
    return result

print 'merge: ', merge(copy.deepcopy(VALUES))
print 'merge: ', merge(['a', 'c', 'b'])


def heapify(lst, end, i):
    """
        [2, 3, 1] = [3, 2, 1]
        Reorder the array to put maximum number at 0th index.
    """
    left = 2 * i + 1
    right = 2 * (i + 1)
    maxi = i
    if left < end and lst[i] < lst[left]:
        maxi = left
    if right < end and lst[maxi] < lst[right]:
        maxi = right
    if maxi != i:
        swap(lst, i, maxi)
        heapify(lst, end, maxi)


def heap(lst):
    total = len(lst)
    mid = total // 2 - 1  # use // instead of /
    for i in range(mid, -1, -1):
        heapify(lst, total, i)
    # print lst
    for i in range(total - 1, 0, -1):
        swap(lst, i, 0)
        heapify(lst, i, 0)
    return lst

print 'heap: ', heap(copy.deepcopy(VALUES))
print 'heap: ', heap(['edas', 'dfasg', 'bwer'])


# def radix(lst):
#     return lst

# print 'radix: ', radix(copy.deepcopy(VALUES))
# print 'radix: ', radix(['a', 'c', 'b'])


# def bucket(lst):
#     return lst

# print 'bucket: ', bucket(copy.deepcopy(VALUES))
# print 'bucket: ', bucket(['a', 'c', 'b'])


# def binary(lst):
#     return lst

# print 'bucket: ', bucket(copy.deepcopy(VALUES))
# print 'bucket: ', bucket(['a', 'c', 'b'])

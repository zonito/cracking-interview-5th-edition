function toBin(num) {
  var result = [];
  while (num) {
    result.push(num % 2);
    num = Math.floor(num / 2);
  }
  return result.reverse().join('');
}

function toDec(bin) {
  var temp, sum = 0,
    counter = 0;
  while (bin) {
    sum += (bin % 10) * Math.pow(2, counter);
    bin = Math.floor(bin / 10);
    counter += 1;
  }
  return sum;
}

console.log(toBin(31));
console.log(toBin(14));
console.log(toBin(2147727868));
console.log(toDec(10000000000000000000000000000000));
console.log(toDec(10101010));
console.log(toDec(1010101));
console.log(toDec(10101010));
console.log(toDec(1010101));

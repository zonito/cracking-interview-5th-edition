// Search Algorithms
"use strict";
var search = {
  sequential: function (arr, data) {
    return arr.some(function (item) {
      return item === data;
    });
  },
  binary: function (arr, data) {
    var total = arr.length;
    if (total <= 1) {
      return total ? total[0] === data : false;
    }
    var mid = Math.ceil(total / 2);
    if (arr[mid] === data) {
      return true;
    }
    if (total === 2) {
      return arr[0] === data || arr[1] === data;
    }
    if (arr[mid] < data) {
      return this.binary(arr.slice(mid), data);
    } else {
      return this.binary(arr.slice(0, mid), data);
    }
  },
  binary2: function (arr, data) {
    var upperBound = arr.length - 1;
    var lowerBound = 0,
      mid;
    while (lowerBound <= upperBound) {
      mid = Math.floor((upperBound + lowerBound) / 2);
      if (arr[mid] < data) {
        lowerBound = mid + 1;
      } else if (arr[mid] > data) {
        upperBound = mid - 1;
      } else {
        return mid;
      }
    }
    return -1;
  }
};

var nums = [1, 2, 3, 4, 5, 6, 7, 8];
// for (var i = 0; i < 10; ++i) {
//   nums[i] = Math.floor(Math.random() * 101);
// }
// console.log(search.sequential(nums, 10));
console.log(search.binary(nums, 60));
console.log(search.binary2(nums, 6));

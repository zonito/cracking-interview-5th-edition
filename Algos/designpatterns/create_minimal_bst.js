// Binary tree

"use strict";

function Node(data) {
  this.data = data;
  this.left = null;
  this.right = null;
}

function createMinimalBST(arr, start, end) {
  if (end < start) {
    return null;
  }
  var mid = Math.floor((end + start) / 2);
  var node = new Node(arr[mid]);
  node.left = createMinimalBST(arr, start, mid - 1);
  node.right = createMinimalBST(arr, mid + 1, end);
  return node;
}

console.log(createMinimalBST([1, 2, 3, 4, 5, 6, 7, 8, 9, 0], 0, 9));

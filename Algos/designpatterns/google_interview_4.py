Please use this Google doc to code during your interview. To free your hands for coding, we recommend that you use a headset or a phone with speaker option.


Unsigned 64 bit int = 2 ^ 64

Factors:
CPU(3GHz) - 3 * 10 ^ 9 operation / sec
(2 ^ 64) / (3 * 10 ^ 9) = (2 ^ 34)

x ^ n * x ^ m = x ^ (n + m)
x ^ n / x ^ m = x ^ (n - m)


10 ^ 9 = (2 * 5) ^ 9 =

2 ^ 1 = 2
2 ^ 2 = 4
2 ^ 3 = 8
2 ^ 4 = 16
2 ^ 5 = 32
2 ^ 6 = 64
2 ^ 7 = 128
2 ^ 8 = 256
2 ^ 9 = 512

2 ^ 10 = 1024
10 ^ 3 = 1000

2 ^ 10 ~ 10 ^ 3
2 ^ 20 ~ 10 ^ 6
2 ^ 30 ~ 10 ^ 9


2 ^ 1 = 2 sec
2 ^ 6 = 64 sec = 1 min

x = 2
n = 34
m = 6

2 ^ 28 min / 2 ^ 22 hours / 2 ^ 18 days / 1024 years


x = 2
n = 28
m = 6
2 ^ 34 sec =


Implement Conway's Game of Life.
For a 2D m by n rectangular grid, each cell can be on or off.
Each cell updates based on its eight neighbours.
If 2 neighbours are on, the state remains the same.
If 3 neighbours are on, the state becomes on.
Otherwise the state becomes off.
All cells should behave as if updated simultaneously.
The grid should be toroidally mapped, such that looking off the right edge sees the left edge, vice versa, and top to bottom.


N x M = 5x6

0 1 1 1 1
1 0 1 1 1
1 1 1 1 1
1 1 1 1 1
1 1 1 1 1
1 1 1 1 1


def next_iteration(grid):
    “””Return next iteration of given grid.”””
    if not grid:
        return grid

    snap_grid = deepcopy(grid)
    N = len(snap_grid)
    M = len(snap_grid[0])
    result_grid = [[0 for _ in M] for _ in N]

    for i in range(N):
        for j in range(M):
num_of_on = snap_grid[i - i][j] + snap_grid[i + 1][j] + snap_grid[i][j - 1] + snap_grid[i][j + 1] + \
    snap_grid[i + 1][j + 1] + snap_grid[i + 1][j - 1] + \
    snap_grid[i - 1][j + 1] + snap_grid[i - 1][j - 1]
if num_of_on == 3:
    result_grid[i][j] = 1
elif num_of_on == 2:
    result_grid[i][j] = snap_grid[i][j]

    return grid

// Modules pattern
// Disadvantage:
//   1. Change of visibility is not easy, you need to change every early
//      accessible places.
//   2. Unit test for private method is not possible, need to mock public.

var someObj = (function () {
  "use strict";
  var counter = 0;
  return {
    inc: function () {
      return counter++;
    },
    dec: function () {
      return counter--;
    },
    reset: function () {
      console.log('Counter: ', counter);
      counter = 0;
    }
  };
}());

someObj.inc();
someObj.inc();
someObj.inc();
someObj.dec();
someObj.reset();
someObj.reset();

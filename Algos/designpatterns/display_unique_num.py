"""
Display all the numbers from 0 to 1000000. All the digits of a displayed number
should be different.
"""


def get_nums():
    queue_num = range(1, 10)
    unique_val = range(1, 10)
    while queue_num:
        num = queue_num.pop(0)
        num_str = set(map(int, str(num))).symmetric_difference(range(10))
        num *= 10
        for i in num_str:
            temp = num + i
            if temp > 1000000:
                break
            queue_num.append(temp)
            unique_val.append(temp)
        if num > 1000000:
            break
    return unique_val

VALUE = get_nums()
print len(VALUE)


def all_perms(elements):
    if len(elements) <= 1:
        yield elements
    else:
        for perm in all_perms(elements[1:]):
            for j in range(len(elements)):
                # nb elements[0:1] works in both string and list contexts
                yield perm[:j] + elements[0:1] + perm[j:]

# for p in all_perms(range(3)):
#     print p

# from itertools import islice, permutations
# print ''.join(next(islice(permutations(list(map(str, list(range(10))))),
# 999999, None)))

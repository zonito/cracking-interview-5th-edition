"use strict";

function Vertex(label) {
  this.label = label;
  this.weight = 0;
  this.fromVertex = null;
}

function Graph() {
  this.adj = [];
  this.addEdge = function (v1, v2, weight) {
    if (!this.adj.hasOwnProperty(v1.label)) {
      this.adj[v1.label] = [];
    }
    if (!this.adj.hasOwnProperty(v2.label)) {
      this.adj[v2.label] = [];
    }
    this.adj[v1.label].push([v2, weight]);
    this.adj[v2.label].push([v1, weight]);
  };
  this.updateWeight = function (referenceVertex) {
    console.log(this.adj);
    var queue = [referenceVertex],
      vertex, temp;
    while (queue.length) {
      vertex = queue.shift();
      this.adj[vertex.label].forEach(
        function (otherVertex) {
          var toVertex = otherVertex[0];
          if (toVertex === referenceVertex) return;
          temp = vertex.weight + otherVertex[1];
          console.log(toVertex.label, vertex.label, temp);
          if (toVertex.weight && toVertex.weight <= temp) return;
          toVertex.weight = temp;
          toVertex.fromVertex = vertex;
          if (queue.indexOf(toVertex) === -1) queue.push(toVertex);
        }
      );
    }
  };
  this.pathTo = function (referenceVertex, toVertex) {
    this.updateWeight(referenceVertex);
    var result = [toVertex.label],
      toVer = toVertex;
    while (toVer && toVer !== referenceVertex) {
      toVer = toVer.fromVertex;
      if (toVer) {
        result.push(toVer.label);
      }
    }
    if (!toVer) {
      console.log('No Route found!')
      return;
    }
    console.log(result.join('<-') + ' with weight ' + toVertex.weight);
  };
}

var v1 = new Vertex('v1'),
  v2 = new Vertex('v2'),
  v3 = new Vertex('v3'),
  v4 = new Vertex('v4'),
  v5 = new Vertex('v5'),
  v6 = new Vertex('v6'),
  v7 = new Vertex('v7');
var graph = new Graph();
graph.addEdge(v1, v2, 7);
graph.addEdge(v1, v3, 9);
graph.addEdge(v1, v6, 14);
graph.addEdge(v6, v5, 9);
graph.addEdge(v5, v4, 2);
graph.addEdge(v2, v3, 10);
graph.addEdge(v3, v6, 1);
graph.addEdge(v3, v4, 13);
graph.addEdge(v2, v4, 15);
graph.addEdge(v5, v7, 6);
graph.addEdge(v4, v7, 3);
graph.pathTo(v1, v7);
// console.log(graph.adj);

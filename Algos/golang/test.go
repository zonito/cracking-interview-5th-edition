package main

import (
	"fmt"
	"math"
	"math/cmplx"
	"math/rand"
	"time"
)

const Pi = 3.14

const (
	StatusOK                   = 200
	StatusCreated              = 201
	StatusAccepted             = 202
	StatusNonAuthoritativeInfo = 203
	StatusNoContent            = 204
	StatusResetContent         = 205
	StatusPartialContent       = 206
	Truth                      = false
	Big                        = 1 << 62
	Small                      = Big >> 61
)

func add(x int, y int) int {
	return x + y
}
func mul(x, y int) (result int) {
	result = x * y
	return
}

type Artist struct {
	Name, Genre string
	Songs       int
}

func newRelease(a Artist) int { //passing an Artist by value
	a.Songs++
	return a.Songs
}

var (
	goIsFun bool       = true                 //declaring a variable of type bool
	maxInt  uint64     = 1<<64 - 1            //declaring a variable of type uint64
	complex complex128 = cmplx.Sqrt(-5 + 12i) //declaring a variable of type complex128
)

func varmain() {
	const f = "%T(%v)\n"
	fmt.Printf(f, goIsFun, goIsFun)
	fmt.Printf(f, maxInt, maxInt)
	fmt.Printf(f, complex, complex)
}

func timeMap(y interface{}) {
	z, ok := y.(map[string]interface{}) //asserting y as a map of interfaces
	if ok {
		z["updated_at"] = time.Now() //z now has the type map[string]interface
	}
}

func assertmain() {
	foo := map[string]interface{}{
		"Matt": 42,
	}
	timeMap(foo)
	fmt.Println(foo)
}

func main() {
	name, location := "Prince Oberyn", "Dorne"
	age := 32
	fmt.Printf("%s age %d from %s ", name, age, location)
	fmt.Println()
	action := func() { //action is a variable that contains a function
		const Greeting = "ハローワールド" //declaring a constant
		fmt.Println(Greeting)
		fmt.Println(Big)
		fmt.Println(Small)
	}
	action()

	aka := fmt.Sprintf("Number %d", 6)
	fmt.Printf("%s is also known as %s", name, aka)
	fmt.Println()
	fmt.Println("My Fav Number is ", rand.Intn(10))
	fmt.Println()
	fmt.Printf("Sqrt 7: %g ", math.Sqrt(7))
	fmt.Println(math.Pi)
	fmt.Println(add(1, 2))
	fmt.Println(mul(3, 2))

	me := Artist{Name: "Matt", Genre: "Electro", Songs: 42}
	fmt.Printf("%s released their %dth song\n", me.Name, newRelease(me))
	fmt.Printf("%s has a total of %d songs", me.Name, me.Songs)
	varmain()
	assertmain()
}

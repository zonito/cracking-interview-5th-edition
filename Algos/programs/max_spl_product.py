from typing import List


def get_max_spl_product(arr: List[int]) -> int:
    total_arr = len(arr)
    max_product = 0
    first_index = 0
    for index in range(total_arr - 1):
        if arr[index] > arr[index + 1]:
            first_index = index
        elif arr[index] < arr[index + 1] and first_index:
            max_product = max(first_index * (index + 1), max_product)
            first_index = 0
    return max_product % 1000000007


""" Correct Solution - in Ruby!
class Solution
    # @param a : array of integers
    # @return an integer
    def maxSpecialProduct(array)
        return 0 if array.size < 4

        stack = []
        left = []

        array.each_with_index do |el, idx|
            stack.pop while !stack.empty? && stack.last[0] <= el
            left << (stack.empty? ? 0 : stack.last[1])
            stack.push [el, idx]
        end
        stack = []

        array.each_with_index.reverse_each.reduce(0) do |max, (el, idx)|
            stack.pop while !stack.empty? && stack.last[0] <= el
            right_idx = (stack.empty? ? 0 : stack.last[1])
            stack.push [el, idx]

            [left[idx] * right_idx, max].max
        end % 1_000_000_007
    end
end

"""

print(get_max_spl_product([5, 9, 6, 8, 6, 4, 6, 9, 5, 4, 9]))
print(get_max_spl_product([5, 9, 6, 8, 6, 4, 6, 9, 5, 4, 4]))
print(get_max_spl_product([6, 7, 9, 5, 5, 5, 8]))
print(get_max_spl_product([5, 4, 3, 4, 5]))
print(get_max_spl_product([
    1950, 9417, 7760, 1939, 8551, 5184, 2187, 1097, 9686, 525, 7923, 364, 9182,
    3013, 3252, 2203, 5496, 1537, 3455, 2209, 6981, 8032, 831, 2096, 6715,
    3113, 2137, 9938, 2010, 5686, 2920, 4382, 9611, 9909, 1610
]))

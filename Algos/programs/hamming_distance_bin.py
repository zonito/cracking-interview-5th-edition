from typing import List


def get_hamming_distance(num1: int, num2: int) -> int:
    if num1 == num2:
        return 0
    bin_max = bin(max(num1, num2)).replace('0b', '')
    bin_min = bin(min(num1, num2)).replace('0b', '')
    bin_min = '0' * (len(bin_max) - len(bin_min)) + bin_min
    distance = 0
    for index in range(len(bin_min)):
        if bin_min[index] != bin_max[index]:
            distance += 1
    # print(num1, num2, bin_min, bin_max, distance)
    return distance


def get_total_hamming_distance(arr: List[int]) -> int:
    size = len(arr)
    distance = 0
    for index in range(size):
        for next_index in range(index + 1, size):
            distance += get_hamming_distance(arr[index], arr[next_index]) * 2
    return distance


print(get_total_hamming_distance([2, 4, 6]))
print(get_total_hamming_distance([2, 96]))
print(get_total_hamming_distance([i for i in range(200)]))

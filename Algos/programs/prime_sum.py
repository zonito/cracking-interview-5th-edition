from typing import List


def sieve_of_erastosthenes(num: int):
    is_prime = {0: False, 1: False}
    for index in range(2, num + 1):
        is_prime[index] = True

    coefficient = 2
    while coefficient**2 <= num:
        if is_prime[coefficient]:
            index = coefficient**2
            while index < num:
                is_prime[index] = False
                index += coefficient
        coefficient += 1
    return is_prime


def get_prime_pairs(num: int) -> List[int]:
    prime_dict = sieve_of_erastosthenes(num)
    for i in range(num):
        if prime_dict[i] and prime_dict[num - i]:
            return [i, num - i]
    return []


print(get_prime_pairs(4))
print(get_prime_pairs(15))
print(get_prime_pairs(16))
print(get_prime_pairs(74))

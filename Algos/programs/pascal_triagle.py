from typing import List


def get_row(row_num: int) -> List[int]:
    if row_num < 0:
        return []
    result = [1]
    for _ in range(row_num):
        temp = [result[0]]
        for i in range(len(result) - 1):
            temp.append(result[i] + result[i + 1])
        temp.append(result[-1])
        result = temp
    return result


if __name__ == '__main__':
    for num in range(10):
        print(get_row(num))

from typing import List


def plus_one(arr: List[int]) -> List[int]:
    total_arr = len(arr)
    valid_start_index = 0
    for index in range(total_arr):
        if arr[index] > 0:
            valid_start_index = index
            break
    last_index = total_arr - 1
    total = 0
    while last_index > valid_start_index - 1:
        total = arr[last_index] + 1
        if total < 10:
            arr[last_index] = total
            break
        arr[last_index] = 0
        last_index -= 1
    if total == 10:
        return [1] + arr[valid_start_index:]
    return arr[valid_start_index:]


print(plus_one([1, 2, 3]))
print(plus_one([1, 2, 9]))
print(plus_one([1, 9, 9]))
print(plus_one([9, 9, 9]))
print(plus_one([0, 3, 7, 6, 4, 0, 5, 5, 5]))
print(plus_one([0, 9, 9, 9]))

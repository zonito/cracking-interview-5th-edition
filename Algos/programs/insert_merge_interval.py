from typing import List


def get_ins_merge_intervals(intervals: List[List[int]],
                            new_interval: List[int]) -> List[List[int]]:
    size = len(intervals)
    if not intervals:
        return [new_interval]
    if intervals[-1][1] < new_interval[0]:
        return intervals + [new_interval]
    if intervals[0][0] > new_interval[1]:
        return [new_interval] + intervals
    max_position = -1
    min_position = -1
    for last_index in range(size - 1, -1, -1):
        block = intervals[last_index]
        # print(block, new_interval, max(block[1], new_interval[1]))
        if (max(block[1], new_interval[1]) == new_interval[1] or max(
                block[0],
                new_interval[1]) == new_interval[1]) and max_position is -1:
            max_position = last_index
        if min(block[0], new_interval[0]) == block[0] and min_position is -1:
            min_position = last_index
            break
    min_position = 0 if min_position is -1 else min_position
    # print(intervals[last_index], min_position, max_position)
    return intervals[:min_position] + [
        (min(new_interval[0], intervals[min_position][0]),
         max(new_interval[1], intervals[max_position][1]))
    ] + intervals[max_position + 1:]


"""
vector<Interval> Solution::insert(vector<Interval> &intervals, Interval newInterval) {
    vector<Interval> result;
    bool pinnedStart = false;
    bool inserted = false;

    for(Interval &i : intervals){
        if(inserted){
            result.push_back(i);
            continue;
        }

        if(!pinnedStart && newInterval.start < i.start){
            pinnedStart = true;
        } else if(!pinnedStart && newInterval.start < i.end){
            pinnedStart = true;
            newInterval.start = i.start;
        } else if(!pinnedStart) {
            result.push_back(i);
            continue;
        }

        if(newInterval.end < i.start){
            result.push_back(newInterval);
            result.push_back(i);
            inserted = true;
        } else if(newInterval.end < i.end){
            newInterval.end = i.end;
            result.push_back(newInterval);
            inserted = true;
        }
    }

    if(!inserted){
        result.push_back(newInterval);
    }

    return result;
}
"""

print(get_ins_merge_intervals([], [1, 12]))
print(get_ins_merge_intervals([(3, 5), (8, 10)], [1, 12]))
print(get_ins_merge_intervals([(3, 6), (8, 10)], (1, 2)))
print(get_ins_merge_intervals([(1, 2), (3, 6)], (8, 10)))
print(get_ins_merge_intervals([[1, 3], [6, 9]], [2, 5]))
print(get_ins_merge_intervals([[-1, 0], [1, 2], [6, 9]], [3, 5]))
print(get_ins_merge_intervals([[1, 2], [3, 5], [6, 7], [8, 10], [12, 16]],
                              [4, 9]))

import math
from typing import List


def get_factorization(num: int) -> List[int]:
    if num == 1:
        return [1]
    result = []
    sqrt = math.sqrt(num)
    for i in range(1, math.ceil(sqrt) + 1):
        if num % i == 0:
            result.append(i)
            if i != sqrt:
                result.append(int(num / i))
    result.sort()
    return result


print(get_factorization(38808))

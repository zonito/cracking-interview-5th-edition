from typing import List
from sys import maxsize


def get_max_abs_diff(array: List[int]) -> int:
    # max and min variables as described
    # in algorithm.
    max1 = -maxsize - 1
    min1 = maxsize
    max2 = -maxsize - 1
    min2 = maxsize

    for i, _ in enumerate(array):

        # Updating max and min variables
        # as described in algorithm.
        max1 = max(max1, array[i] + i)
        min1 = min(min1, array[i] + i)
        max2 = max(max2, array[i] - i)
        min2 = min(min2, array[i] - i)

    # Calculating maximum absolute difference.
    return max(max1 - min1, max2 - min2)


print(get_max_abs_diff([1, 3, -1]))
print(get_max_abs_diff([3, -2, 5, -4]))
print(get_max_abs_diff([-70, -64, -6, -56, 64, 61, -57, 16, 48, -98]))

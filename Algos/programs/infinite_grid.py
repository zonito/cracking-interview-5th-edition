from typing import List


def get_steps(arr_a: List[int], arr_b: List[int]) -> int:
    return sum(
        max(abs(arr_a[i] - arr_a[i - 1]), abs(arr_b[i] - arr_b[i - 1]))
        for i in range(1, len(arr_a)))


print(get_steps([-7, -13], [-5, 1]))
print(get_steps([0, 1, 1], [0, 1, 2]))

from typing import List


def knapsack(arr: List[tuple], capacity: int) -> int:
    total_arr = len(arr)
    # DO NOT USE: capacity_matrix = [[0] * (capacity + 1)] * (total_arr + 1)
    # Above one will use value as reference. Use below initialization method.
    capacity_matrix = [[0 for _ in range(capacity + 1)]
                       for _ in range(total_arr + 1)]
    for item_index in range(1, total_arr + 1):
        for capacity_index in range(1, capacity + 1):
            include_cost = 0
            if capacity_index - arr[item_index - 1][0] >= 0:
                include_cost = arr[item_index - 1][1] + capacity_matrix[
                    item_index - 1][capacity_index - arr[item_index - 1][0]]
            exclude_cost = capacity_matrix[item_index - 1][capacity_index]
            capacity_matrix[item_index][capacity_index] = max(
                exclude_cost, include_cost)
    return capacity_matrix[total_arr][capacity]


print(knapsack([(2, 3), (2, 1), (1, 3)], 4))

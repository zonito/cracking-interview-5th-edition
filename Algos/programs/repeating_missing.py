from typing import List


def get_repeat_missing_nums(array: List[int]) -> List[int]:
    repeat_num = sum(array) - sum(set(array))  # 14-11 = 3
    size = len(array)  # 5
    sum_n = int((size * (size + 1)) / 2)  # 5 * (6) / 2 = 15
    missing_num = sum_n - sum(set(array))  # 15 - 11 = 4
    return [repeat_num, missing_num]


print(get_repeat_missing_nums([3, 1, 2, 5, 3]))
print(get_repeat_missing_nums([3, 1, 3]))
print(get_repeat_missing_nums([4, 3, 6, 2, 1, 1]))
print(get_repeat_missing_nums([i for i in range(100000)]))

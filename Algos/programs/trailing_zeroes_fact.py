def get_zeroes_count(num: int) -> int:
    zeroes = 0
    five = 5
    while (num / five) >= 1:
        zeroes += int(num / five)
        five *= 5
    return zeroes


print(get_zeroes_count(5))
print(get_zeroes_count(20))
print(get_zeroes_count(1000))
print(get_zeroes_count(1000))

from typing import List


def get_nobel_int(array: List[int]) -> int:
    if not array:
        return -1
    array.sort()
    if array[-1] == 0:
        return 0
    size = len(array)
    for index in range(size - 1):
        if array[index] == array[index + 1]:
            continue
        if array[index] == size - index - 1:
            return array[index]
    return -1


print(get_nobel_int([7, 3, 16, 10]))
print(get_nobel_int([-1, -9, -2, -78, 0]))

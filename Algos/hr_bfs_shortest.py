"""
Given an undirected graph consisting of NN nodes (labelled 1 to N) where a
specific given node SS represents the start position and an edge between any
two nodes is of length 66 units in the graph.

It is required to calculate the shortest distance from start position (Node S)
to all of the other nodes in the graph.

Note 1: If a node is unreachable , the distance is assumed as -1.
Note 2: The length of each edge in the graph is 66 units.

Input Format

The first line contains TT, denoting the number of test cases.
First line of each test case has two integers NN, denoting the number of nodes
in the graph and MM, denoting the number of edges in the graph.
The next MM lines each consist of two space separated integers x y, where
xx and yy denote the two nodes between which the edge exists.
The last line of a testcase has an integer SS, denoting the starting position.

Constraints
1<=T<=101<=T<=10
2<=N<=10002<=N<=1000
1<=M<=Nx(N-1)21<=M<=Nx(N-1)2
1<=x,y,S<=N1<=x,y,S<=N

Output Format

For each of TT test cases, print a single line consisting of N-1
space-separated integers, denoting the shortest distances of the N-1 nodes
from starting position SS. This will be done for all nodes same as in the
order of input 1 to N.

For unreachable nodes, print -1-1.

Sample Input

2
4 2
1 2
1 3
1
3 1
2 3
2
Sample Output

6 6 -1
-1 6
"""

_TEST_CASES = int(raw_input())
_RESULTS = []
for case in range(_TEST_CASES):
    nodes, edges = map(int, raw_input().split())
    adj = {}
    for i in range(edges):
        node1, node2 = map(int, raw_input().split())
        if node1 not in adj:
            adj[node1] = []
        adj[node1].append(node2)
        if node2 not in adj:
            adj[node2] = []
        adj[node2].append(node1)
    start = int(raw_input())
    result = {start: 0}
    queue = adj[start]
    while queue:
        node = queue.pop(0)
        if node not in result:
            values = []
            for temp_node in adj[node]:
                if temp_node in result:
                    values.append(result[temp_node])
                else:
                    queue.append(temp_node)
            if values:
                result[node] = 6 + min(values)
    values = []
    for node in range(1, nodes + 1):
        if node != start:
            values.append(str(result.get(node, -1)))
    _RESULTS.append(' '.join(values))
for result in _RESULTS:
    print result

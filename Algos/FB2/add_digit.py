class Solution:
    def addDigits(self, num: int):
        return (num % 9 if num % 9 else 9) if num else 0

    def addDigits2(self, num: int):
        rem_digit = 0
        while num > 0:
            rem_digit += num % 10
            if rem_digit >= 10:
                rem_digit = rem_digit // 10 + rem_digit % 10
            num //= 10
        return rem_digit

assert Solution().addDigits(38) == Solution().addDigits2(38) == 2
assert Solution().addDigits(999999992) == Solution().addDigits2(999999992) == 2
assert Solution().addDigits(0) == Solution().addDigits2(0) == 0

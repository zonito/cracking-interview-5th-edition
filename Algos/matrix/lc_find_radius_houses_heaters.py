class Solution(object):

    def findRadiusx(self, houses, heaters):
        """
        :type houses: List[int]
        :type heaters: List[int]
        :rtype: int
        """
        output = 1
        bit_heater = 0
        for num in heaters:
            bit_heater |= 1 << num
        counter = 0
        ln_h = len(houses)
        last_index = 0
        for i_h in range(ln_h):
            house = houses[i_h]
            if bit_heater & 1 << house:
                gap = (counter - i_h + 1) / 2
                if gap > output:
                    output = gap
                last_index = i_h
            counter += 1
        # print counter - 1, last_index
        gap = counter - 1 - last_index
        if gap > output:
            output = gap
        return output

    def findRadius(self, houses, heaters):
        houses.sort()
        heaters.sort()
        heat_l = heat_r = -10**9
        min_rad = 0
        heat_it = iter(heaters)
        for house in houses:
            if house > heat_r:
                for cur_heat in heat_it:
                    # print cur_heat
                    if cur_heat < house:
                        heat_l = cur_heat
                        heat_r = cur_heat
                    else:
                        heat_l = heat_r
                        heat_r = cur_heat
                        break

            min_rad = max(min_rad, min(abs(house - heat_l),
                                       abs(house - heat_r)))
            # print house, heat_l, heat_r, min_rad
        return min_rad

# print Solution().findRadius([1, 5], [2])
# print Solution().findRadius([1, 2, 3], [2])
print Solution().findRadius([1, 2, 3, 4], [1, 4])
# print Solution().findRadius([1, 2, 3, 4, 5, 6, 7], [2, 5])
# print Solution().findRadius([1, 2, 3, 4], [1])

class Solution(object):

    def maxIncreaseKeepingSkyline(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        if not grid:
            return 0
        rows = len(grid)
        columns = len(grid[0])
        top_bottom = [0] * columns
        left_right = [0] * rows
        for row in range(rows):
            for column in range(columns):
                if top_bottom[column] < grid[row][column]:
                    top_bottom[column] = grid[row][column]
                if left_right[row] < grid[row][column]:
                    left_right[row] = grid[row][column]
        heights = 0
        for row in range(rows):
            for column in range(columns):
                heights += abs(grid[row][column] - min(
                    top_bottom[column], left_right[row]))
        return heights


print Solution().maxIncreaseKeepingSkyline(
    [
        [3, 0, 8, 4],
        [2, 4, 5, 7],
        [9, 2, 6, 3],
        [0, 3, 1, 0]
    ])

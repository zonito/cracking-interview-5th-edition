''' https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/309/ '''

from typing import List


class Solution:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int):
        # Make a copy of the first m elements of nums1.
        nums1_copy = nums1[:m]

        # Read pointers for nums1Copy and nums2 respectively.
        p1 = 0
        p2 = 0

        # Compare elements from nums1Copy and nums2 and write the smallest to nums1.
        for p in range(n + m):
            # We also need to ensure that p1 and p2 aren't over the boundaries
            # of their respective arrays.
            if p2 >= n or (p1 < m and nums1_copy[p1] <= nums2[p2]):
                nums1[p] = nums1_copy[p1]
                p1 += 1
            else:
                nums1[p] = nums2[p2]
                p2 += 1
        print(nums1_copy, nums1)


for num1, num2 in [
    ([1, 2, 3, 0, 0, 0], [2, 5, 6]),
    ([1], []),
    ([2, 3, 4, 0, 0], [0, 1])
]:
    ans = sorted(num1[:len(num1) - len(num2)] + num2)
    Solution().merge(num1, len(num1) - len(num2), num2, len(num2))
    assert num1 == ans

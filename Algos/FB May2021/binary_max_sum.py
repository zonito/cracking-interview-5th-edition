''' https://leetcode.com/problems/binary-tree-maximum-path-sum '''


from binarytree import build, Node as TreeNode


class Solution:
    max_num = -1001

    def findMaxPathSum(self, root: TreeNode):
        if not root:
            return 0
        # print(root)
        left_sum = self.findMaxPathSum(root.left)
        right_sum = self.findMaxPathSum(root.right)
        self.max_num = max(
            self.max_num,
            root.val,
            root.val + left_sum,
            root.val + right_sum,
            root.val + left_sum + right_sum
        )
        if not left_sum and not right_sum:
            return root.val
        return max(root.val, left_sum + root.val, right_sum + root.val)

    def maxPathSum(self, root: TreeNode):
        self.findMaxPathSum(root)
        return self.max_num


assert Solution().maxPathSum(build([1, -2, -3, 1, 3, -2, None, -1])) == 3
assert Solution().maxPathSum(build([-10])) == -10
assert Solution().maxPathSum(build([-10, -3])) == -3
assert Solution().maxPathSum(build([-10, 9, 20, None, None, 15, 7])) == 42
assert Solution().maxPathSum(build([1, 2, 3])) == 6

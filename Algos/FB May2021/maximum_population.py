from typing import List
import collections


class Solution:
    def maximumPopulation(self, logs: List[List[int]]):
        years = collections.defaultdict(int)
        max_pop = 0
        max_year = 0
        for log in logs:
            for year in range(log[0], log[1]):
                years[year - 1950] += 1
                if years[year - 1950] > max_pop or (years[year - 1950] == max_pop and year < max_year):
                    max_pop = years[year - 1950]
                    max_year = year
        return max_year

    def maximumPopulation2(self, logs: List[List[int]]):
        years = []
        for birth, death in logs:
            years.append((birth, 1))
            years.append((death, -1))
        years.sort()
        population = max_population = max_year = 0
        for year, delta in years:
            population += delta
            if population > max_population:
                max_population = population
                max_year = year
        return max_year


assert Solution().maximumPopulation2(
    [
        [2033, 2034],
        [2039, 2047],
        [1998, 2042],
        [2047, 2048],
        [2025, 2029],
        [2005, 2044],
        [1990, 1992],
        [1952, 1956],
        [1984, 2014]
    ]) == 2005
assert Solution().maximumPopulation2(
    [
        [2008, 2026],
        [2004, 2008],
        [2034, 2035],
        [1999, 2050],
        [2049, 2050],
        [2011, 2035],
        [1966, 2033],
        [2044, 2049]
    ]) == 2011
assert Solution().maximumPopulation2([[1993, 1999], [2000, 2010]]) == 1993
assert Solution().maximumPopulation2([[1950, 1962], [1960, 1971], [1961, 1971], [1970, 1981]] * 25) == 1961
assert Solution().maximumPopulation2([[1950, 2050], [1960, 2050], [1961, 2050], [1970, 2050]] * 25) == 1970

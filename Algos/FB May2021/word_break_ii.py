''' https://leetcode.com/problems/word-break-ii/ '''

from typing import List


class Trie:
    def __init__(self):
        self.trie = {}

    def insert(self, word: str):
        trie = self.trie
        for char in word:
            trie[char] = trie.get(char, {})
            trie = trie[char]
        trie['*'] = '*'


class Solution:

    def __init__(self):
        self.output = []

    def breakWord(self, s: str, trie: Trie, output: List[str]):
        if not s:
            self.output.append(' '.join(output))
            return
        trie_obj = trie.trie
        for index, char in enumerate(s):
            if not trie_obj.get(char):
                break
            trie_obj = trie_obj[char]
            if trie_obj.get('*') == '*':
                self.breakWord(s[index + 1:], trie, output[::] + [s[:index + 1]])

    def wordBreak(self, s: str, wordDict: List[str]):
        trie = Trie()
        for word in wordDict:
            trie.insert(word)
        self.breakWord(s, trie, [])
        return self.output


assert Solution().wordBreak('catsanddog', ['cat', 'cats', 'and', 'sand', 'dog']) == ['cats and dog', 'cat sand dog']
# assert Solution().wordBreak('pineapplepenapple', ['apple', 'pen', 'applepen', 'pine', 'pineapple']) == [
#     'pine apple pen apple', 'pineapple pen apple', 'pine applepen apple']
# assert Solution().wordBreak('catsandog', ['cats', 'dog', 'sand', 'and', 'cat']) == []

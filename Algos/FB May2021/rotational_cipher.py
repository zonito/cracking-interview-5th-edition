
def rotationalCipher(input, rotation_factor):
    cap_range = (ord('A'), ord('Z'))
    small_range = (ord('a'), ord('z'))
    num_range = (ord('0'), ord('9'))
    result = []
    for char in input:
        if cap_range[0] <= ord(char) <= cap_range[1]:
            nchar = cap_range[0] + (ord(char) + rotation_factor - cap_range[0]) % 26
        elif small_range[0] <= ord(char) <= small_range[1]:
            nchar = small_range[0] + (ord(char) + rotation_factor - small_range[0]) % 26
        elif num_range[0] <= ord(char) <= num_range[1]:
            nchar = num_range[0] + (ord(char) + rotation_factor - num_range[0]) % 10
        else:
            result.append(char)
            continue
        result.append(chr(nchar))
    return ''.join(result)


assert rotationalCipher('Zebra-493?', 3) == 'Cheud-726?'


from binarytree import build, Node as TreeNode


class Solution:
    def get_end_index(self, s: str, start_index: str):
        stack = ['(']
        end_index = start_index + 1
        n = len(s)
        while end_index < n and stack:
            if s[end_index] == '(':
                stack.append(s[end_index])
            elif s[end_index] == ')':
                stack.pop()
            end_index += 1
        return end_index - 1

    def str2tree(self, s: str):
        if not s:
            return None
        n = len(s)
        i = 0

        # Extract number
        num = []
        while i < n and s[i] not in ['(', ')']:
            num.append(s[i])
            i += 1

        # Make tree
        root = TreeNode(int(''.join(num)))
        if i < n and s[i] == '(':
            left_end_index = self.get_end_index(s, i)
            root.left = self.str2tree(s[i + 1: left_end_index])
            root.right = self.str2tree(
                s[left_end_index + 2: self.get_end_index(s, left_end_index)])
        print(root)
        return root


assert Solution().str2tree('4(2(3)(1))(6(5))')

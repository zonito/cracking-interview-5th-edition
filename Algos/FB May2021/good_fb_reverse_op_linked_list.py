import collections


class Node:
    def __init__(self, x):
        self.data = x
        self.next = None


def reverse(head):
    node = head
    evens = collections.deque()
    while node is not None:
        if node.data % 2 == 0:  # even, start pushing
            evens.append(node)
        if evens and node.next:  # not even, start popping
            while len(evens) > 1:
                evens[0].data, evens[-1].data = evens[-1].data, evens[0].data
                evens.pop()
                evens.popleft()
            evens.clear()
        node = node.next
    return head


def createLinkedList(arr):
    head = None
    tempHead = head
    for v in arr:
        if head == None:
            head = Node(v)
            tempHead = head
        else:
            head.next = Node(v)
            head = head.next
    return tempHead


# 100,605.19 - 469 - 214.51
# 100,605.19 - 625 - 160.9

# reverse series of even numbers in the array
reverse(createLinkedList([2, 18, 24, 3, 5, 7, 9, 6, 12]))  # [24,18,21,3,5,7,9,12,6]
reverse(createLinkedList([1, 2, 8, 9, 12, 16]))  # [1,8,2,9,16,12]

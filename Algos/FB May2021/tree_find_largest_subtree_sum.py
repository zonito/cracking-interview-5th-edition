
from binarytree import build, Node


class Solution:
    total = 0

    def find_largest_subtree_sum(self, root):
        if root is None:
            return 0
        if root.left is None and root.right is None:
            return root.val
        # print(root)
        left_sum = self.find_largest_subtree_sum(root.left)
        right_sum = self.find_largest_subtree_sum(root.right)
        self.total = max(self.total, left_sum + right_sum + root.val)
        return left_sum + right_sum + root.val


assert Solution().find_largest_subtree_sum(build([1, 2, 3, 4, 5, 6, 7])) == 28
assert Solution().find_largest_subtree_sum(build([1, -2, 3, 4, 5, -6, 2])) == 7

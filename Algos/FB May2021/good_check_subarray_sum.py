""" https://leetcode.com/problems/continuous-subarray-sum/ """

from typing import List


class Solution:

    def checkSubarraySum(self, nums: List[int], k: int):
        d = dict()
        d[0] = -1

        total = 0  # Running sum of the mod
        for i in range(len(nums)):
            total += nums[i]  # add the next number to the total
            if k != 0:
                total = total % k  # Take a mod with K

            if d.get(total) is not None:  # Running sum of mod already seen before.
                if i - d.get(total) >= 2:  # validates the length of sub-array is atleast 2
                    return True
            else:
                d[total] = i
        return False  # Unable to find a sun-array


assert not Solution().checkSubarraySum([23, 2, 6, 7], 13)
assert Solution().checkSubarraySum([23, 2, 4, 6, 7], 6)  # 2,4
assert Solution().checkSubarraySum([23, 2, 6, 4, 7], 6)  # 2,6,4
assert not Solution().checkSubarraySum([23, 2, 6, 4, 7], 13)

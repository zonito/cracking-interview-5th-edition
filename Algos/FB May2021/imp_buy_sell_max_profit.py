""" https://leetcode.com/explore/interview/card/facebook/55/dynamic-programming-3/304/ """


from typing import List


class Solution:
    def maxProfit(self, prices: List[int]):
        output = 0
        start = 0
        n = len(prices)
        second = 1
        while second < n:
            # Find min price
            if prices[start] > prices[second]:
                start = second
                second += 1
                continue
            # Find max profit from min to next max price
            output = max(output, prices[second] - prices[start])
            second += 1
        return output


assert Solution().maxProfit([7, 2, 5, 3, 6, 5, 7, 8, 10, 11, 12, 1, 15, 16, 4]) == 15
assert Solution().maxProfit([7, 1, 5, 3, 6, 4]) == 5
assert Solution().maxProfit([7, 6, 4, 3, 1]) == 0
assert Solution().maxProfit([2, 4, 1]) == 2

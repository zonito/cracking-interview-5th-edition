""" https://leetcode.com/problems/random-pick-with-weight/ """


import random
import itertools
from typing import List


class Solution:

    def __init__(self, w: List[int]):
        """
        :type w: List[int]
        """
        self.prefix_sums = list(itertools.accumulate(w))

    def pickIndex(self):
        """
        :rtype: int
        """
        target = self.prefix_sums[-1] * random.random()
        # run a linear search to find the target zone
        for i, s in enumerate(self.prefix_sums):
            if target < s:
                return i


# Your Solution object will be instantiated and called as such:
obj = Solution([1])
print(obj.pickIndex())

obj = Solution([1, 3])
print(obj.pickIndex())
print(obj.pickIndex())
print(obj.pickIndex())

obj = Solution([188, 927, 949, 95, 151, 659, 405, 906, 481, 363, 728, 839])
for _ in range(10):
    print(obj.pickIndex())

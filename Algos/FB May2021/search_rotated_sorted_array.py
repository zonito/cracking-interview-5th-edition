''' https://leetcode.com/explore/interview/card/facebook/54/sorting-and-searching-3/279/ '''


from typing import List


class Solution:
    def search(self, nums: List[int], target: int):
        start, end = 0, len(nums) - 1
        while start <= end:
            mid = start + (end - start) // 2
            if nums[mid] == target:
                return mid
            elif nums[mid] >= nums[start]:
                if target >= nums[start] and target < nums[mid]:
                    end = mid - 1
                else:
                    start = mid + 1
            else:
                if target <= nums[end] and target > nums[mid]:
                    start = mid + 1
                else:
                    end = mid - 1
        return -1


assert Solution().search([4, 5, 6, 7, 0, 1, 2], 0) == 4
assert Solution().search([7, 0, 1, 2, 4, 5, 6], 1) == 2
assert Solution().search([7, 0, 1, 2, 4, 5, 6], 3) == -1
assert Solution().search([5, 6], 5) == 0
assert Solution().search([6, 5], 6) == 0
assert Solution().search([9, 5, 6, 8], 6) == 2

''' https://leetcode.com/problems/permutations/solution/ '''


from typing import List


class Solution:
    def permute(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        def backtrack(first=0):
            # if all integers are used up
            if first == n:
                output.add(tuple(nums[:]))
            for i in range(first, n):
                # place i-th integer first
                # in the current permutation
                nums[first], nums[i] = nums[i], nums[first]
                # use next integers to complete the permutations
                backtrack(first + 1)
                # backtrack
                nums[first], nums[i] = nums[i], nums[first]

        n = len(nums)
        output = set()
        backtrack()
        print(list(output))
        return list(output)


Solution().permute([1, 2, 3])
Solution().permute([1, 1, 3])
Solution().permute([1, 2, 3, 4])

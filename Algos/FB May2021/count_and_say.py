""" https://leetcode.com/problems/count-and-say/ """

import collections


class Solution:
    def count(self, s: str, n: int):
        ans = []
        i = 0
        last = None
        ctr = 0
        for i in range(n):
            if last and s[i] != last:
                ans.append(ctr)
                ans.append(last)
                ctr = 1
            else:
                ctr += 1
            last = s[i]
        ans.append(ctr)
        ans.append(last)
        return ans

    def countAndSay(self, n: int):
        i = 2
        last = [1]
        while i <= n:
            last = self.count(last, len(last))
            i += 1
        return ''.join(map(str, last))


assert Solution().countAndSay(1) == '1'
assert Solution().countAndSay(4) == '1211'
for i in range(1, 10):
    print(Solution().countAndSay(i))

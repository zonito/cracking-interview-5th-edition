
def right_rotate(lst, k):
    if not lst:
        return []
    k = k % len(lst)
    return lst[-k:] + lst[:k]


assert right_rotate(['right', 'rotate', 'python'], 4) == ['python', 'right', 'rotate']

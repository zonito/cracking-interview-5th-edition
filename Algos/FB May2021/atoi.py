'''
Implement the myAtoi(string s) function, which converts a string to a 32-bit signed integer (similar to C/C++'s atoi function).

The algorithm for myAtoi(string s) is as follows:

    Read in and ignore any leading whitespace.
    Check if the next character (if not already at the end of the string) is '-' or '+'. Read this character in if it is either. This determines if the final result is negative or positive respectively. Assume the result is positive if neither is present.
    Read in next the characters until the next non-digit charcter or the end of the input is reached. The rest of the string is ignored.
    Convert these digits into an integer (i.e. "123" -> 123, "0032" -> 32). If no digits were read, then the integer is 0. Change the sign as necessary (from step 2).
    If the integer is out of the 32-bit signed integer range [-231, 231 - 1], then clamp the integer so that it remains in the range. Specifically, integers less than -231 should be clamped to -231, and integers greater than 231 - 1 should be clamped to 231 - 1.
    Return the integer as the final result.

Note:

    Only the space character ' ' is considered a whitespace character.
    Do not ignore any characters other than the leading whitespace or the rest of the string after the digits.
'''


class Solution:
    def atoi(self, string):
        ans = 0
        string = string.strip()
        if not string:
            return ans
        is_neg = string[0] == '-'
        string = string[1:] if string[0] in ['+', '-'] else string
        for char in string:
            num = ord(char) - ord('0')
            if num < 0 or num > 9:
                break
            ans = ans * 10 + num
            if ans >= 2 ** 31:
                ans = 2 ** 31
                if not is_neg:
                    ans -= 1
                break
        ans = ans * -1 if is_neg else ans
        # print(string, ans)
        return ans


assert Solution().atoi('42') == 42
assert Solution().atoi('   -42') == -42
assert Solution().atoi('4193 with words') == 4193
assert Solution().atoi('words and 987') == 0
assert Solution().atoi('-91283472332') == -2 ** 31
assert Solution().atoi('') == 0

''' https://leetcode.com/problems/vertical-order-traversal-of-a-binary-tree/ '''

import collections
from binarytree import build, Node as TreeNode


class Solution:
    def __init__(self):
        self.output = collections.defaultdict(dict)

    def dfs(self, root: TreeNode, index: int, level: int):
        if not root:
            return
        # print(root)
        if not self.output[index].get(level):
            self.output[index][level] = [root.val]
        else:
            self.output[index][level].append(root.val)
        self.dfs(root.left, index - 1, level + 1)
        self.dfs(root.right, index + 1, level + 1)

    def verticalTraversal(self, root: TreeNode):
        self.dfs(root, 0, 0)
        start = min(self.output.keys())
        result = []
        n = len(self.output)
        while start < n:
            if not self.output[start]:
                start += 1
                continue
            keys = self.output[start].keys()
            temp = []
            for i in range(min(keys), max(keys) + 1):
                if not self.output[start].get(i):
                    continue
                temp.extend(sorted(self.output[start][i]))
            result.append(temp)
            start += 1
        return result


assert Solution().verticalTraversal(build([3, 9, 20, None, None, 15, 7])) == [[9], [3, 15], [20], [7]]
assert Solution().verticalTraversal(build([1, 2, 3, 4, 5, 6, 7])) == [[4], [2], [1, 5, 6], [3], [7]]
assert Solution().verticalTraversal(build([1, 2, 3, 4, 6, 5, 7])) == [[4], [2], [1, 5, 6], [3], [7]]
assert Solution().verticalTraversal(build([3, 1, 4, 0, 2, 2])) == [[0], [1], [3, 2, 2], [4]]



def are_they_equal(array_a, array_b):
    # Write your code here
    n = len(array_a)
    i = 0
    while i < n:
        if array_a[i] != array_b[i]:
            for j in range(i, n):
                if array_a[i] == array_b[j] and array_a[i: j+1] == array_b[i:j+1][::-1]:
                    i = j
                    break
            else:
                return False
        i += 1
    return True


assert are_they_equal([1, 2, 3, 4], [1, 4, 3, 2])
assert not are_they_equal([1, 3], [1, 2])

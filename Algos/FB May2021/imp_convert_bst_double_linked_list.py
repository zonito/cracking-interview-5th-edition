''' https://leetcode.com/explore/interview/card/facebook/52/trees-and-graphs/544/ '''

from binarytree import build, Node


class Solution:
    head = None
    prev = None

    def treeToDoublyList(self, root: Node):
        if not root:
            return None
        self.treeToDoublyListHelper(root)
        self.prev.right = self.head
        self.head.left = self.prev
        return self.head

    def treeToDoublyListHelper(self, node: Node):
        if not node:
            return
        # In Order Traversal
        self.treeToDoublyListHelper(node.left)
        if self.prev:  # Then: Attach to prev
            node.left = self.prev
            self.prev.right = node
        else:  # First: We are at the head.
            self.head = node
        self.prev = node
        self.treeToDoublyListHelper(node.right)


Solution().treeToDoublyList(build([4, 2, 5, 1, 3]))

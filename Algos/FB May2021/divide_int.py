''' https://leetcode.com/explore/interview/card/facebook/54/sorting-and-searching-3/308/ '''


class Solution:
    def divide(self, dividend: int, divisor: int):
        output = 0
        counter = 0
        if dividend < 0 and divisor < 0:
            dividend = abs(dividend)
            divisor = abs(divisor)
        sign_factor = (-1 if divisor < 0 or dividend < 0 else 1)
        if divisor in [1, -1]:
            return dividend * (-1 if divisor < 0 else 1)
        while abs(dividend) >= output:
            output += abs(divisor)
            counter += 1
        counter = (counter - 1) * sign_factor
        print(output, counter)
        return counter


print(2**31)
assert Solution().divide(-10, -6) == 1
assert Solution().divide(10, 3) == 3
assert Solution().divide(10, 6) == 1
assert Solution().divide(7, -3) == -2
assert Solution().divide(0, 1) == 0

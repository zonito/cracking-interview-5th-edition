def getMilestoneDays(revenues, milestones):
    running_sum = 0
    mi = 0
    mn = len(milestones)
    result = [-1 for _ in milestones]
    s_ms = sorted([(obj, index) for index, obj in enumerate(milestones)], key=lambda obj: obj[0])
    for index, amount in enumerate(revenues):
        running_sum += amount
        while mi < mn and running_sum >= s_ms[mi][0]:
            result[s_ms[mi][1]] = index + 1
            mi += 1
            if mi >= mn:
                break
    return result

# O(r + mlogm)


assert getMilestoneDays([10, 20, 30, 40, 50, 60, 70, 80, 90, 100], [100, 200, 500]) == [4, 6, 10]
assert getMilestoneDays([100, 200, 300, 400, 500], [300, 800, 1000, 1400]) == [2, 4, 4, 5]
assert getMilestoneDays([700, 800, 600, 400, 600, 700], [3100, 2200, 800, 2100, 1000]) == [5, 4, 2, 3, 2]

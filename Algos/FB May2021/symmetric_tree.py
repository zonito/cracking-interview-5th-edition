""" https://leetcode.com/problems/symmetric-tree/ """

import collections
from typing import List
from binarytree import build, Node as TreeNode


class Solution:
    def bfs(self, root: TreeNode):
        left_queue = collections.deque([root.left])
        right_queue = collections.deque([root.right])
        while left_queue and right_queue:
            left_pop = left_queue.popleft()
            right_pop = right_queue.popleft()
            if not left_pop and not right_pop:
                continue
            if (not left_pop and right_pop) or (left_pop and not right_pop) or (left_pop.val != right_pop.val):
                return False
            left_queue.append(left_pop.left)
            left_queue.append(left_pop.right)
            right_queue.append(right_pop.right)
            right_queue.append(right_pop.left)
        if left_queue or right_queue:
            return False
        return True

    def dfs(self, root: TreeNode, arr: List[int], is_left=False):
        if not root:
            arr.append(None)
            return
        # print(root)
        arr.append(root.val)
        if is_left:
            self.dfs(root.left, arr, True)
            self.dfs(root.right, arr, True)
        else:
            self.dfs(root.right, arr)
            self.dfs(root.left, arr)

    def isSymmetric(self, root: TreeNode, is_bfs=True):
        print(root)
        if is_bfs:
            return self.bfs(root)
        left_list = []
        self.dfs(root.left, left_list, is_left=True)
        right_list = []
        self.dfs(root.right, right_list)
        return left_list == right_list


assert Solution().isSymmetric(build([1, 2, 2, 3, 4, 4, 3]))
assert not Solution().isSymmetric(build([1, 2, 2, None, 3, None, 3]))
assert Solution().isSymmetric(build([1, 2, 2, None, 3, 3, None]))
assert Solution().isSymmetric(build([1, 2, 2, 3, 4, 4, 3]), False)
assert not Solution().isSymmetric(build([1, 2, 2, None, 3, None, 3]), False)
assert Solution().isSymmetric(build([1, 2, 2, None, 3, 3, None]), False)

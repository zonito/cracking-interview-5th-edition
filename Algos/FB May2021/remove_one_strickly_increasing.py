from typing import List


class Solution:
    def canBeIncreasing(self, nums: List[int]):
        min_count = max_count = 0
        end = len(nums) - 1

        min_no = float('inf')
        max_no = float('-inf')

        while end >= 0:
            if nums[end] < min_no:
                min_no = nums[end]
            else:
                min_count += 1
            end -= 1

        for i in range(0, len(nums)):
            if nums[i] > max_no:
                max_no = nums[i]
            else:
                max_count += 1

        return not (min_count > 1 and max_count > 1)


assert Solution().canBeIncreasing(nums=[1, 2, 3])
assert Solution().canBeIncreasing(nums=[105, 924, 32, 968])
assert Solution().canBeIncreasing(nums=[1, 2, 10, 5, 7])
assert not Solution().canBeIncreasing(nums=[2, 3, 1, 2])
assert not Solution().canBeIncreasing(nums=[1, 1, 1])

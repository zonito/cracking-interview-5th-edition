""" https://leetcode.com/explore/interview/card/facebook/52/trees-and-graphs/274/ """

from typing import List


class Solution:
    directions = ((-1, 0), (0, -1), (1, 0), (0, 1))
    visited = {}

    def __init__(self):
        self.visited = {}

    def getNumIslands(self, grid: List[List[str]], location: tuple[int]):
        if grid[location[0]][location[1]] == "0" or self.visited.get(location):
            return False
        x, y = location
        self.visited[location] = True
        n = len(grid)
        m = len(grid[0])
        for d in self.directions:
            dx = d[0] + x
            dy = d[1] + y
            if dx >= 0 and dy >= 0 and dx < n and dy < m:
                self.getNumIslands(grid, (dx, dy))
        return True

    def numIslands(self, grid: List[List[str]]):
        # Comments
        len_n = len(grid)
        len_m = len(grid[0])
        islands = 0
        for rind in range(len_n):
            for cind in range(len_m):
                if self.getNumIslands(grid, (rind, cind)):
                    islands += 1
        print(islands)
        return islands


Solution().numIslands([["1", "1", "1"], ["1", "1", "0"], ["1", "1", "0"]])

Solution().numIslands(
    [
        ["1", "1", "1", "1", "0"],
        ["1", "1", "0", "1", "0"],
        ["1", "1", "0", "0", "0"],
        ["0", "0", "0", "0", "0"],
    ]
)
Solution().numIslands(
    [
        ["1", "1", "0", "0", "0"],
        ["1", "1", "0", "0", "0"],
        ["0", "0", "1", "0", "0"],
        ["0", "0", "0", "1", "1"],
    ]
)

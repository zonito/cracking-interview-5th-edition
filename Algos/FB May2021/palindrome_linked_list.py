# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def get_size(self, head: ListNode):
        node = head
        counter = 1
        while node.next:
            node = node.next
            counter += 1
        return counter

    def isPalindrome(self, head: ListNode):
        size = self.get_size(head)
        mid = size // 2
        node = head
        prev = node
        index = 0
        while index < mid:
            temp = node.next
            node.next = prev
            prev = node
            node = temp
            index += 1
        if size % 2:
            node = node.next
        while prev and node:
            if node.val != prev.val:
                return False
            node = node.next
            prev = prev.next
        return True


assert Solution().isPalindrome(ListNode(1, ListNode(2, ListNode(3, ListNode(2, ListNode(1))))))
assert Solution().isPalindrome(ListNode(1, ListNode(2, ListNode(2, ListNode(1)))))

""" https://leetcode.com/problems/group-shifted-strings/"""

import collections
import functools
from typing import List


class Solution:

    def groupStrings(self, strings: List[str]):
        """Copied: https://leetcode.com/problems/group-shifted-strings/discuss/282285/Python-Solution-with-Explanation-(44ms-84)"""
        hashmap = {}
        for s in strings:
            key = ()
            for i in range(len(s) - 1):
                # We need to watch out for the "wraparound" case - for example, 'az' and 'ba' should map to the same "shift group" as a + 1 = b and z + 1 = a. Given the above point, the respective tuples would be (25,) (122 - 97) and (-1,) (79 - 80) and az and ba would map to different groups. This is incorrect.
                # To account for this case, we add 26 to the difference between letters (smallest difference possible is -25, za) and mod by 26. So, (26 + 122 - 97) % 26 and (26 + 79 - 80) % 26 both equal (25,)
                circular_difference = 26 + ord(s[i+1]) - ord(s[i])
                key += (circular_difference % 26,)
            # The key can be represented as a tuple of the "differences" between adjacent characters. Characters map to integers (e.g. ord('a') = 97). For example, 'abc' maps to (1,1) because ord('b') - ord('a') = 1 and ord('c') - ord('b') = 1
            hashmap[key] = hashmap.get(key, []) + [s]
        return list(hashmap.values())


assert Solution().groupStrings(['abc', 'bcd', 'acef', 'xyz', 'aeb', 'az', 'ba', 'a', 'z']) == [
    ['abc', 'bcd', 'xyz'], ['acef'], ['aeb'], ['az', 'ba'], ['a', 'z']]
assert Solution().groupStrings(['a']) == [['a']]

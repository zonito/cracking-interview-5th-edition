
import collections


class Solution:
    def match_phrase(self, phrase, statement):
        phrases = phrase.split()
        pn = len(phrases)
        sn = len(statement)
        max_i = 0
        for si in range(sn):
            pi = 0
            while si < sn and pi < pn and statement[si] == phrases[pi]:
                si += 1
                pi += 1
            max_i = max(pi, max_i)
        return max_i == pn

    def count_topic_occurences(self, topics, reviews):
        result = dict([[key, 0] for key in topics])

        def remove_special_chars(word):
            return ''.join(list(filter(lambda c: c.isalpha() or ord('0') <= ord(c) <= ord('9'), word)))

        for index in range(len(reviews)):
            reviews[index] = [remove_special_chars(obj) for obj in reviews[index].lower().split()]

        for review in reviews:
            for key, values in topics.items():
                for value in values:
                    if value in review or (len(value.split()) > 1 and self.match_phrase(value, review)):
                        result[key] += 1
                        break
        print(result)
        return result


assert Solution().count_topic_occurences(
    {
        'Prices': ['cheap', 'expensive', 'price'],
        'Biz': ['genom', 'genoms'],
        'love sharma': ['love sharma']
    },
    ['Love love Sharma, is a b1oy!', 'genom! cheap is a gre1at genoms!', 'genoms is a great!'])

''' https://leetcode.com/problems/all-nodes-distance-k-in-binary-tree/ '''

import collections
from binarytree import build, Node as TreeNode


class Solution:
    def set_parent(self, node: TreeNode, parent: TreeNode = None):
        if not node:
            return
        if parent:
            node.parent = parent
        self.set_parent(node.left, node)
        self.set_parent(node.right, node)

    def distanceK(self, root: TreeNode, target: TreeNode, k: int):
        print(root)
        root.parent = None
        self.set_parent(root)   # core

        queue = collections.deque()
        queue.append((target, 0))
        visited = {target}
        while queue:
            if queue[0][1] == k:    # By this time, we should have k distanced in the queue
                return [obj[0].val for obj in queue]
            node, d = queue.popleft()
            for nei in (node.left, node.right, node.parent):
                if nei and nei not in visited:
                    visited.add(nei)
                    queue.append((nei, d + 1))
        return []


tree = build([3, 5, 1, 6, 2, 0, 8, None, None, 7, 4])
assert Solution().distanceK(tree, tree.left, 2) == [7, 4, 1]

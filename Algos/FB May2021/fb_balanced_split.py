
import itertools


def balancedSplitExists(arr):
    arr.sort()  # 1,1,5,7
    running_sum_arr = list(itertools.accumulate(arr))   # 1,2,7,14
    i = len(arr) - 1    # 3
    running_sum = 0
    while i > 1:
        running_sum += arr[i]
        if running_sum == running_sum_arr[i - 1] and arr[i - 1] != arr[i]:
            return True
        i -= 1
    return False


assert balancedSplitExists([1, 5, 7, 1])
assert not balancedSplitExists([12, 7, 6, 7, 6])

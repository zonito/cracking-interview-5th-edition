''' https://leetcode.com/explore/interview/card/facebook/53/recursion-3/3029/ '''


class Solution:
    num_sets = {
        0: [],
        1: ['1', '8', '0'],
        2: ['11', '88', '69', '96', '00']
    }
    singlets = ['1', '8', '0']

    def getStrobogrammatic(self, n: int):
        if n <= 2:
            return self.num_sets[n]
        pairs = self.getStrobogrammatic(n - 2)
        result = [num + pair + num for num in self.singlets for pair in pairs]
        for pair in pairs:
            result.extend(['6' + pair + '9', '9' + pair + '6'])
        return result

    def findStrobogrammatic(self, n: int):
        if n <= 2:
            return self.num_sets[n]
        return sorted([obj for obj in self.getStrobogrammatic(n) if obj[0] != '0'])


assert Solution().findStrobogrammatic(1) == ['1', '8', '0']
assert Solution().findStrobogrammatic(2) == ['11', '88', '69', '96', '00']
assert Solution().findStrobogrammatic(3) == ["101", "111", "181", "609", "619", "689", "808", "818", "888", "906", "916", "986"]
assert Solution().findStrobogrammatic(4) == ["1001", "1111", "1691", "1881", "1961", "6009", "6119", "6699", "6889", "6969", "8008", "8118", "8698", "8888", "8968", "9006", "9116", "9696", "9886", "9966"]

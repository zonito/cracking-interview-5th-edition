
from typing import List


class Solution(object):
    def remove_dups(self, arr: List[int]):
        ''' Remove Dups in-place because arr is pass by reference object. '''
        if not arr:
            return 0
        replace_index = 1
        total_elements = len(arr)
        index = 0
        counter = 1
        while index < total_elements:
            while index + 1 < total_elements and arr[index] == arr[index + 1]:
                index += 1
            if index + 1 >= total_elements:
                break
            arr[replace_index] = arr[index + 1]
            replace_index += 1
            index += 1
            counter += 1
        print(arr, counter)
        return counter


assert Solution().remove_dups([]) == 0
assert Solution().remove_dups([0]) == 1
assert Solution().remove_dups([1, 1]) == 1
assert Solution().remove_dups([1, 1, 2]) == 2
assert Solution().remove_dups([1, 1, 1, 2]) == 2
assert Solution().remove_dups([0, 0, 1, 1, 1, 2, 2, 3, 3, 4]) == 5
assert Solution().remove_dups([0] * 10) == 1

import heapq


def find_second_maximum(lst):
    return heapq.nlargest(2, lst)[-1]  # O(n * log(t))


assert find_second_maximum([9, 2, 3, 6]) == 6

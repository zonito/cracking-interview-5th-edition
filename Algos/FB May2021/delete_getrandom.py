''' https://leetcode.com/problems/insert-delete-getrandom-o1/ '''


import random


class RandomizedSet:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.arr = []
        self.hash = {}
        self.idx_counter = 0

    def insert(self, val: int):
        """
        Inserts a value to the set. Returns true if the set did not already contain the specified element.
        """
        if val in self.hash:
            return False
        self.arr.append(val)
        self.hash[val] = self.idx_counter
        self.idx_counter += 1
        return True

    def remove(self, val: int):
        """
        Removes a value from the set. Returns true if the set contained the specified element.
        """
        if val not in self.hash:
            return False
        old_index = self.hash[val]
        self.hash[self.arr[self.idx_counter - 1]] = old_index
        # Swap with Last Element
        self.arr[old_index], self.arr[self.idx_counter - 1] = self.arr[self.idx_counter - 1], self.arr[old_index]
        # Remove
        self.arr.pop()
        self.idx_counter -= 1
        del self.hash[val]
        return True

    def getRandom(self):
        """
        Get a random element from the set.
        """
        return self.arr[random.randint(0, self.idx_counter - 1)]


# Your RandomizedSet object will be instantiated and called as such:
obj = RandomizedSet()
obj.insert(3)
obj.insert(3)
print(obj.getRandom())
print(obj.getRandom())
obj.insert(1)
obj.remove(3)
print(obj.getRandom())
print(obj.getRandom())
obj.insert(0)
obj.remove(0)


obj = RandomizedSet()
obj.remove(0)
obj.remove(0)
obj.insert(0)
print(obj.getRandom())
obj.remove(0)
obj.insert(0)

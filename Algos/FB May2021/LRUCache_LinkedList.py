class DllNode:
    def _init_(self, key, value):
        self.key = key
        self.value = value
        self.next = None
        self.prev = None


class Dll:
    def _init_(self):
        self.tail = DllNode(0, 0)
        self.head = DllNode(0, 0)
        self.tail.prev = self.head
        self.head.next = self.tail

    def isEmpty(self):
        return self.head == self.tail

    def getFirstNode(self):
        if self.isEmpty():
            return None
        return self.head.next

    def getLastNode(self):
        if self.isEmpty():
            return None
        return self.tail.prev

    def detachNode(self, node):
        node.prev.next = node.next
        node.next.prev = node.prev

    def moveNodeToFront(self, node):
        node.next = self.head.next
        node.prev = self.head
        self.head.next.prev = node
        self.head.next = node


class LRUCache:

    def _init_(self, capacity: int):
        self.capacity = capacity
        self.mapper = {}
        self.dll = Dll()
        self.size = 0

    def get(self, key: int) -> int:
        if key in self.mapper:
            node = self.mapper[key]
            self.dll.detachNode(node)
            self.dll.moveNodeToFront(node)
            return node.value
        return -1

    def put(self, key: int, value: int) -> None:
        if key in self.mapper:
            node = self.mapper[key]
            node.value = value
            self.dll.detachNode(node)
            self.dll.moveNodeToFront(node)
            return

        node = DllNode(key, value)
        if self.size >= self.capacity:
            self.evictKey()

        self.mapper[key] = node
        self.dll.moveNodeToFront(node)
        self.size += 1

        # Key already exists
        # Capacity is full
        # Add the key, val to store and make it MRU
            # Move the key to the head of the Dll

    def evictKey(self):
        last = self.dll.getLastNode()
        keyToEvict = last.key
        del self.mapper[keyToEvict]
        self.dll.detachNode(last)
        return last.key

# Your LRUCache object will be instantiated and called as such:
# obj = LRUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)

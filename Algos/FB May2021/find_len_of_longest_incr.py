""" https://leetcode.com/problems/longest-continuous-increasing-subsequence/
    """

from typing import List


class Solution:
    def findLengthOfLCIS(self, nums: List[int]):
        cnt = 1
        i = 0
        n = len(nums)
        max_cnt = 1
        while i < n - 1:
            if nums[i + 1] > nums[i]:
                cnt += 1
            else:
                max_cnt = max(max_cnt, cnt)
                cnt = 1
            i += 1
        return max(max_cnt, cnt)


assert Solution().findLengthOfLCIS([1, 3, 5, 4, 7]) == 3
assert Solution().findLengthOfLCIS([1, 3, 5, 7]) == 4
assert Solution().findLengthOfLCIS([1]) == 1

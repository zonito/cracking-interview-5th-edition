""" https://leetcode.com/problems/find-all-duplicates-in-an-array/"""

from typing import List


class Solution:
    def findDuplicates(self, nums: List[int]):
        result = []
        for num in nums:
            num = abs(num)
            if nums[num - 1] < 0:
                result.append(num)
            nums[num - 1] *= -1
        return result


assert Solution().findDuplicates([4, 3, 2, 7, 8, 2, 3, 1]) == [2, 3]


class Solution(object):
    def get_long_str(self, s):
        char_bucket = {}  # Stores as {key: index}
        long_len = 0
        start_index = 0
        for index, char in enumerate(s):
            if char_bucket.get(char):
                start_index = max(char_bucket[char], start_index)
            long_len = max(long_len, index - start_index + 1)
            char_bucket[char] = index + 1
        # print(s, long_len)
        return long_len


assert Solution().get_long_str('abcabcbb') == 3
assert Solution().get_long_str('bbbbb') == 1
assert Solution().get_long_str('pwwkew') == 3
assert Solution().get_long_str(' ') == 1

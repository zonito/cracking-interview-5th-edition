""" https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree """

from binarytree import build, Node as TreeNode


class Solution:
    def lowestCommonAncestor(self, root: TreeNode, p: TreeNode, q: TreeNode):
        # print(root, p, q)
        if not root:
            return
        if root == p:
            return p
        elif root == q:
            return q
        left_node = self.lowestCommonAncestor(root.left, p, q)
        right_node = self.lowestCommonAncestor(root.right, p, q)
        if not left_node and not right_node:
            return None
        elif left_node and right_node:
            return root
        return left_node if left_node else right_node


tree = build([3, 5, 1, 6, 2, 0, 8, None, None, 7, 4])
assert Solution().lowestCommonAncestor(tree, tree.left, tree.right) == tree
assert (
    Solution().lowestCommonAncestor(tree, tree.left, tree.left.right.right) == tree.left
)

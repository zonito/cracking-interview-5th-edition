''' https://www.facebookrecruiting.com/portal/coding_practice_question/?problem_id=840934449713537 '''


from collections import defaultdict


def numberOfWays(arr, k):
    storage = defaultdict(int)
    output = 0
    for num in arr:
        output += storage[k - num]
        storage[num] += 1
    return output


assert numberOfWays([1, 2, 3, 4, 3], 6) == 2
assert numberOfWays([1, 5, 3, 3, 3], 6) == 4

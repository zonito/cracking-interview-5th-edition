""" https://leetcode.com/problems/find-k-closest-elements/
    """

from typing import List


class Solution:
    def findClosestElements(self, arr: List[int], k: int, x: int):
        return sorted(sorted(arr, key=lambda obj: abs(obj - x))[:k])


def merge_lists(lst1, lst2):
    i = 0
    j = 0
    result = []
    while lst2 and lst1:
        if lst1[i] > lst2[j]:
            result.append(lst2.pop(0))
        else:
            result.append(lst1.pop(0))
    result.extend(lst1)
    result.extend(lst2)
    return result


assert merge_lists([1, 3, 4, 5], [2, 6, 7, 8]) == [1, 2, 3, 4, 5, 6, 7, 8]

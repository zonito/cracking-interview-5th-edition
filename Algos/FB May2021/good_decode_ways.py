"""
https://leetcode.com/explore/interview/card/facebook/55/dynamic-programming-3/264/
https://leetcode.com/problems/decode-ways/
"""


class Solution:
    def __init__(self):
        self.memo = {}

    def getWays(self, index: int, s: str):
        if self.memo.get(index):
            return self.memo[index]
        if index == len(s):  # Last Digit, Yes Possible Way!
            return 1
        if s[index] == '0':  # 0 < 1, 0 Possible
            return 0
        if index == len(s) - 1:  # With condition that last 2 digits <= 26, then Yes, possible way!
            return 1
        ans = self.getWays(index + 1, s)
        if int(s[index:index + 2]) <= 26:
            ans += self.getWays(index + 2, s)
        self.memo[index] = ans
        return ans

    def numDecodings(self, s: str):
        return self.getWays(0, s)


# assert Solution().numDecodings('12') == 2
assert Solution().numDecodings('226') == 3
# Solution().numDecodings('0') == 0
# Solution().numDecodings('06') == 0

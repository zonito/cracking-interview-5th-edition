''' https://leetcode.com/problems/design-add-and-search-words-data-structure/ '''

import collections
import json


class Trie:
    def __init__(self):
        self.trie = collections.defaultdict(dict)

    def insert(self, word: str):
        trie = self.trie
        for char in word:
            trie[char] = trie.get(char, {})
            trie = trie[char]
        trie['*'] = '*'

    def is_valid_query(self, query: str, trie: dict = None):
        trie = trie or self.trie
        for index, char in enumerate(query):
            if char != '.':
                trie = trie.get(char, {})
                continue
            for key in trie:
                if key == '*':
                    continue
                if self.is_valid_query(query[index + 1:], trie[key]):
                    return True
            else:
                return False
        return trie and trie.get('*') == '*'


class WordDictionary:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.trie_ins = Trie()

    def addWord(self, word: str):
        self.trie_ins.insert(word)

    def search(self, word: str):
        return bool(self.trie_ins.is_valid_query(word))


# Your WordDictionary object will be instantiated and called as such:
wd = WordDictionary()
wd.addWord("bat")
wd.addWord("bad")
wd.addWord("dad")
wd.addWord("mad")
print(json.dumps(wd.trie_ins.trie, indent=2))
assert not wd.search("pad")  # return False
assert wd.search("bad")  # return True
assert wd.search(".ad")  # return True
assert wd.search("b..")  # return True
assert wd.search("b.d")  # return True
assert not wd.search("b...")  # return True

# Your WordDictionary object will be instantiated and called as such:
wd = WordDictionary()
wd.addWord("a")
wd.addWord("ab")
print(json.dumps(wd.trie_ins.trie, indent=2))
assert wd.search("a")
assert wd.search("a.")
assert wd.search("ab")
assert not wd.search(".a")
assert wd.search(".b")
assert not wd.search("ab.")
assert wd.search(".")
assert wd.search("..")

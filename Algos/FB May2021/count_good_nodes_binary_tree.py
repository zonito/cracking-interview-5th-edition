
import sys
from binarytree import build, Node as TreeNode


class Solution:
    def dfs(self, node: TreeNode, root_val: int = -(sys.maxsize-1)):
        if not node:
            return 0
        root_val = max(root_val, node.val)
        val = 1 if root_val <= node.val else 0
        left = self.dfs(node.left, root_val)
        right = self.dfs(node.right, root_val)
        return left + right + val

    def goodNodes(self, root: TreeNode):
        return self.dfs(root)


assert Solution().goodNodes(build([-3, 3, None, -4, 2])) == 2
assert Solution().goodNodes(build([3, 1, 4, 3, None, 1, 5])) == 4
assert Solution().goodNodes(build([3, 3, None, 4, 2])) == 3
assert Solution().goodNodes(build([3])) == 1

'''
Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.

Notice that the solution set must not contain duplicate triplets.
'''

from typing import List


class Solution:
    def _get_2_sum(self, arr: List[int], target: int):
        '''Return Array of 2 items which totals at target.'''
        result = []
        seen = set()
        for index in range(len(arr)):
            diff = target - arr[index]
            if diff in seen:
                result.append([diff, arr[index]])
            seen.add(arr[index])
        return result

    def get_3_sum(self, arr: List[int]):
        result = set()
        # arr.sort()
        for index in range(len(arr)):
            for item in self._get_2_sum(arr[index + 1:], arr[index] * -1):
                result.add(tuple(sorted([arr[index]] + item)))
        return result


assert Solution().get_3_sum([-1, 0, 1, 2, -1, -4]) == {(-1, 0, 1), (-1, -1, 2)}

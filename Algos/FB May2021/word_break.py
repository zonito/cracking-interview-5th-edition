""" https://leetcode.com/explore/interview/card/facebook/55/dynamic-programming-3/3036/ """


from typing import List


class Solution:
    def __init__(self):
        self.trie = {}

    def make_trie(self, words):
        root = dict()
        for word in words:
            current_dict = root
            for letter in word:
                current_dict = current_dict.setdefault(letter, {})
            current_dict['*'] = '*'
        return root

    def is_word(self, word):
        root = self.trie
        for c in word:
            if root.get(c):
                root = root[c]
                continue
            return False
        return root.get('*') == '*'

    def is_breakable(self, s: str):
        if not s:
            return True
        for i in range(len(s) - 1, -1, -1):
            if self.is_word(s[:i]) and self.is_breakable(s[i:]):
                return True
        return False

    def wordBreak(self, s: str, wordDict: List[str]):
        self.trie = self.make_trie(wordDict)
        return self.is_breakable(s)


assert Solution().wordBreak('leetcode', ['leet', 'code'])
assert Solution().wordBreak('applepenapple', ['apple', 'pen'])
assert not Solution().wordBreak('catsandog', ['cats', 'sand', 'dog', 'and', 'cat'])
""" Failed / Time Exceeded Test Cases:
"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"]
"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"]
"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabaabaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
["aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa","ba"]
"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabab"
["a","aa","ba"]
"""

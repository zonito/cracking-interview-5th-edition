''' https://leetcode.com/problems/merge-intervals/ '''


from typing import List


class Solution:
    def merge(self, intervals: List[List[int]]):
        n = len(intervals)
        i = 1
        intervals = sorted(intervals, key=lambda obj: obj[0])
        while i < n:
            last = intervals[i - 1]
            if (last[0] <= intervals[i][0] and last[1] >= intervals[i][0]) or (
                last[0] <= intervals[i][1] and last[1] >= intervals[i][1]
            ):
                intervals[i - 1] = [min(last[0], intervals[i][0]), max(intervals[i][1], last[1])]
                intervals.pop(i)
                n -= 1
                continue
            i += 1
        return intervals


assert Solution().merge([[1, 3], [2, 6], [8, 10], [15, 18]]) == [[1, 6], [8, 10], [15, 18]]
assert Solution().merge([[1, 4], [4, 5]]) == [[1, 5]]
assert Solution().merge([[1, 4], [0, 4]]) == [[0, 4]]
assert Solution().merge([[1, 4], [0, 1]]) == [[0, 4]]
assert Solution().merge([[1, 4], [0, 5]]) == [[0, 5]]
assert Solution().merge([[1, 4], [0, 0]]) == [[0, 0], [1, 4]]
assert Solution().merge([[1, 4], [0, 1], [0, 2], [4, 10], [9, 15], [8, 20]]) == [[0, 20]]

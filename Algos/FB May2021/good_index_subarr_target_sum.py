
from typing import List


def get_indexes_sum(arr: List[int], target: int):
    seen = {}
    running_sum = 0
    for index, num in enumerate(arr):
        running_sum += num
        if running_sum == target:
            return 0, index
        diff = target - running_sum
        if diff in seen:
            return seen[diff] + 1, index + 1
        seen[running_sum] = index
    return -1, -1


assert get_indexes_sum([1, 4, 20, 3, 10, 5], 33) == (2, 4)
assert get_indexes_sum([10, 2, -2, -20, 10], -10) == (0, 3)

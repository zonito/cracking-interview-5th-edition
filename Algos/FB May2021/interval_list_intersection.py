''' https://leetcode.com/problems/interval-list-intersections/ '''

from typing import List


class Solution:

    def intervalIntersection(self, A: List[List[int]], B: List[List[int]]):
        i = 0
        j = 0
        result = []
        while i < len(A) and j < len(B):
            a_start, a_end = A[i]
            b_start, b_end = B[j]
            if a_start <= b_end and b_start <= a_end:                       # Criss-cross lock
                result.append([max(a_start, b_start), min(a_end, b_end)])   # Squeezing
            if a_end <= b_end:         # Exhausted this range in A
                i += 1               # Point to next range in A
            else:                      # Exhausted this range in B
                j += 1               # Point to next range in B
        return result


assert Solution().intervalIntersection(
    [[0, 2], [5, 10], [13, 23], [24, 25]],
    [[1, 5], [8, 12], [15, 24], [25, 26]]
) == [[1, 2], [5, 5], [8, 10], [15, 23], [24, 24], [25, 25]]
assert Solution().intervalIntersection([[1, 3], [5, 9]], []) == []
assert Solution().intervalIntersection([], [[4, 8], [10, 12]]) == []
assert Solution().intervalIntersection([[1, 7]], [[3, 10]]) == [[3, 7]]

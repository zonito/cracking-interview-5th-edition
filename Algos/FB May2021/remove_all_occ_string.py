class Solution:
    def removeOccurrences(self, s: str, part: str):
        si = 0
        pn = len(part)
        while si < len(s) - pn + 1:
            if s[si:si+pn] == part:
                s = s[:si] + s[si+pn:]
                si = max(0, si-pn)
            else:
                si += 1
        return s


assert Solution().removeOccurrences('daabcbaabcbc', 'abc') == 'dab'
assert Solution().removeOccurrences('axxxxyyyyb', 'xy') == 'ab'

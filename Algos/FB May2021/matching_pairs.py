""" https://www.facebookrecruiting.com/portal/coding_practice_question/?problem_id=559324704673058 """

import collections
import json


def matching_pairs(s, t):
    i = 0
    n = min(len(s), len(t))
    result = 0
    spending = collections.defaultdict(list)
    tpending = collections.defaultdict(list)
    while i < n:
        if s[i] == t[i]:
            result += 1
        else:
            spending[s[i]].append(i)
            tpending[t[i]].append(i)
        i += 1

    delta = 0
    for sk in spending:
        if sk in tpending:
            delta += 1
            if delta == 2:
                break
    return result + (delta or -2)


assert matching_pairs('mno', 'mon') == 3
assert matching_pairs('mno', 'mno') == 1
assert matching_pairs('abcd', 'adcb') == 4
assert matching_pairs('abcde', 'adcbe') == 5



from typing import List


def remove_even(lst: List[int]):
    return list(filter(lambda n1: n1 % 2 != 0, lst))


assert remove_even([1, 2, 4, 5, 10, 6, 3]) == [1, 5, 3]

''' https://leetcode.com/problems/accounts-merge/solution/ '''


from collections import defaultdict
from typing import List


class DSU:
    def __init__(self, n):
        self.p = list(range(n))

    def find(self, a):
        if self.p[a] != a:
            self.p[a] = self.find(self.p[a])
        return self.p[a]

    def union(self, a, b):
        self.p[self.find(a)] = self.find(b)


class Solution:
    def accountsMerge(self, accounts: List[List[str]]):
        email_ids = defaultdict(list)
        for index, account in enumerate(accounts):
            for email in account[1:]:
                email_ids[email].append(index)
        dsu = DSU(len(accounts))
        for indexes in email_ids.values():
            for eid in indexes[1:]:
                dsu.union(indexes[0], eid)

        merged_accounts = defaultdict(set)
        for i, account in enumerate(accounts):
            merged_accounts[dsu.find(i)].update(account[1:])

        result = [
            [accounts[i][0]] + sorted(emails)
            for i, emails in merged_accounts.items()]
        print(result)
        return result


assert Solution().accountsMerge([
    ["John", "johnsmith@mail.com", "john_newyork@mail.com"],
    ["John", "johnsmith@mail.com", "john00@mail.com"],
    ["John", "johnx@mail.com", "john00@mail.com"],
    ["Mary", "mary@mail.com"],
    ["John", "johnnybravo@mail.com"]
]) == [
    ['John', 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.com', 'johnx@mail.com'],
    ['Mary', 'mary@mail.com'],
    ['John', 'johnnybravo@mail.com']
]

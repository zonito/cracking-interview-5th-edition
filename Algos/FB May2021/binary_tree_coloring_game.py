
from binarytree import build, Node as TreeNode


class Solution:
    def btreeGameWinningMove(self, root: TreeNode, n: int, x: int):
        player_1 = []

        def search(node):
            if not node:
                return 0
            left = search(node.left)
            right = search(node.right)
            if node.val == x:
                player_1.append(left)
                player_1.append(right)
            return left + right + 1

        search(root)
        if sum(player_1) + 1 < n / 2:  # case1 - in parent
            return True
        if max(player_1) > n - max(player_1):  # case2 - in children
            return True
        return False


assert Solution().btreeGameWinningMove(build([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]), 11, 3)

''' https://leetcode.com/explore/interview/card/facebook/52/trees-and-graphs/3026/ '''

from collections import defaultdict
import sys
from typing import List, Tuple


class Solution:
    directions = ((-1, 0), (1, 0), (0, -1), (0, 1))

    def dfs(self, grid: List[List[int]], location: Tuple[int], distance, depth: int):
        m = len(grid)
        n = len(grid[0])
        for d in self.directions:
            dx = location[0] + d[0]
            dy = location[1] + d[1]
            if (dx >= 0 and dx < m and dy >= 0 and dy < n and
                    (not distance[(dx, dy)] or distance[(dx, dy)] > depth) and grid[dx][dy] == 0):
                distance[(dx, dy)] = depth
                self.dfs(grid, (dx, dy), distance, depth + 1)

    def shortestDistance(self, grid: List[List[int]]):
        # Get the list of buidings and obstacles
        m = len(grid)
        n = len(grid[0])
        buildings = [
            (i, j) for i in range(m)
            for j in range(n) if grid[i][j] == 1]
        distance = {}
        for building in buildings:
            distance[building] = defaultdict(bool)
            self.dfs(grid, building, distance[building], 1)

        # min_distance = sys.maxsize - 1
        total = defaultdict(int)
        counter = defaultdict(int)
        for building in buildings:
            for d in distance[building]:
                if distance[building][d]:
                    total[d] += distance[building][d]
                    counter[d] += 1
        # print(total)
        # print(counter)
        total_buildings = len(buildings)
        result = [total[d] for d, v in counter.items() if v == total_buildings]
        return min(result) if result else -1


assert Solution().shortestDistance([
    [1, 2, 0]
]) == -1

assert Solution().shortestDistance([
    [1, 1, 2, 1, 1],
    [1, 1, 0, 1, 1],
    [1, 1, 1, 1, 1]
]) == -1
assert Solution().shortestDistance([
    [1, 0, 2, 0, 1],
    [0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0]
]) == 7
assert Solution().shortestDistance([
    [1, 0]
]) == 1
assert Solution().shortestDistance([
    [1]
]) == -1

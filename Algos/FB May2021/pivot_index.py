''' https://leetcode.com/problems/find-pivot-index/ '''


from typing import List


class Solution:
    def pivotIndex(self, nums: List[int]):
        left = 0
        right = sum(nums)
        for i in range(len(nums)):
            if i - 1 >= 0:
                left += nums[i - 1]
            right -= nums[i]
            if left == right:
                return i
        return -1


assert Solution().pivotIndex([-1, -1, 0, 1, 1, 0]) == 5
assert Solution().pivotIndex([1, 7, 3, 6, 5, 6]) == 3
assert Solution().pivotIndex([1, 2, 3]) == -1
assert Solution().pivotIndex([2, 1, -1]) == 0

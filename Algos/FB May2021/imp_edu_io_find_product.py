def find_product(lst):
    result = []
    i = 0
    size = len(lst)
    prev = 1
    while i < size:
        result.append(prev)
        prev *= lst[i]
        i += 1
    prev = 1
    i = size - 1
    while i >= 0:
        result[i] *= prev
        prev *= lst[i]
        i -= 1
    return result


assert find_product([1, 2, 3, 4]) == [24, 12, 8, 6]
assert find_product([4, 2, 1, 5, 0]) == [0, 0, 0, 0, 40]

"""
1  2  3  4
1  1  2  6
24 12 4  1
24 12 8  6
"""

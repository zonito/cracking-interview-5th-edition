""" https://leetcode.com/problems/binary-tree-paths/solution/ """

from binarytree import build, Node as TreeNode
from typing import List


class Solution:
    paths = None

    def __init__(self):
        self.paths = []

    def findBinaryTreePaths(self, root: TreeNode, parents: List = None):
        # print(root)
        if not root:
            return
        parents = parents or []
        if not root.left and not root.right:
            self.paths.append("->".join(map(str, parents + [root.val])))
            return
        self.findBinaryTreePaths(root.left, parents + [root.val])
        self.findBinaryTreePaths(root.right, parents + [root.val])
        return

    def binaryTreePaths(self, root: TreeNode):
        self.findBinaryTreePaths(root)
        return self.paths


assert Solution().binaryTreePaths(build([1, 2, 3, None, 5])) == ['1->2->5', '1->3']

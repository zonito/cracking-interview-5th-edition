''' https://leetcode.com/problems/subtree-of-another-tree/ '''

from binarytree import build, Node as TreeNode


class Solution:
    def isSame(self, p: TreeNode, q: TreeNode):
        # print(p, q)
        if not p and not q:
            return True
        if (not p and q) or (p and not q):
            return False
        return p.val == q.val and self.isSame(p.left, q.left) and self.isSame(p.right, q.right)

    def isSubtree(self, root: TreeNode, subRoot: TreeNode):
        # print(root, subRoot)
        if not root:
            return False
        if root.val == subRoot.val and self.isSame(root, subRoot):
            return True
        return self.isSubtree(root.left, subRoot) or self.isSubtree(root.right, subRoot)


assert Solution().isSubtree(build([1, 1]), build([1]))
assert Solution().isSubtree(build([3, 4, 5, 1, 2]), build([4, 1, 2]))
assert not Solution().isSubtree(build([3, 4, 5, 1, 2, None, None, None, None, 0]), build([4, 1, 2]))

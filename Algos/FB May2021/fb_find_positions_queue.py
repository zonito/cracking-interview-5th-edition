
def findPositions(arr, k):
    if not k or not arr:
        return []
    arr = [[val, index + 1] for index, val in enumerate(arr)]
    result = []
    while len(result) != k:
        n = len(arr)
        temp = [arr.pop(0) for i in range(k) if i < n]
        max_element = temp[0]
        for i in range(1, min(k, n)):
            if temp[i][0] > max_element[0] or (temp[i][0] == max_element[0] and temp[i][1] < max_element[1]):
                if max_element[0]:
                    max_element[0] -= 1
                arr.append(max_element)
                max_element = temp[i]
            else:
                if temp[i][0]:
                    temp[i][0] -= 1
                arr.append(temp[i])
        result.append(max_element[1])
    return result


assert findPositions([1, 2, 2, 3, 4, 5], 5) == [5, 6, 4, 1, 2]

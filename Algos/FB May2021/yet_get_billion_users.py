import math


def numUsers(growthRates, t):
    return sum(map(lambda rate: rate ** t, growthRates))


def getBillionUsersDay(growthRates):
    max_rate = max(growthRates)
    max_rates_count = sum([1 for i in range(len(growthRates)) if growthRates[i] == max_rate])
    maximum_days = math.ceil(math.log(10 ** 9 / max_rates_count, max_rate))
    bounds = [0, maximum_days]

    while True:
        candidate = sum(bounds) // 2
        current_day = numUsers(growthRates, candidate)
        next_day = numUsers(growthRates, candidate + 1)

        if current_day == 10 ** 9 or (current_day < 10 ** 9 and next_day >= 10 ** 9):
            return candidate + 1

        if current_day < 10 ** 9:
            bounds = [candidate,  bounds[1]]
        else:
            bounds = [bounds[0], candidate]
    return -1


assert getBillionUsersDay([1.5]) == 52
assert getBillionUsersDay([1.1, 1.2, 1.3]) == 79
assert getBillionUsersDay([1.01, 1.02]) == 1047

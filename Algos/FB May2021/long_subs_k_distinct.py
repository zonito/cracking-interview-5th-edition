""" https://leetcode.com/problems/longest-substring-with-at-most-k-distinct-characters """

import collections


class Solution:
    def lengthOfLongestSubstringKDistinct(self, s: str, k: int):
        p1 = 0
        p2 = 0
        distinct = collections.defaultdict(int)
        max_distance = 0
        while p2 < len(s):
            if not distinct.get(s[p2]):
                distinct[s[p2]] += 1
                while len(distinct) > k:
                    max_distance = max(max_distance, p2 - p1)
                    distinct[s[p1]] -= 1
                    if not distinct[s[p1]]:
                        del distinct[s[p1]]
                    p1 += 1
            else:
                distinct[s[p2]] += 1
            p2 += 1
        return max(max_distance, p2 - p1)


# assert Solution().lengthOfLongestSubstringKDistinct('eceba', 2) == 3
assert (
    Solution().lengthOfLongestSubstringKDistinct(
        "ecdafdfdadfwewerasdavasdasdfacascsadeba", 5
    )
    == 15
)
# assert Solution().lengthOfLongestSubstringKDistinct('aa', 1) == 2

''' https://leetcode.com/explore/interview/card/facebook/6/linked-list/301/ '''


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode):
        result = ListNode()
        head = result
        while l1 and l2:
            if l1.val < l2.val:
                result.next = ListNode(l1.val)
                result = result.next
                l1 = l1.next
                continue
            result.next = ListNode(l2.val)
            result = result.next
            l2 = l2.next
        while l1:
            result.next = ListNode(l1.val)
            result = result.next
            l1 = l1.next
        while l2:
            result.next = ListNode(l2.val)
            result = result.next
            l2 = l2.next
        return head.next


Solution().mergeTwoLists(
    ListNode(1, ListNode(2, ListNode(4))),
    ListNode(1, ListNode(3, ListNode(4)))
)

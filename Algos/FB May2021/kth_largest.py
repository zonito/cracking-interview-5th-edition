''' https://leetcode.com/problems/kth-largest-element-in-an-array/ '''


from typing import List
import heapq


class Solution:
    def __init__(self):
        self.kth_arr = []

    def maintainKthElements(self, num: int, k: int):
        i = 0
        n = len(self.kth_arr)
        while i < n:  # change it to binary search
            if num > self.kth_arr[i]:
                break
            i += 1
        if i != n or n < k:
            self.kth_arr.insert(i, num)
            if n + 1 > k:
                self.kth_arr.pop()

    def findKthLargest(self, nums: List[int], k: int):
        for num in nums:
            self.maintainKthElements(num, k)
        return self.kth_arr[-1]

    def findKthLargestHeap(self, nums: List[int], k: int):
        return heapq.nlargest(k, nums)[-1]


assert Solution().findKthLargest([3, 2, 1, 5, 6, 4], 2) == 5
assert Solution().findKthLargest([3, 2, 3, 1, 2, 4, 5, 5, 6], 4) == 4
assert Solution().findKthLargestHeap([3, 2, 1, 5, 6, 4], 2) == 5
assert Solution().findKthLargestHeap([3, 2, 3, 1, 2, 4, 5, 5, 6], 4) == 4

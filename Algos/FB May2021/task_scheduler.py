"""
https://leetcode.com/problems/task-scheduler/
Copied Solution
"""

from typing import List


class Solution:
    def leastInterval(self, tasks: List[str], n: int):
        # frequencies of the tasks
        frequencies = [0] * 26
        for t in tasks:
            frequencies[ord(t) - ord('A')] += 1

        frequencies.sort()

        # max frequency
        f_max = frequencies.pop()
        idle_time = (f_max - 1) * n

        # Find the idle time
        while frequencies and idle_time > 0:
            idle_time -= min(f_max - 1, frequencies.pop())
        idle_time = max(0, idle_time)

        return idle_time + len(tasks)


assert Solution().leastInterval(['A', 'A', 'A', 'B', 'B', 'B'], 2) == 8
assert Solution().leastInterval(['A', 'A', 'A'], 2) == 7
assert Solution().leastInterval(['A', 'A', 'A'], 1) == 5
assert Solution().leastInterval(['A', 'A', 'A'], 0) == 3
assert Solution().leastInterval(['A', 'A', 'A', 'B', 'B', 'B'], 0) == 6
assert Solution().leastInterval(['A', 'A', 'A', 'A', 'A', 'A', 'B', 'C', 'D', 'E', 'F', 'G'], 2) == 16

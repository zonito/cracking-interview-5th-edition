''' https://leetcode.com/explore/interview/card/facebook/54/sorting-and-searching-3/3032/ '''


from typing import List


class Solution:
    def findPeakElement(self, nums: List[int]):
        n = len(nums)
        if n == 1:
            return 0
        peak = [nums[0], 0]
        start = 0
        end = n
        while start <= end:
            mid = start + (end - start) // 2
            if nums[mid] > peak[0]:
                peak = [nums[mid], mid]
            if nums[start] > nums[mid]:  # Peak is in this range
                end = mid - 1
            elif mid + 1 < n and nums[mid + 1] > nums[mid]:
                start = mid + 1
            else:
                end = mid - 1
        print(peak)
        return peak[1]


assert Solution().findPeakElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 0, 14, 15, 1, 18, 19, 29, 49, 9, 10, 12, 1]) == 11
assert Solution().findPeakElement([1, 2, 3, 1]) == 2
assert Solution().findPeakElement([1, 2, 1, 3, 5, 6, 4]) == 5

''' https://leetcode.com/problems/exclusive-time-of-functions/ '''

import collections
from typing import List


class Solution:
    def exclusiveTime(self, n: int, logs: List[str]):
        stack = []
        ln = len(logs)
        i = 0
        output = collections.defaultdict(int)
        while i < ln:
            fid, state, timestamp = logs[i].split(':')
            i += 1
            if state == 'start':
                stack.append([fid, int(timestamp), 0])
                continue
            _, last_timestamp, last_diff = stack.pop()
            diff = int(timestamp) - last_timestamp - last_diff + 1
            output[fid] += diff
            if stack:
                stack[-1][2] += diff + last_diff
        return [output[str(i)] for i in range(n)]


assert Solution().exclusiveTime(1, ["0:start:0", "0:start:1", "0:start:2", "0:end:3", "0:end:4", "0:end:5"]) == [6]
assert Solution().exclusiveTime(1, ["0:start:0", "0:start:1", "0:start:2", "0:start:3", "0:end:4", "0:end:5", "0:end:6", "0:end:7"]) == [8]
assert Solution().exclusiveTime(2, ["0:start:0", "0:start:2", "0:end:5", "1:start:7", "1:end:7", "0:end:8"]) == [8, 1]
assert Solution().exclusiveTime(2, ["0:start:0", "1:start:2", "1:end:5", "0:end:6"]) == [3, 4]
assert Solution().exclusiveTime(1, ["0:start:0", "0:start:2", "0:end:5", "0:start:6", "0:end:6", "0:end:7"]) == [8]
assert Solution().exclusiveTime(2, ["0:start:0", "0:start:2", "0:end:5", "1:start:6", "1:end:6", "0:end:7"]) == [7, 1]

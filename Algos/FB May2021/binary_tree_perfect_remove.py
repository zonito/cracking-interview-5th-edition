
from binarytree import build, Node


def make_perfect_tree(root: Node):
    if not root:
        return None
    if root.left and not root.right:
        root.left = None
    elif root.right and not root.left:
        root.right = None
    else:
        return make_perfect_tree(root.left) or make_perfect_tree(root.right)
    return root


tree = build([18, 15, None, 40, 50, None, None])
print(tree)
make_perfect_tree(tree)
print(tree)

""" https://leetcode.com/problems/buildings-with-an-ocean-view/ """

from typing import List


class Solution:
    def findBuildings(self, heights: List[int]):
        i = len(heights) - 2
        result = [i + 1]
        while i >= 0:
            if heights[i] > heights[result[-1]]:
                result.append(i)
            i -= 1
        result.reverse()
        return result


Solution().findBuildings([4, 2, 3, 1])

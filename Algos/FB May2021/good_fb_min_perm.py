"""Sort Array with minimum flips. - PanCake Flips"""

from typing import List


def min_operations(arr: List[int]):
    operations = 0
    i = 0
    n = len(arr)
    while i < n - 1:
        # Find decreasing nums
        j = i
        while j + 1 < n and arr[j] > arr[j + 1]:
            j += 1
        if j != i:
            arr = arr[:i] + arr[i:j + 1][::-1] + arr[j + 1:]
            operations += 1
        i += 1
    return operations


assert min_operations([3, 1, 2]) == 2
assert min_operations([1, 2, 5, 4, 3]) == 1

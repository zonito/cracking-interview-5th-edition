''' https://leetcode.com/problems/minimum-remove-to-make-valid-parentheses/ '''


class Solution:
    def minRemoveToMakeValid(self, s: str):
        stack = []
        n = len(s)
        i = 0
        while i < n:
            if s[i] == '(' or (not stack and s[i] == ')'):
                stack.append((s[i], i))
            elif s[i] == ')':
                last = stack.pop()
                if last[0] != '(':
                    stack.append(last)
                    stack.append((s[i], i))
            i += 1
        output = []
        stack = [obj[1] for obj in stack]
        i = 0
        while i < n:
            if i not in stack:
                output.append(s[i])
            i += 1
        return ''.join(output)


assert Solution().minRemoveToMakeValid('lee(t(c)o)de)') == 'lee(t(c)o)de'
assert Solution().minRemoveToMakeValid('a)b(c)d') == 'ab(c)d'
assert Solution().minRemoveToMakeValid('))((') == ''
assert Solution().minRemoveToMakeValid('(a(b(c)d)') == 'a(b(c)d)'

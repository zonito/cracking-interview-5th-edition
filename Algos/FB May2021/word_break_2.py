import collections
from typing import List


class Solution:
    def wordBreak(self, s: str, wordDict: List[str]):
        word_set = set(wordDict)
        q = collections.deque()
        visited = set()

        q.append(0)
        while q:
            start = q.popleft()
            if start in visited:
                continue
            for end in range(start + 1, len(s) + 1):
                if s[start:end] in word_set:
                    q.append(end)
                    if end == len(s):
                        return True
                visited.add(start)
        return False


assert Solution().wordBreak('leetcode', ['leet', 'code'])
assert Solution().wordBreak('applepenapple', ['apple', 'pen'])
assert not Solution().wordBreak('catsandog', ['cats', 'sand', 'dog', 'and', 'cat'])

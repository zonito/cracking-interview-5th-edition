# Kadane's Algorithm

def find_max_sum_sublist(arr):
    if not arr:
        return 0
    if len(arr) == 1:
        return arr[0]
    con_total = arr[0]
    total = arr[0]
    for num in arr:
        con_total = max(con_total + num, num)
        total = max(total, con_total)
    return total


assert find_max_sum_sublist([-4, 2, -5, 1, 2, 3, 6, -5, 1]) == 12
assert find_max_sum_sublist([-4, 2, -5, 1, 2, 3, 6, -5, 12, -10]) == 19
assert find_max_sum_sublist([-4]) == -4
assert find_max_sum_sublist([-4, 2]) == 2
assert find_max_sum_sublist([-4, -1]) == -1
assert find_max_sum_sublist([]) == 0

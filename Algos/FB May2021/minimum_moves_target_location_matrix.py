from typing import List


class Solution:
    def minPushBox(self, grid: List[List[str]]):
        num_row, num_col = len(grid), len(grid[0])
        target = box = player = (0, 0)
        for row in range(num_row):
            for col in range(num_col):
                if grid[row][col] == 'S':
                    player = (row, col)
                elif grid[row][col] == 'T':
                    target = (row, col)
                elif grid[row][col] == 'B':
                    box = (row, col)

        directions = ((0, -1), (-1, 0), (0, 1), (1, 0))

        def inside_grid(pos):
            row, col = pos
            return row >= 0 and row < num_row and col >= 0 and col < num_col

        def is_empty(pos):
            row, col = pos
            return grid[row][col] != '#' and grid[row][col] != 'B'

        def has_path(src, dst):
            # this function is used to find out a path for player to reach DST from SRC
            # player can't jump through the Box
            # src : (row1, col1) , dst : (row2, col2)
            # explicit BFS using queue
            queue = [src]
            visited = set()
            while len(queue) > 0:
                cur = queue.pop(0)
                if cur == dst:
                    return True
                visited.add(cur)
                for direction in directions:
                    next_row, next_col = cur[0] + direction[0], cur[1] + direction[1]
                    next_pos = (next_row, next_col)
                    if not inside_grid(next_pos) or not is_empty(next_pos) or next_pos in visited:
                        continue
                    queue.append(next_pos)
                    visited.add(next_pos)
            return False

        queue = [(box, player, 0)]
        del box, player
        visited = set()  # keeps record of (next_box, cur_box)

        while len(queue) > 0:
            cur_box, cur_player, steps = queue.pop(0)
            if cur_box == target:
                return steps
            grid[cur_box[0]][cur_box[1]] = 'B'  # this is where the box located

            # if the box wants to be moved towards left, i.e, from (i, j) to (i, j-1), the player must be able to reach (i,j+1) to move it
            # same thing from all the other 3 directions
            for direction in directions:
                next_box_row, next_box_col = cur_box[0] + direction[0], cur_box[1] + direction[1]
                next_player_row, next_player_col = cur_box[0] - direction[0], cur_box[1] - direction[1]
                next_box, next_player = (next_box_row, next_box_col), (next_player_row, next_player_col)
                # 1. (next_box_row, next_box_col) and (next_player_row, next_player_col) are inside grid and are empty
                # 2. there exsits path from cur_player to next_player
                # 3. (next_box, cur_box) are unseen before
                if not inside_grid(next_box) or not inside_grid(next_player) or not is_empty(next_box) or not is_empty(next_player) or not has_path(
                        cur_player, next_player) or (next_box, cur_box) in visited:
                    continue

                # for player, if it starts at (i, j+1) and moves a box towards left from (i, j) to (i, j-1), it stops at (i, j)
                queue.append((next_box, cur_box, steps+1))
                visited.add((next_box, cur_box))

            grid[cur_box[0]][cur_box[1]] = '.'  # The box is going to be moved to another place, if not, jump out of this loop

        return -1

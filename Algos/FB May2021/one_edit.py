''' https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/3015/ '''


class Solution:

    def isOneEditDistance(self, s: str, t: str):
        s_len = len(s)
        t_len = len(t)
        diff = abs(s_len - t_len)
        if diff > 1:
            return False
        if s_len > t_len:
            return self.isOneEditDistance(t, s)

        sindex = 0
        tindex = 0
        edits = 0
        while sindex < s_len and tindex < t_len:
            if s[sindex] != t[tindex]:
                edits += 1
                if diff == 1:
                    sindex -= 1
            tindex += 1
            sindex += 1
        return (t_len - tindex) + (s_len - sindex) + edits == 1


assert Solution().isOneEditDistance('axb', 'acb')
assert Solution().isOneEditDistance('ac', 'acb')
assert Solution().isOneEditDistance('acba', 'acb')
assert not Solution().isOneEditDistance('ax', 'acb')
assert Solution().isOneEditDistance('ba', 'a')

import collections


def min_length_substring(s, t):
    need, missing = collections.Counter(t), len(t)
    i = I = J = 0
    for j, c in enumerate(s, 1):
        missing -= int(need[c] > 0)
        need[c] -= 1
        if not missing:
            while i < j and need[s[i]] < 0:
                need[s[i]] += 1
                i += 1
            if not J or j - i <= J - I:
                I, J = i, j
    return len(s[I:J]) if s[I:J] else -1


assert min_length_substring('ADOBECODEBANC', 'ABC') == 4
# [('c', 0), ('a', 1), ('e', 4), ('e', 7), ('c', 9), ('a', 11), ('e', 12), ('c', 15)]
assert min_length_substring('cabwefgewcwaefgcf', 'cae') == 4
assert min_length_substring('dcbefebce', 'fd') == 5
assert min_length_substring('dcbdefadfebce', 'fd') == 2
assert min_length_substring('bfbeadbcbcbfeaaeefcddcccbbbfaaafdbebedddf', 'cbccfafebccdccebdd') == -1

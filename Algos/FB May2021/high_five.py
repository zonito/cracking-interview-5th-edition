''' https://leetcode.com/problems/high-five '''

from typing import List
from collections import defaultdict


class Solution:
    def __init__(self):
        self.students = defaultdict(list)
        self.top = 5

    def add_mark(self, mark: List[int]):
        marks = self.students[mark[0]]
        i = len(marks)
        while i >= 1:
            if mark[1] > marks[i - 1]:
                marks.insert(i, mark[1])
                break
            i -= 1
        else:
            marks.insert(0, mark[1])
        n = len(marks)
        if n > self.top:
            marks = marks[1:]
        self.students[mark[0]] = marks

    def highFive(self, items: List[List[int]]):
        i = 0
        n = len(items)
        while i < n:
            self.add_mark(items[i])
            i += 1
        result = sorted([[id, sum(marks) // 5] for id, marks in self.students.items()], key=lambda obj: obj[0])
        return result


assert Solution().highFive(
    [
        [1, 91],
        [1, 92],
        [2, 93],
        [2, 97],
        [1, 60],
        [2, 77],
        [1, 65],
        [1, 87],
        [1, 100],
        [2, 100],
        [2, 76]
    ]) == [[1, 87], [2, 88]]
assert Solution().highFive(
    [
        [1, 100],
        [7, 100],
        [1, 100],
        [7, 100],
        [1, 100],
        [7, 100],
        [1, 100],
        [7, 100],
        [1, 100],
        [7, 100]
    ]) == [[1, 100], [7, 100]]

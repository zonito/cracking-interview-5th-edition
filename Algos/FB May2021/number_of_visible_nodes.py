''' https://www.facebookrecruiting.com/portal/coding_practice_question/?problem_id=495004218121393 '''
from binarytree import build, Node as TreeNode


def visible_nodes(root: TreeNode):
    if not root:
        return 0
    left_level = visible_nodes(root.left)
    right_level = visible_nodes(root.right)
    return max(left_level, right_level) + 1


assert visible_nodes(build([8, 3, 10, 1, 6, None, 14, None, None, 4, 7, None, None, 13, None])) == 4

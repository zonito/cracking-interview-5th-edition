""" https://leetcode.com/problems/k-closest-points-to-origin/ """

import math
import heapq
from typing import List


class Solution:
    def kClosest(self, points: List[List[int]], k: int):
        distances = []
        counter = 0
        for p in points:
            distance = (p[0]**2 + p[1]**2)
            if counter == k:
                heapq.heappushpop(distances, (-distance, p))
                continue
            heapq.heappush(distances, (-distance, p))
            counter += 1
        return [dis[1] for dis in distances]


assert Solution().kClosest([[1, 3], [-2, 2]], 1) == [[-2, 2]]
assert Solution().kClosest([[3, 3], [5, -1], [-2, 4]], 2)

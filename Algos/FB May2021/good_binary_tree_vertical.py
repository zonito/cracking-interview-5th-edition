''' https://leetcode.com/explore/interview/card/facebook/52/trees-and-graphs/298/ '''


import collections
from typing import List
from binarytree import build, Node as TreeNode


class Solution:

    def verticalOrder(self, root: TreeNode):
        columns = collections.defaultdict(list)
        queue = collections.deque([(root, 0)])
        while queue:
            node, index = queue.popleft()
            if node is not None:
                columns[index].append(node.val)
                queue.append((node.left, index - 1))
                queue.append((node.right, index + 1))
        return [columns[x] for x in sorted(columns.keys())]


assert Solution().verticalOrder(build([3, 9, 20, None, None, 15, 7])) == [[9], [3, 15], [20], [7]]
assert Solution().verticalOrder(build([3, 9, 8, 4, 0, 1, 7])) == [[4], [9], [3, 0, 1], [8], [7]]
assert Solution().verticalOrder(build([3, 9, 8, 4, 0, 1, 7, None, None, None, 2, 5])) == [[4], [9, 5], [3, 0, 1], [8, 2], [7]]
assert Solution().verticalOrder(
    build([
        6, 1, None, None, 3, None, None, None, None, 2, 5, None, None,
        None, None, None, None, None, None, None, None, 4
    ])) == [[1, 2], [6, 3, 4], [5]]

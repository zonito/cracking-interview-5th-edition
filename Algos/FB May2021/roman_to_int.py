"""
Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.

Symbol       Value
I             1
V             5
X             10
L             50
C             100
D             500
M             1000
"""


class Solution(object):
    def get_roman_to_int(self, roman):
        num_map = dict(I=1, V=5, X=10, L=50, C=100, D=500, M=1000)
        ans = 0
        index = 0
        len_r = len(roman)
        while index < len_r:
            if index + 1 < len_r and num_map[roman[index]] < num_map[roman[index + 1]]:
                ans += num_map[roman[index + 1]] - num_map[roman[index]]
                index += 2
            else:
                ans += num_map[roman[index]]
                index += 1
        # print(roman, ans)
        return ans


assert Solution().get_roman_to_int('III') == 3
assert Solution().get_roman_to_int('IV') == 4
assert Solution().get_roman_to_int('IX') == 9
assert Solution().get_roman_to_int('LVIII') == 58
assert Solution().get_roman_to_int('MCMXCIV') == 1994

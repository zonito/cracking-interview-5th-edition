""" https://leetcode.com/problems/maximum-points-you-can-obtain-from-cards/"""

import sys
from typing import List


class Solution:
    def maxScore(self, cardPoints: List[int], k: int):
        max_score = running_sum = sum(cardPoints[:k])
        for index in range(1, k + 1):
            running_sum += cardPoints[-index] - cardPoints[k - index]
            max_score = max(max_score, running_sum)
        return max_score


assert Solution().maxScore([9, 7, 7, 9, 7, 7, 9], 7) == 55
assert Solution().maxScore([1, 2, 3, 4, 5, 6, 1], 3) == 12
assert Solution().maxScore([3, 2, 1], 1) == 3
assert Solution().maxScore([1, 1, 1, 1, 1], 1) == 1
assert Solution().maxScore([2, 2, 2], 2) == 4
assert Solution().maxScore([1, 1000, 1], 1) == 1
assert Solution().maxScore([1, 79, 80, 1, 1, 1, 200, 1], 3) == 202

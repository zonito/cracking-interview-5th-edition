''' https://leetcode.com/explore/interview/card/facebook/55/dynamic-programming-3/3034/ '''


class Solution:
    def longestPalindrome(self, s: str):
        n = len(s)
        i = 0
        max_len = 1
        start_index = 0
        while i < n:
            even_cnt = odd_cnt = 1
            even_idx = odd_idx = i
            counter = 1
            if i + 1 < n and s[i] == s[i + 1]:
                even_cnt += 1
                while i - counter >= 0 and i + counter + 1 < n and s[i - counter] == s[i + counter + 1]:
                    even_cnt += 2
                    even_idx = i - counter
                    counter += 1
            counter = 1
            while i - counter >= 0 and counter + i < n and s[i - counter] == s[counter + i]:
                odd_cnt += 2
                odd_idx = i - counter
                counter += 1
            if odd_cnt > max_len:
                start_index = odd_idx
                max_len = odd_cnt
            if even_cnt > max_len:
                start_index = even_idx
                max_len = even_cnt
            i += 1
        return s[start_index:start_index + max_len]


assert Solution().longestPalindrome('aaaa') == 'aaaa'
assert Solution().longestPalindrome('ccc') == 'ccc'
assert Solution().longestPalindrome('abbx') == 'bb'
assert Solution().longestPalindrome('abba') == 'abba'
assert Solution().longestPalindrome('babad') == 'bab'
assert Solution().longestPalindrome('babab') == 'babab'

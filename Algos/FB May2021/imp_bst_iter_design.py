''' https://leetcode.com/problems/binary-search-tree-iterator/ - In-Order Traversal'''

from binarytree import build, Node as TreeNode


class BSTIterator:

    def __init__(self, node: TreeNode):
        self.stack = [node]
        self._collect_lefts(node)

    def _collect_lefts(self, node: TreeNode):
        while node.left:
            node = node.left
            self.stack.append(node)

    def next(self):
        node = self.stack.pop()
        if node.right:
            self.stack.append(node.right)
            self._collect_lefts(node.right)
        return node.val

    def hasNext(self):
        return bool(self.stack)


# Your BSTIterator object will be instantiated and called as such:
obj = BSTIterator(build([7, 3, 15, None, None, 9, 20]))
assert obj.next() == 3
assert obj.next() == 7
assert obj.hasNext()
assert obj.next() == 9
assert obj.hasNext()
assert obj.next() == 15
assert obj.hasNext()
assert obj.next() == 20
assert not obj.hasNext()

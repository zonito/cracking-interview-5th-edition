from typing import List


class Solution:
    def maxAreaOfIsland(self, grid: List[List[int]]):
        max_area = 0
        directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]
        rows = len(grid)
        cols = len(grid[0])

        def dfs(grid, i, j, area):
            grid[i][j] = 0
            total = area
            for d in directions:
                if i - d[0] >= 0 and j - d[1] >= 0 and i - d[0] < rows and j - d[1] < cols and grid[i - d[0]][j - d[1]] == 1:
                    total += dfs(grid, i - d[0], j - d[1], area)
            return total

        for ri in range(rows):
            for ci in range(cols):
                if grid[ri][ci] == 1:
                    max_area = max(dfs(grid, ri, ci, 1), max_area)
        print('Max Area: ', max_area)
        return max_area


assert Solution().maxAreaOfIsland([
    [0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
    [0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0],
    [0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0]
]) == 6

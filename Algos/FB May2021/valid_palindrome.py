''' https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/288/ '''


class Solution:
    def isPalindrome(self, s: str):
        lower_chars = list(
            map(lambda ch: ch.lower(), filter(lambda ch: ch.isalnum(), s)))
        return lower_chars == lower_chars[::-1]


assert Solution().isPalindrome('A man, a plan, a canal: Panama')

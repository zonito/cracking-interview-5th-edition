''' https://www.facebookrecruiting.com/portal/coding_practice_question/?problem_id=146466059993201 '''

import collections
from typing import List


def findSignatureCounts(arr: List[int]):
    n = len(arr)
    students = collections.defaultdict(int)
    while True:
        temp = arr[::]
        is_change = False
        i = 0
        while i < n:
            if arr[i] - 1 != i:
                temp[arr[i] - 1] = arr[i]
                is_change = True
            students[arr[i] - 1] += 1
            i += 1
        arr = temp
        if not is_change:
            break
    return [students[i] for i in range(n)]


assert findSignatureCounts([2, 1]) == [2, 2]
assert findSignatureCounts([1, 2]) == [1, 1]

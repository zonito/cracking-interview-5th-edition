''' https://leetcode.com/explore/interview/card/facebook/53/recursion-3/278/ '''


from typing import List


class Solution:
    def getSubsets(self, nums: List[int]):
        n = len(nums)
        output = []
        for index in range(n):
            output.append([nums[index]])
            for sub in self.getSubsets(nums[index + 1:]):
                output.append(sub + [nums[index]])
        return output

    def subsets(self, nums: List[int]):
        result = [[]] + self.getSubsets(nums)
        print(result)
        return result


# Solution().subsets([1, 2, 3])
Solution().subsets([1, 2, 3])
Solution().subsets([3, 2, 4, 1])
"""
[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
[[],[3],[2],[2,3],[4],[3,4],[2,4],[2,3,4],[1],[1,3],[1,2],[1,2,3],[1,4],[1,3,4],[1,2,4],[1,2,3,4]]
"""

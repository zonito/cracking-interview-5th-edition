from typing import List


class Solution:
    def is_exists(self, arr: List[int], val: int):
        if not arr:
            return False
        if len(arr) == 1:
            return arr[0] == val
        mid = (len(arr) // 2) - 1
        if arr[mid] > val:
            return self.is_exists(arr[:mid], val)
        elif arr[mid] < val:
            return self.is_exists(arr[mid + 1:], val)
        return True

    def arraysIntersection(self, arr1: List[int], arr2: List[int], arr3: List[int]):
        inte_arr = []
        n = len(arr1)
        i = 0
        while i < n:
            if self.is_exists(arr2, arr1[i]) and self.is_exists(arr3, arr1[i]):
                inte_arr.append(arr1[i])
            i += 1
        return inte_arr


assert Solution().arraysIntersection(
    arr1=[1, 2, 3, 4, 5], arr2=[1, 2, 5, 7, 9], arr3=[1, 2, 3, 4, 5, 8]) == [1, 2, 5]
assert Solution().arraysIntersection(
    arr1=[197, 418, 523, 876, 1356], arr2=[501, 880, 1593, 1710, 1870], arr3=[521, 682, 1337, 1395, 1764]) == []

class Solution:
    def maxDepth(self, s: str):
        n = len(s)
        i = 0
        depth = 0
        open_params = 0
        while i < n:
            if s[i] == '(':
                open_params += 1
            elif s[i] == ')':
                open_params -= 1
            depth = max(depth, open_params)
            i += 1
        return depth


assert Solution().maxDepth('(1+(2*3)+((8)/4))+1') == 3
assert Solution().maxDepth('(1)+((2))+(((3)))') == 3
assert Solution().maxDepth('1+(2*3)/(2-1)') == 1
assert Solution().maxDepth('1') == 0

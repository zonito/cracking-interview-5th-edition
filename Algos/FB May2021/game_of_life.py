from typing import List


class Solution:
    def count_live_neighbors(self, board: List[List[int]], i: int, j: int):
        count = 0
        rows = len(board)
        cols = len(board[0])
        for row_index in range(i - 1, i + 2):
            for col_index in range(j - 1, j + 2):
                if row_index in range(rows) and col_index in range(cols) and (row_index != i or col_index != j):
                    count += board[row_index][col_index]
        return count

    def gameOfLife(self, board: List[List[int]]):
        result = [row[:] for row in board]
        rows = len(board)
        cols = len(board[0])
        for row_index in range(rows):
            for col_index in range(cols):
                count = self.count_live_neighbors(result, row_index, col_index)
                if result[row_index][col_index] == 1:
                    if count < 2 or count > 3:
                        board[row_index][col_index] = 0
                elif count == 3:
                    board[row_index][col_index] = 1


board = [[0, 1, 0], [0, 0, 1], [1, 1, 1], [0, 0, 0]]
Solution().gameOfLife(board)
assert board == [[0, 0, 0], [1, 0, 1], [0, 1, 1], [0, 1, 0]]

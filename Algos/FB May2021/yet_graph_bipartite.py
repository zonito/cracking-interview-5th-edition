''' https://leetcode.com/problems/is-graph-bipartite/solution/ '''

from typing import List


class Solution:
    def isBipartite(self, graph: List[List[int]]):
        colors = [None] * len(graph)
        for index in range(len(graph)):
            if colors[index] is not None:
                continue
            stack = [index]
            colors[index] = 0
            while stack:
                node_index = stack.pop()
                for nindex in graph[node_index]:
                    if colors[node_index] == colors[nindex]:
                        return False
                    if colors[nindex] is None:
                        colors[nindex] = colors[node_index] ^ 1
                        stack.append(nindex)
        return True


assert not Solution().isBipartite([[1, 2, 3], [0, 2], [0, 1, 3], [0, 2]])
assert Solution().isBipartite([[1, 3], [0, 2], [1, 3], [0, 2]])

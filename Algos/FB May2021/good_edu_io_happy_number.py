def is_happy_number(num):
    outputs = {num}
    while num != 1:
        n = num
        total = 0
        while n != 0:
            total += (n % 10) ** 2
            n = n // 10
        # To Avoid going in a loop
        if total in outputs:
            return total == 1
        num = total
        outputs.add(total)
    return num == 1


# 23
# 3**2 + 2**2 = 9 + 4 = 13
# 1**2 + 3**2 = 1 + 9 = 10
# 1**2 + 0**2 = 1 + 0 = 1 <-- Happy Number
assert is_happy_number(23)

# 12
# 1 + 4 = 5
# 25
# 4 + 24 = 29
# 4 + 81 = 85
# 64 + 25 = 89  <-- Watch this number
# 94 + 81 = 175
# 1 + 49 + 25 = 75
# 49 + 25 = 74
# 49 + 16 = 65
# 36 + 25 = 61
# 36 + 1 = 37
# 9 + 49 = 58
# 25 + 64 = 89  <--- Starting another loop
assert not is_happy_number(12)

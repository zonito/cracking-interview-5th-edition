
import collections


def countDistinctTriangles(arr):
    storage = collections.defaultdict(int)
    for tri in arr:
        storage[tuple(sorted(tri))] += 1
    return len(storage)


assert countDistinctTriangles([[2, 2, 3], [3, 2, 2], [2, 5, 6]]) == 2
assert countDistinctTriangles([[8, 4, 6], [100, 101, 102], [84, 93, 173]]) == 3
assert countDistinctTriangles([[5, 8, 9], [5, 9, 8], [9, 5, 8], [9, 8, 5], [8, 9, 5], [8, 5, 9]]) == 1

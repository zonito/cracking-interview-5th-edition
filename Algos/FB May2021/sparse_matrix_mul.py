""" https://leetcode.com/problems/sparse-matrix-multiplication/ """

from typing import List


class Solution:
    def multiply(self, mat1: List[List[int]], mat2: List[List[int]]):
        n1, n2_c, n1_c = len(mat1), len(mat2[0]), len(mat1[0])
        result = [[0] * n2_c for _ in range(n1)]
        for i in range(n1):
            for k in range(n1_c):
                if not mat1[i][k]:
                    continue
                for j in range(n2_c):
                    result[i][j] += mat1[i][k] * mat2[k][j]
        return result

# Good Approaches:
# https://leetcode.com/problems/sparse-matrix-multiplication/discuss/577491/4-python-approaches-with-time-and-space-analysis


assert Solution().multiply(
    [
        [1, 0, 0],
        [-1, 0, 3]
    ],
    [
        [7, 0, 0],
        [0, 0, 0],
        [0, 0, 1]
    ]) == [[7, 0, 0], [-7, 0, 3]]

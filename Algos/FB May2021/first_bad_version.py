''' https://leetcode.com/explore/interview/card/facebook/54/sorting-and-searching-3/272/ '''

# The isBadVersion API is already defined for you.
# @param version, an integer
# @return an integer
def isBadVersion(version):
    return version == 4


class Solution:
    def firstBadVersion(self, n):
        start = 1
        while start < n:
            mid = (start + n) // 2
            if isBadVersion(mid):
                n = mid
            else:
                start = mid + 1
        return n


# Solution().firstBadVersion(2126753390)
print(Solution().firstBadVersion(5))

from typing import List


class Solution:
    def findLengthOfLCIS(self, nums: List[int]):
        output = 1
        counter = 1
        for i in range(1, len(nums)):
            if nums[i-1] < nums[i]:
                counter += 1
            else:
                output = max(counter, output)
                counter = 1
        return max(output, counter)


assert Solution().findLengthOfLCIS([1, 3, 5, 4, 7]) == 3
assert Solution().findLengthOfLCIS([1, 3, 4, 7]) == 4
assert Solution().findLengthOfLCIS([2, 2, 2]) == 1

class Solution:
    def getSmallestString(self, n: int, k: int):
        result = []
        while n != k:
            i = 25
            while k - i < n:
                i -= 1
            k -= i
            result.append(chr(ord('a') + i))
        while len(result) < n:
            result.append('a')
        return ''.join(result[::-1])


assert Solution().getSmallestString(5, 73) == 'aaszz'
assert Solution().getSmallestString(3, 27) == 'aay'

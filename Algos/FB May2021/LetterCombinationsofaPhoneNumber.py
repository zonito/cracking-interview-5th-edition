''' https://leetcode.com/explore/interview/card/facebook/53/recursion-3/267/ '''


class Solution:
    digit_char = {
        '2': 'abc',
        '3': 'def',
        '4': 'ghi',
        '5': 'jkl',
        '6': 'mno',
        '7': 'pqrs',
        '8': 'tuv',
        '9': 'wxyz',
        '0': '',
    }
    storage = {}

    def letterCombinations(self, digits: str):
        if not digits:
            return []
        comb = []
        for char in self.digit_char[digits[0]]:
            rest = digits[1:]
            if not rest:
                comb.append(char)
                continue
            if not self.storage.get(rest):
                self.storage[rest] = self.letterCombinations(digits[1:])
            for lc in self.storage[rest]:
                comb.append(char + lc)
        return comb


# assert sorted(Solution().letterCombinations('23')) == sorted(
#     ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"])
print(Solution().letterCombinations('234'))

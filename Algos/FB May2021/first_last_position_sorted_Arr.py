''' https://leetcode.com/explore/interview/card/facebook/54/sorting-and-searching-3/3030/ '''

import sys
from typing import List


class Solution:
    def searchRange(self, nums: List[int], target: int):
        if not nums:
            return [-1, -1]
        n = len(nums)
        start = 0
        end = n
        index = None
        while start <= end:
            mid = start + (end - start) // 2
            if mid >= n:
                break
            if nums[mid] == target:
                index = [mid, mid]
                for i in range(mid - 1, start - 1, -1):
                    if i >= 0 and nums[i] == target:
                        index[0] -= 1
                        continue
                    break
                for i in range(mid + 1, end + 1):
                    if i < n and nums[i] == target:
                        index[1] += 1
                        continue
                    break
                break
            elif nums[mid] > target:
                end = mid - 1
            else:
                start = mid + 1
        return index if index else [-1, -1]


assert Solution().searchRange([2, 2], 2) == [0, 1]
assert Solution().searchRange([2, 2], 3) == [-1, -1]
assert Solution().searchRange([5, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 10], 8) == [3, 12]
assert Solution().searchRange([5, 7, 7, 8, 8, 10], 8) == [3, 4]
assert Solution().searchRange([5, 7, 7, 8, 8, 10], 6) == [-1, -1]
assert Solution().searchRange([], 0) == [-1, -1]

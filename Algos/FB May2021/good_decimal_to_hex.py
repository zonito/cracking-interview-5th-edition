""" https://leetcode.com/problems/convert-a-number-to-hexadecimal/submissions/"""


class Solution:
    def toHex(self, num: int):
        if num == 0:
            return '0'
        result = []
        hex_nums = '0123456789abcdef'
        if num < 0:
            num += 2 ** 32
        while num > 0:
            reminder = num % 16
            num //= 16
            result.append(hex_nums[reminder])
        return ''.join(result[::-1])

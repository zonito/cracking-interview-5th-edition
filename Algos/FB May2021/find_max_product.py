""" https://www.facebookrecruiting.com/portal/coding_practice_question/?problem_id=510655302929581 """


from functools import reduce


def findMaxProduct(arr):
    max_3 = [1, 1, 1]
    i = 0
    n = len(arr)
    output = []
    while i < n:
        if i < 2:
            output.append(-1)

        # Add to max 3
        j = 2
        while arr[i] < max_3[j] and j > 0:
            j -= 1
        if j:
            max_3.insert(j + 1, arr[i])
            max_3 = max_3[-3:]

        # Product
        if i >= 2:
            output.append(reduce(lambda n, m: n*m, max_3))
        i += 1
    return output


assert findMaxProduct([1, 2, 3, 4, 5]) == [-1, -1, 6, 24, 60]
assert findMaxProduct([2, 1, 2, 1, 2]) == [-1, -1, 4, 4, 8]
assert findMaxProduct([2, 4, 7, 1, 5, 3]) == [-1, -1, 56, 56, 140, 140]

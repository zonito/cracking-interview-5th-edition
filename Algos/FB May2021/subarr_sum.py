''' https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/3019/ '''

import collections
from typing import Collection, List


class Solution:
    def subarraySum(self, nums: List[int], k: int):
        sums = collections.defaultdict(int)
        sums[0] = 1
        result = 0
        con_sum = 0
        for i in range(len(nums)):
            con_sum += nums[i]
            result += sums[con_sum - k]
            sums[con_sum] += 1
        return result


assert Solution().subarraySum([1, 1, 1], 2) == 2
assert Solution().subarraySum([1, 2, 3], 3) == 2
assert Solution().subarraySum([1], 0) == 0
assert Solution().subarraySum([-1, -1, 1], 0) == 1
assert Solution().subarraySum([1, -1, 0], 0) == 3

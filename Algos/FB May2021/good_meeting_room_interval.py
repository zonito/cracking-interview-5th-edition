""" https://leetcode.com/problems/meeting-rooms-ii/ """

from typing import List


class Solution:
    def minMeetingRooms(self, intervals: List[List[int]]):
        rooms = []
        intervals.sort(key=lambda x: x[0])
        for interval in intervals:
            start, end = interval
            for room in rooms:
                if start >= room[1]:
                    room[1] = end
                    break
            else:
                rooms.append([start, end])
        return len(rooms)


assert Solution().minMeetingRooms([[0, 30], [5, 10], [15, 20]]) == 2
assert Solution().minMeetingRooms([[7, 10], [2, 4]]) == 1
assert Solution().minMeetingRooms([[0, 1], [1, 2], [0, 1], [0, 1]]) == 3
assert Solution().minMeetingRooms([[2, 15], [36, 45], [9, 29], [16, 23], [4, 9]]) == 2

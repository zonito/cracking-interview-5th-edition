''' https://leetcode.com/problems/integer-to-english-words '''


class Solution:
    words = [
        'Thousand', 'Million', 'Billion', 'Trillion', 'Quadrillion',
        'Quintillion', 'Sextillion', 'Septillion', 'Octillion', 'Nonillion', 'Decillion'
    ]
    elevens = [
        'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen',
        'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'
    ]
    tens = [
        'Twenty', 'Thirty', 'Forty', 'Fifty',
        'Sixty', 'Seventy', 'Eighty', 'Ninety'
    ]
    numbers = [
        'One', 'Two', 'Three', 'Four',
        'Five', 'Six', 'Seven', 'Eight', 'Nine'
    ]

    def getInWord(self, num: int):
        ''' Given num is group of 3 numbers max. '''
        words = []
        counter = 0
        prev_last_digit = -1
        while num:
            last_digit = num % 10
            num //= 10
            if last_digit:
                if counter == 0:
                    words.append(self.numbers[last_digit - 1])
                elif counter == 1:
                    if last_digit == 1 and prev_last_digit != -1:
                        if words:
                            words.pop()
                        words.append(self.elevens[prev_last_digit])
                    else:
                        words.append(self.tens[last_digit - 2])
                else:
                    words.append('Hundred')
                    words.append(self.numbers[last_digit - 1])
            counter += 1
            prev_last_digit = last_digit
        return ' '.join(reversed(words))

    def numberToWords(self, num: int):
        if num == 0:
            # print('Zero')
            return 'Zero'
        words = ['']
        counter = 0
        while num:
            last_3 = num % 1000
            num //= 1000
            if not num:
                if last_3:
                    words.append(self.getInWord(last_3))
                break
            if last_3:
                words.append(self.getInWord(last_3))
            else:
                words.pop()
            words.append(self.words[counter])
            counter += 1
        # print(' '.join(reversed(words)))
        return ' '.join(reversed(words)).strip()


# Solution().numberToWords(123)
# Solution().numberToWords(12345)
# Solution().numberToWords(1234567)
# Solution().numberToWords(1234567891)
# Solution().numberToWords(1000010)
Solution().numberToWords(10000000000000100000000)
# Solution().numberToWords(0)

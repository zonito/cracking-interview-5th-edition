""" https://leetcode.com/problems/maximum-distance-in-arrays/submissions/ """

from typing import List


class Solution:
    def maxDistance(self, arrays: List[List[int]]):
        f_arr = sorted(arrays, key=lambda obj: obj[0])
        l_arr = sorted(arrays, key=lambda obj: obj[-1])
        if f_arr[0] != l_arr[-1]:
            return l_arr[-1][-1] - f_arr[0][0]
        return max(l_arr[-1][-1] - f_arr[1][0], l_arr[-2][-1] - f_arr[0][0])

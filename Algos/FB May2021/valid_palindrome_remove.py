''' https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/289/ '''


class Solution:
    def validPalindrome(self, s: str):
        index = 0
        last_index = len(s) - 1
        while index <= last_index:
            if s[index] != s[last_index]:
                first, second = s[index: last_index], s[index+1:last_index+1]
                return first == first[::-1] or second == second[::-1]
            index += 1
            last_index -= 1
        return True


assert Solution().validPalindrome('aba')
assert not Solution().validPalindrome('abc')
assert Solution().validPalindrome('abca')
assert not Solution().validPalindrome('aabca')
assert Solution().validPalindrome('axbababa')
assert Solution().validPalindrome('cbbcc')
assert not Solution().validPalindrome('cbbccc')

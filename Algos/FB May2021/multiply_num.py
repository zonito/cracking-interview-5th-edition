'''
Given two non-negative integers num1 and num2 represented as strings, return the product of num1 and num2, also represented as a string.

Note: You must not use any built-in BigInteger library or convert the inputs to integer directly.
    29
  x 38
  ----
   232
  +870
  ----
  1102
'''


class Solution:
    def multiply_num(self, num1: str, num2: str):
        # To push last digit of every multiplications
        result = [0] * (len(num2) + len(num1) + 2)
        i = len(num2) - 1
        while i >= 0:
            j = len(num1) - 1
            carry = 0
            counter = len(result) - (len(num2) - i)
            while j >= 0:
                ans = int(num2[i]) * int(num1[j]) + carry
                result[counter] += ans % 10
                carry = ans // 10
                if result[counter] > 9:
                    carry += result[counter] // 10
                    result[counter] = result[counter] % 10
                counter -= 1
                j -= 1
            result[counter] += carry
            i -= 1

        # Find index
        index = 0
        while index < len(result):
            if result[index]:
                break
            index += 1
        print(''.join(map(str, result[index:])))
        return ''.join(map(str, result[index:])) or '0'


# Complexity:
# Time: O(len(n1) * len(n2))
# Space: O(len(n1) + len(n2))
assert Solution().multiply_num("2", "3") == "6"
assert Solution().multiply_num("0", "0") == "0"
assert Solution().multiply_num("29", "38") == "1102"
assert Solution().multiply_num("123", "456") == "56088"
assert Solution().multiply_num("999", "999") == "998001"

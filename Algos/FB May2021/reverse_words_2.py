""" https://leetcode.com/problems/reverse-words-in-a-string-ii/submissions/
    """

from typing import List


class Solution:
    def reverseWords(self, s: List[str]):
        """
        Do not return anything, modify s in-place instead.
        """
        start = 0

        def rev(s, start, end):
            while start < end:
                s[start], s[end] = s[end], s[start]
                start += 1
                end -= 1
        for index, char in enumerate(s):
            if char == ' ':
                rev(s, start, index - 1)
                start = index + 1
        rev(s, start, len(s) - 1)
        s.reverse()

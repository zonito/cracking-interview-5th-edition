""" https://leetcode.com/explore/interview/card/facebook/52/trees-and-graphs/322/ """


from binarytree import build, Node as TreeNode


class Solution:
    def flatten(self, root: TreeNode):
        """
        Do not return anything, modify root in-place instead.
        """
        if not root:
            return root
        if root.left or root.right:
            root.left, root.right = root.right, root.left
            root.right = self.flatten(root.right)
            root.left = self.flatten(root.left)
            temp = root
            while temp.right:
                temp = temp.right
            temp.right = root.left
            root.left = None
        return root


# values = [1, 2, 10, 3, 4, 11, 12, 5, 6, 7, 8]
values = [1, 2, 5, 3, 4, None, 6]
print(Solution().flatten(build(values)))
print(Solution().flatten(build([])))
print(Solution().flatten(build([0])))

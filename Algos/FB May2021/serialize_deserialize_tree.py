''' https://leetcode.com/problems/serialize-and-deserialize-binary-tree/ '''

from binarytree import build, Node as TreeNode


class Codec:

    def serialize(self, root: TreeNode):
        """Encodes a tree to a single string.

        :type root: TreeNode
        :rtype: str
        """
        if not root:
            return
        return (root.val, self.serialize(root.left), self.serialize(root.right))

    def deserialize(self, data):
        """Decodes your encoded data to tree.

        :type data: str
        :rtype: TreeNode
        """
        print(data)
        if not data:
            return
        return TreeNode(data[0], self.deserialize(data[1]), self.deserialize(data[2]))


print(Codec().deserialize(Codec().serialize(build([1, 2, 3, None, None, 4, 5]))))

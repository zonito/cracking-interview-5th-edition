""" There're 3 kinds of bus ticket.
1: ticket 1 cost 2 and can be used for a day.
2: ticket 2 cost 7 and can be used for a consecutive 7 days.
3: ticket 3 cost 25 can be used for a month. Assume month here means 30 consecutive days.

Now there's a array filled with elements. Each element value is a date for a person to travel. This array has already been sorted in ascending order, like {1,3,4,5,11,12,23,24,30}.
Obviously the final day is 30th and first day is 1st.

So for any given array from a person to travel, how can this person cost least ?

Example:
given array like
int [] arr={1,3,4,5,6,7,12,13,14,15,16,17,18,19,20,28,30};
the least cost is 22, for 1~7 days cost 7, 12~18 days cost 7, and 19,20,28,30 each costs 2 so it sums up to be 22.
"""

from typing import List


class Solution:
    def buy_tickets(self, days: List[int]):
        cost = min(len(days) * 2, 25)
        week_bucket = []
        mod = days[0] + 7
        temp_cost = 0
        for day in days:
            if day < mod:
                week_bucket.append(day)
                continue
            temp_cost += min(len(week_bucket) * 2, 7)
            week_bucket = [day]
            mod = day + 7
        return min(cost, temp_cost + min(len(week_bucket) * 2, 7))


assert Solution().buy_tickets([1, 3, 4, 5, 6, 7, 12, 13, 14, 15, 16, 17, 18, 19, 20, 28, 30]) == 22

''' https://leetcode.com/explore/interview/card/facebook/52/trees-and-graphs/291/ '''

from binarytree import build, Node as TreeNode


class Solution:
    total = 0

    def dfs(self, root: TreeNode):
        if not root:
            return 0
        left = self.dfs(root.left)
        right = self.dfs(root.right)
        self.total = max(self.total, left + right + 1)
        return max(left, right) + 1

    def diameterOfBinaryTree(self, root: TreeNode):
        self.total = 1
        self.dfs(root)
        return self.total - 1


# assert Solution().diameterOfBinaryTree(build([1, 2])) == 3
assert Solution().diameterOfBinaryTree(build([1, 2, 3, 4, 5])) == 3

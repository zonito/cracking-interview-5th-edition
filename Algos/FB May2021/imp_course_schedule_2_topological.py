import collections
from typing import List


class Solution:

    WHITE = 1   # Start
    GRAY = 2    # Processing...
    BLACK = 3   # Completed

    def findOrder(self, numCourses: int, prerequisites: List[List[int]]):

        # Create In Degree / In Edges
        in_edges = collections.defaultdict(list)
        for dest, src in prerequisites:
            in_edges[src].append(dest)

        topological_sorted_order = []
        is_possible = True

        # Default: Start all node
        color = {k: Solution.WHITE for k in range(numCourses)}

        def dfs(node: int):
            nonlocal is_possible
            if not is_possible:
                return

            # Processing...
            color[node] = Solution.GRAY
            for neighbor in in_edges.get(node, []):
                if color[neighbor] == Solution.WHITE:
                    dfs(neighbor)
                elif color[neighbor] == Solution.GRAY:  # Found Circular Loop
                    is_possible = False
                    break

            # Processed / Completed
            color[node] = Solution.BLACK
            topological_sorted_order.append(node)

        for vertex in range(numCourses):
            if color[vertex] == Solution.WHITE:
                dfs(vertex)

        return topological_sorted_order[::-1] if is_possible else []


assert Solution().findOrder(3, [[0, 2], [1, 2], [2, 0]]) == []
assert Solution().findOrder(2, [[1, 0]]) == [0, 1]
assert Solution().findOrder(2, [[1, 0], [0, 1]]) == []
assert Solution().findOrder(3, [[1, 0], [2, 1]]) == [0, 1, 2]
assert Solution().findOrder(4, [[1, 0], [2, 0], [3, 2], [3, 1]]) == [0, 2, 1, 3]

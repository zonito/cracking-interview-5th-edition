''' https://www.facebookrecruiting.com/portal/coding_practice_question/?problem_id=226517205173943 '''


def count_subarrays(arr):
    output = []
    n = len(arr)
    for i in range(n):
        j = i
        rj = 1
        total = 1
        # Spread both side from i index
        while True:
            # If next is bigger
            if j + 1 < n and arr[i] > arr[j + 1]:
                total += 1
                j += 1
                continue
            # if previous is bigger
            if i - rj >= 0 and arr[i] > arr[i - rj]:
                total += 1
                rj += 1
                continue
            break
        output.append(total)
    return output


# Larger than surronding including self
assert count_subarrays([3, 4, 1, 6, 2]) == [1, 3, 1, 5, 1]

from typing import List


class Solution:
    def longestIncreasingPath(self, matrix: List[List[int]]):
        if not matrix:
            return 0
        rows = len(matrix)
        cols = len(matrix[0])
        directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]

        def dfs(matrix, ri, ci):
            current = matrix[ri][ci]
            max_total = 1
            for d in directions:
                x = ri - d[0]
                y = ci - d[1]
                total = 1
                if x >= 0 and x < rows and y >= 0 and y < cols and matrix[x][y] > current:
                    total += dfs(matrix, x, y)
                max_total = max(max_total, total)
            return max_total

        max_path = 1
        for ri in range(rows):
            for ci in range(cols):
                max_path = max(max_path, dfs(matrix, ri, ci))

        return max_path


class Solution2:
    def longestIncreasingPath(self, matrix: List[List[int]]):

        m, n = len(matrix), len(matrix[0])
        ans = [[-1] * n for _ in range(m)]

        def dfs(i, j, prev):
            if i < 0 or i >= m or j < 0 or j >= n:
                return 0
            if matrix[i][j] <= prev:
                return 0
            if ans[i][j] != -1:
                return ans[i][j]
            cur = matrix[i][j]
            maxLen = 1 + max(
                dfs(i + 1, j, cur), dfs(i - 1, j, cur),
                dfs(i, j + 1, cur), dfs(i, j - 1, cur)
            )
            ans[i][j] = maxLen
            return maxLen

        return max(dfs(i, j, float('-inf')) for i in range(m) for j in range(n))


assert Solution().longestIncreasingPath([[9, 9, 4], [6, 6, 8], [2, 1, 1]]) == 4

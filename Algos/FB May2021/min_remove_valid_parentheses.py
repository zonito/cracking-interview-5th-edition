class Solution:
    def minRemoveToMakeValid(self, s: str) -> str:
        stack = []
        for index, char in enumerate(s):
            if char == '(':
                stack.append([char, index])
            elif char == ')':
                if stack and stack[-1][0] == '(':
                    stack.pop()
                else:
                    stack.append([char, index])
        for _, index in stack[::-1]:
            s = s[:index] + s[index+1:]
        return s


assert Solution().minRemoveToMakeValid(")()())") == "()()"
assert Solution().minRemoveToMakeValid("lee(t(c)o)de)") == "lee(t(c)o)de"

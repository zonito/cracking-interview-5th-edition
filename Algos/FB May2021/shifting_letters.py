""" https://leetcode.com/problems/shifting-letters/"""

from itertools import accumulate
from typing import List


class Solution:
    def shiftingLetters(self, s: str, shifts: List[int]):
        shifts = list(accumulate(shifts[::-1]))[::-1]   # important - Accumulate and reverse
        return ''.join(
            chr(((ord(c) - ord('a')) + shifts[i]) % 26 + ord('a'))
            for i, c in enumerate(s)
        )


assert Solution().shiftingLetters(s="abc", shifts=[3, 5, 9]) == "rpl"
assert Solution().shiftingLetters(s="ruu", shifts=[26, 9, 17]) == "rul"
assert Solution().shiftingLetters(
    s="abc", shifts=[918626230, 520749458, 524182212]) == "srk"

''' Minimize rounding error '''

import math
from typing import List


class Solution:
    def minimizeError(self, prices: List[str], target: int):
        prices = list(map(float, prices))
        diff = []
        upper_bound = lower_bound = 0
        for price in prices:
            floor = math.floor(price)
            ceil = math.ceil(price)
            upper_bound += ceil
            lower_bound += floor
            if floor != ceil:
                diff.append([ceil - price, floor - price])

        if target < lower_bound or target > upper_bound:
            return '-1'

        diff.sort(key=lambda x: x[0])
        flips = target - int(lower_bound)
        # print(upper_bound, lower_bound, target, flips, prices)
        abs_sum = 0
        abs_sum += sum([cost[0] for cost in diff[:flips]])
        abs_sum += sum([cost[0] for cost in diff[flips:]])
        return '%0.03f' % round(abs_sum, 3)


print(Solution().minimizeError(["0.700", "2.800", "4.900"], 8))
print(Solution().minimizeError(["1.500", "2.500", "3.500"], 10))
print(Solution().minimizeError(["1.500", "2.500", "3.500"], 9))
print(Solution().minimizeError(
    ["2.146", "2.656", "2.908", "2.739", "2.589"], 11))


class Node:
    def __init__(self, val=0, neighbors=None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []


class Solution:
    def cloneGraph(self, node: Node):
        if not node:
            return
        n_node = Node(node.val)
        storage = {node.val: n_node}
        queue = [node]
        visited = set()
        while queue:
            n = queue.pop()
            visited.add(n.val)
            for neig in n.neighbors:
                if storage.get(neig.val) is None:
                    storage[neig.val] = Node(neig.val)
                storage[n.val].neighbors.append(storage[neig.val])
                if neig.val not in visited and neig not in queue:
                    queue.append(neig)
        return n_node


Solution().cloneGraph(Node(1, [Node(2, [Node(3), Node(4)]), Node(5, [Node(6)])]))

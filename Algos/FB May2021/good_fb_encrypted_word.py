
def findEncryptedWord(s):
    n = len(s)
    if n <= 1:
        return s

    mid = (n - 1) // 2

    result = []
    result.append(s[mid])
    result.append(findEncryptedWord(s[:mid]))
    result.append(findEncryptedWord(s[mid + 1:]))
    return ''.join(result)


assert findEncryptedWord('abc') == 'bac'
assert findEncryptedWord('abcd') == 'bacd'
assert findEncryptedWord('abcxcba') == 'xbacbca'
assert findEncryptedWord('facebook') == 'eafcobok'

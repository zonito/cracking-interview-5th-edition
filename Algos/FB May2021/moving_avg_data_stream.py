""" https://leetcode.com/problems/moving-average-from-data-stream/ """

import collections


class MovingAverage:

    def __init__(self, size: int):
        """
        Initialize your data structure here.
        """
        self.size = size
        self.queue = collections.deque()
        self.counter = 0
        self.sum = 0

    def next(self, val: int):
        if self.counter == self.size:
            self.sum -= self.queue.popleft()
            self.counter -= 1
        self.sum += val
        self.queue.append(val)
        self.counter += 1
        return round(self.sum / self.counter, 5)


# Your MovingAverage object will be instantiated and called as such:
obj = MovingAverage(3)
assert obj.next(1) == 1.0
assert obj.next(10) == 5.5
assert obj.next(3) == 4.66667
assert obj.next(5) == 6.0

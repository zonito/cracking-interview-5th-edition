""" https://leetcode.com/explore/interview/card/facebook/55/dynamic-programming-3/3035/ """


class Solution:
    def longestValidParentheses(self, s: str):
        if not s:
            return 0
        n = len(s)
        stack = ['-', s[0]]
        len_stack = [0] * (n + 1)
        i = 1
        while i < n:
            p = s[i]
            if stack[-1] == '(' and p == ')':
                stack.pop()
                l_idx = len(stack)
                len_stack[l_idx] += 2
                if len_stack[l_idx + 1]:
                    len_stack[l_idx] += len_stack[l_idx + 1]
                    len_stack[l_idx + 1] = 0
            else:
                stack.append(p)
            i += 1
        return max(len_stack)


assert Solution().longestValidParentheses('(()') == 2
assert Solution().longestValidParentheses(')()())') == 4
assert Solution().longestValidParentheses('((()))(())') == 10
assert Solution().longestValidParentheses('(()())') == 6
assert Solution().longestValidParentheses('(()())(()()())') == 14
assert Solution().longestValidParentheses('(((()))))') == 8

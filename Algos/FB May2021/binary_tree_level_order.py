from binarytree import build, Node as TreeNode


class Solution:
    def levelOrder(self, root: TreeNode):
        if not root:
            return
        levels = []

        def dfs(node: TreeNode, level: int):
            if not node:
                return
            if len(levels) < level:
                levels.append([])
            levels[level - 1].append(node.val)
            dfs(node.left, level + 1)
            dfs(node.right, level + 1)

        dfs(root, 1)
        return levels


assert Solution().levelOrder(build([3, 9, 20, None, None, 15, 7])) == [[3], [9, 20], [15, 7]]

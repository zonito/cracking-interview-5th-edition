
from typing import List


class Solution:
    def leftMostColumnWithOne(self, binaryMatrix: List[List[int]]):
        if not binaryMatrix or len(binaryMatrix[0]) == 0:
            return -1
        rows, cols = len(binaryMatrix), len(binaryMatrix[0])
        output = cols
        for row_index in range(rows):
            start = 0
            end = min(cols - 1, output)
            while start < end:
                mid = (end + start) // 2
                if binaryMatrix[row_index][mid] == 1:
                    end = mid
                else:
                    start = mid + 1
            if binaryMatrix[row_index][start]:
                output = min(output, start)
        return -1 if output == cols else output


assert Solution().leftMostColumnWithOne([[0, 0], [1, 1]]) == 0
assert Solution().leftMostColumnWithOne([[0, 0, 0, 1], [0, 0, 1, 1], [0, 1, 1, 1]]) == 1

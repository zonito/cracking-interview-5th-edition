from typing import List
import collections


class Solution:
    def insert(self, nums: List[List[int]], num: int, freq: int, top: int):
        if not nums:
            nums.append([num, freq])
            return
        if len(nums) == top and nums[-1][1] > freq:
            return
        for index, obj in enumerate(nums):
            if obj[1] < freq:
                nums.insert(index, [num, freq])
                break
        else:
            nums.append([num, freq])
        if len(nums) > top:
            nums.pop(-1)

    def topKFrequent(self, nums: List[int], k: int):
        # return [k for k, _ in collections.Counter(nums).most_common(k)]
        counter = collections.Counter(nums)
        top_k = []
        for num, freq in counter.items():
            self.insert(top_k, num, freq, k)
        return [k for k, _ in top_k]


assert Solution().topKFrequent([1, 1, 1, 2, 2, 3], 2) == [1, 2]
assert Solution().topKFrequent([1, 1, 1, 2, 2, 3, 3, 3], 2) == [1, 3]

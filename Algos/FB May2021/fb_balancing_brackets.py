""" https://www.facebookrecruiting.com/portal/coding_practice_question/?problem_id=211548593612944 """


def isBalanced(s: str):
    stack = []
    for char in s:
        if char in ['(', '{', '[']:
            stack.append(char)
        elif not stack or (char == '}' and stack[-1] != '{') or (char == ')' and stack[-1] != '(') or (char == ']' and stack[-1] != '['):
            return False
        else:
            stack.pop()
    return not bool(stack)


assert isBalanced('{[()]}')
assert isBalanced('{}()')
assert not isBalanced('{(})')
assert not isBalanced(')')

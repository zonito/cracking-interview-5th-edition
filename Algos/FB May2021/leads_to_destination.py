""" https://leetcode.com/problems/all-paths-from-source-lead-to-destination/ """

import collections
from typing import List


class Solution:

    GRAY = 1
    BLACK = 2

    def __init__(self):
        self.visited = collections.defaultdict(dict)

    def dfs(self, edges: List[List[int]], source: int, dest: int):
        if not edges[source]:
            return source == dest
        self.visited[source] = self.GRAY
        for nei in edges[source]:
            if self.visited[nei] == self.BLACK:
                continue
            if self.visited[nei] == self.GRAY:
                # Circular
                return False
            if not self.dfs(edges, nei, dest):
                return False
        self.visited[source] = self.BLACK
        return True

    def leadsToDestination(self, n: int, edges: List[List[int]], source: int, destination: int):
        nodes = [set() for _ in range(n)]
        for src, dest in edges:
            nodes[src].add(dest)
        print(nodes)

        return self.dfs(nodes, source, destination)


assert Solution().leadsToDestination(1, [], 0, 0)
assert not Solution().leadsToDestination(4, [[0, 1], [0, 3], [1, 2], [2, 1]], 0, 3)
assert Solution().leadsToDestination(5, [[0, 1], [0, 2], [0, 3], [0, 3], [1, 2], [1, 3], [1, 4], [2, 3], [2, 4], [3, 4]], 0, 4)
assert not Solution().leadsToDestination(2, [[0, 1], [1, 1]], 0, 1)
assert Solution().leadsToDestination(4, [[0, 1], [0, 2], [1, 3], [2, 3]], 0, 3)
assert not Solution().leadsToDestination(3, [[0, 1], [0, 2]], 0, 2)

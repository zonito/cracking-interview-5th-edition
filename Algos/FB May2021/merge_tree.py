
from binarytree import build, Node as TreeNode


class Solution:
    def mergeTrees(self, root1: TreeNode, root2: TreeNode):
        if not root1 and not root2:
            return
        # print(root1, root2)
        if root1 and root2:
            root1.val += root2.val
        elif not root1 and root2:
            root1 = TreeNode(root2.val)

        if not root2:
            return root1

        if not root1.left and root2.left:
            root1.left = TreeNode(0)
        elif root1.left and not root2.left:
            root2.left = TreeNode(0)

        if not root1.right and root2.right:
            root1.right = TreeNode(0)
        elif root1.right and not root2.right:
            root2.right = TreeNode(0)

        self.mergeTrees(root1.left, root2.left)
        self.mergeTrees(root1.right, root2.right)
        return root1


ans = Solution().mergeTrees(build([1, 3, 2, 5]), build([2, 1, 3, None, 4, None, 7]))
# print(ans, build([3, 4, 5, 5, 4, None, 7]))
assert ans.values == [3, 4, 5, 5, 4, None, 7]

# More Test Cases
# [1, 3, 2, 5]
# [2, 1, 3, null, 4, null, 7]

# [1]
# [1, 2]

# []
# []

# [1]
# []

# []
# [1]

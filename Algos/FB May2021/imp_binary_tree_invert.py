from binarytree import build, Node as TreeNode


class Solution:
    def invertTree(self, root: TreeNode):
        if not root:
            return
        root.left, root.right = self.invertTree(root.right), self.invertTree(root.left)
        return root


print(Solution().invertTree(build([4, 2, 7, 1, 3, 6, 9])))
print(build([4, 7, 2, 9, 6, 3, 1]))

""" https://leetcode.com/problems/find-largest-value-in-each-tree-row/ """


import sys
from typing import List
from binarytree import build, Node as TreeNode


class Solution:
    def largest_by_level(self, root: TreeNode, level: int, level_storage: List[int]):
        if not root:
            return
        while len(level_storage) < level:
            level_storage.append(-(sys.maxsize-1))
        level_storage[level - 1] = max(level_storage[level - 1], root.val)
        self.largest_by_level(root.left, level + 1, level_storage)
        self.largest_by_level(root.right, level + 1, level_storage)

    def largestValues(self, root: TreeNode):
        if not root:
            return []
        level_storage = []
        self.largest_by_level(root, 1, level_storage)
        return level_storage


assert Solution().largestValues(build([1, 3, 2, 5, 3, None, 9])) == [1, 3, 9]
assert Solution().largestValues(build([1, 2, 3])) == [1, 3]
assert Solution().largestValues(build([1])) == [1]
assert Solution().largestValues(build([1, None, 2])) == [1, 2]
assert Solution().largestValues(build([])) == []

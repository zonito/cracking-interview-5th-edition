""" https://leetcode.com/explore/challenge/card/june-leetcoding-challenge-2021/606/week-4-june-22nd-june-28th/3788/ """

import collections
from typing import List


class Solution:
    def numMatchingSubseq(self, s: str, words: List[str]):
        heads = collections.defaultdict(list)
        for word in words:
            it = iter(word)
            heads[next(it)].append(it)

        ans = 0
        for letter in s:
            old_bucket = heads[letter]
            if not old_bucket:
                continue

            heads[letter] = []
            while old_bucket:
                it = old_bucket.pop()
                nxt = next(it, None)
                if nxt:
                    heads[nxt].append(it)
                    continue
                ans += 1

        return ans


assert Solution().numMatchingSubseq('dsahjpjauf', ["ahjpjau", "ja", "ahbwzgqnuk", "tnmlanowax"]) == 2
assert Solution().numMatchingSubseq('abcdea', ['a', 'bb', 'acd', 'ace']) == 3
assert Solution().numMatchingSubseq('aaa', ['a', 'aa', 'abc', 'aaa']) == 3

'''
Given an array of strings strs, group the anagrams together. You can return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.



Example 1:

Input: strs = ["eat","tea","tan","ate","nat","bat"]
Output: [["bat"],["nat","tan"],["ate","eat","tea"]]

'''

import collections
from typing import List


class Solution:
    def group_anagrams(self, arr: List[str]):
        ans = collections.defaultdict(list)
        for item in arr:
            ans[tuple(sorted(item))].append(item)
        return list(ans.values())


class Solution2:
    def __init__(self):
        self._primes = {
            'a': 2,
            'b': 3,
            'c': 5,
            'd': 7,
            'e': 11,
            'f': 13,
            'g': 17,
            'h': 19,
            'i': 23,
            'j': 29,
            'k': 31,
            'l': 37,
            'm': 41,
            'n': 43,
            'o': 47,
            'p': 53,
            'q': 59,
            'r': 61,
            's': 67,
            't': 71,
            'u': 73,
            'v': 79,
            'w': 83,
            'x': 89,
            'y': 97,
            'z': 101
        }

    def groupAnagrams(self, strs: List[str]):
        subLists = collections.defaultdict(list)
        for string in strs:
            product = 1
            for character in string:
                product *= self._primes[character]
            subLists[product].append(string)
        return list(subLists.values())


assert Solution().group_anagrams(["eat", "tea", "tan", "ate", "nat", "bat"]) == [
    ['eat', 'tea', 'ate'], ['tan', 'nat'], ['bat']]
assert Solution2().groupAnagrams(["eat", "tea", "tan", "ate", "nat", "bat"]) == [
    ['eat', 'tea', 'ate'], ['tan', 'nat'], ['bat']]

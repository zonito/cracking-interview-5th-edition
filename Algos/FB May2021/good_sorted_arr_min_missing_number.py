# Asked in Grab Interview

from typing import List


def get_min_miss_numer(arr: List[int]):
    n = len(arr)
    if arr[-1] == n - 1:
        return arr[-1] + 1
    start = 0
    while start < n:
        mid = (start + n) // 2
        if arr[mid] != mid:
            n = mid
        else:
            start = mid + 1
    return arr[start - 1] + 1


assert get_min_miss_numer([0, 1, 2, 6, 9]) == 3
assert get_min_miss_numer([0, 1, 2, 3, 4]) == 5
assert get_min_miss_numer([0, 1, 2, 4]) == 3
assert get_min_miss_numer([0, 1, 2, 3, 6, 9]) == 4
assert get_min_miss_numer([0, 1, 2, 3, 4, 5, 6, 7, 10]) == 8

""" https://leetcode.com/problems/same-tree/ """

from binarytree import build, Node as TreeNode


class Solution:
    def isSameTree(self, p: TreeNode, q: TreeNode):
        # print(p, q)
        if not p and not q:
            return True
        if (not p and q) or (not q and p):
            return False
        return p.val == q.val and self.isSameTree(p.left, q.left) and self.isSameTree(p.right, q.right)


assert Solution().isSameTree(build([1, 2, 3]), build([1, 2, 3]))
assert not Solution().isSameTree(build([1, 2]), build([1, None, 2]))
assert not Solution().isSameTree(build([1, 2, 1]), build([1, 1, 2]))

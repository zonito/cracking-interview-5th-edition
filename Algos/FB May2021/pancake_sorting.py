"""
https://leetcode.com/problems/pancake-sorting/

3 2 4 1         3
4 2 3 1         4
1 3 2 4         2
3 1 2 4         3
2 1 3 4         2
1 2 3 4
"""

from typing import List


class Solution:
    def pancakeSort(self, arr: List[int]):
        end = len(arr)
        result = []
        while end > 1:
            big_index = arr.index(max(arr[:end]))
            if big_index == end - 1:
                end -= 1
                continue
            temp = arr[:big_index + 1][::-1] + arr[big_index + 1:]
            arr = temp[:end][::-1]
            if big_index:
                result.append(big_index + 1)
            result.append(end)
            end -= 1
        return result


assert Solution().pancakeSort(arr=[3, 2, 4, 1]) == [3, 4, 2, 3, 2]
assert Solution().pancakeSort(arr=[1, 2, 3]) == []

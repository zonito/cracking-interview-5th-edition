''' https://leetcode.com/explore/interview/card/facebook/54/sorting-and-searching-3/3033/ '''


from typing import List


class Solution:
    def intersection(self, nums1: List[int], nums2: List[int]):
        nums1.sort()
        nums2.sort()
        i = 0
        j = 0
        output = []
        while i < len(nums1) and j < len(nums2):
            if nums1[i] > nums2[j]:
                j += 1
            elif nums2[j] > nums1[i]:
                i += 1
            else:
                output.append(nums1[i])
                i += 1
                j += 1
        print(output)
        return output


assert Solution().intersection([1, 2, 2, 1], [2, 2]) == [2, 2]
assert Solution().intersection([4, 9, 5], [9, 4, 9, 8, 4]) == [4, 9]


def findMinArray(arr, k):
    result = []
    while k > 0 and arr:
        # Greedily, we find the smallest element from `arr` and swap it to the front
        result.append(min(arr[:min(k + 1, len(arr))]))  # Core
        minIndex = arr.index(result[-1])
        k -= minIndex
        arr = arr[0: minIndex] + arr[minIndex + 1:]  # move this element to the front
    return result + arr


assert findMinArray([5, 3, 1], 2) == [1, 5, 3]
assert findMinArray([8, 9, 11, 2, 1], 3) == [2, 8, 9, 11, 1]
assert findMinArray([8, 9, 11, 2, 5], 10) == [2, 5, 8, 9, 11]

""" https://leetcode.com/discuss/interview-question/1222342/Facebook-or-onsite-or-matching-processes """

from collections import defaultdict
from typing import List


def longestValidProcessPath(processes: List[str]):
    ln = len(processes)
    # pre-calculate aend - next Bx location at each Ax's index
    aend = defaultdict(lambda: None)
    bdic = defaultdict()
    for j in range(ln-1, -1, -1):
        p = processes[j]
        if p[0] == 'B':
            bdic[p] = j
        elif 'B' + p[1] in bdic:
            aend[j] = bdic['B'+p[1]]

    # starting at each process, try to calculate the longest possible run
    ans = 0
    for i, p in enumerate(processes):
        if p[0] == 'A':
            lset = set()
            front = i
            for j in range(i, ln):
                p2 = processes[j]
                if p2[0] == 'A' and p2 in lset:
                    break   # case: extra A
                if p2[0] == 'A' and not aend[j]:
                    break   # case: loose Ax, no Bx ahead
                if p2[0] == 'B' and 'A'+p2[1] not in lset:
                    break   # loose Bx, no previous Ax

                if p2[0] == 'A':
                    front = max(front, aend[j])
                    lset.add(p2)
                elif front == j:
                    ans = max(ans, front-i+1)
    return ans


assert longestValidProcessPath(['A1', 'A3', 'A1', 'B1']) == 2
assert longestValidProcessPath(['A1', 'B1', 'A2', 'B2']) == 4
assert longestValidProcessPath(['A1', 'B1', 'A2', 'A1', 'B1', 'B2']) == 4
assert longestValidProcessPath(['A1', 'A2', 'B1', 'B2', 'A1', 'A2']) == 4
assert longestValidProcessPath(['A1', 'A2', 'A3', 'B1', 'B2', 'B4', 'B3']) == 0
assert longestValidProcessPath(['A1', 'B1', 'A2', 'B2', 'A1', 'A3', 'B1', 'A2', 'B3', 'B2']) == 6
assert longestValidProcessPath(['A2', 'A5', 'A1', 'B1', 'B2', 'A3', 'A4', 'B5', 'A1', 'B3', 'B1', 'B4']) == 2
assert longestValidProcessPath(['A2', 'A5', 'A1', 'B1', 'B5', 'B2', 'A1', 'A2', 'B1', 'B2', 'B4']) == 6
assert longestValidProcessPath(['A2', 'A5', 'A1', 'B1', 'B2', 'A1', 'B5', 'A2', 'A1', 'B2', 'B1']) == 4
assert longestValidProcessPath(['A2', 'A5', 'A1', 'B1', 'B2', 'A1', 'B1', 'B5', 'A2', 'A1', 'B2', 'B1']) == 4

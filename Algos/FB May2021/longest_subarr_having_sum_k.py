"""
Given an integer array nums and an integer k, return the maximum length of a subarray that sums to k. If there isn't one, return 0 instead.

Example 1:

Input: nums = [1,-1,5,-2,3], k = 3
Output: 4
Explanation: The subarray [1, -1, 5, -2] sums to 3 and is the longest.
Example 2:

Input: nums = [-2,-1,2,1], k = 1
Output: 2
Explanation: The subarray [-1, 2] sums to 1 and is the longest.


Constraints:

1 <= nums.length <= 2 * 105
-104 <= nums[i] <= 104
-109 <= k <= 109
"""


from typing import List


class Solution:
    def maxSubArrayLen(self, nums: List[int], k: int):
        ans = 0
        storage = {0: 0}
        total = 0
        for i, num in enumerate(nums):
            total += num
            if total == k:
                ans = i + 1
            if (total - k) in storage:
                ans = max(ans, i - storage.get(total - k))
            if storage.get(total) is None:
                storage[total] = i
        return ans


assert Solution().maxSubArrayLen([1, -1, 5, -2, 3], 3) == 4
assert Solution().maxSubArrayLen([-2, -1, 2, 1], 1) == 2

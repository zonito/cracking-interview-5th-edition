''' https://www.facebookrecruiting.com/portal/coding_practice_question/?problem_id=3068294883205371 '''

import collections


class Node:
    def __init__(self, data):
        self.val = data
        self.children = []


def count_of_nodes(root, queries, s):
    output = []
    for query in queries:
        node_val, char = query
        node = root
        queue = collections.deque([node])
        while node_val != node.val and queue:
            node = queue.popleft()
            queue.extend(node.children)
        if not queue and node_val != node.val:
            output.append(-1)
            continue
        counter = 0
        queue = [node]
        while queue:
            node = queue.pop()
            if s[node.val - 1] == char:
                counter += 1
            queue.extend(node.children)
        output.append(counter)
    return output


s_1 = "aba"
root_1 = Node(1)
root_1.children.append(Node(2))
root_1.children.append(Node(3))
queries_1 = [(1, 'a')]

assert count_of_nodes(root_1, queries_1, s_1) == [2]

s_2 = "abaacab"
root_2 = Node(1)
root_2.children.append(Node(2))
root_2.children.append(Node(3))
root_2.children.append(Node(7))
root_2.children[0].children.append(Node(4))
root_2.children[0].children.append(Node(5))
root_2.children[1].children.append(Node(6))
queries_2 = [(1, 'a'), (2, 'b'), (3, 'a')]
assert count_of_nodes(root_2, queries_2, s_2) == [4, 1, 2]

''' https://leetcode.com/problems/trapping-rain-water/ '''


from typing import List


class Solution:
    def trap(self, height: List[int]):
        start = 0
        end = len(height) - 1
        output = 0
        unit = 0
        while start < end:
            unit = max(min(height[start], height[end]), unit)
            if height[start] >= height[end]:
                end -= 1
                val = height[end]
            else:
                start += 1
                val = height[start]
            if val < unit:
                output += unit - val
        return output


assert Solution().trap([0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]) == 6
assert Solution().trap([4, 2, 0, 3, 2, 5]) == 9

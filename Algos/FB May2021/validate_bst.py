""" https://leetcode.com/explore/interview/card/facebook/52/trees-and-graphs/266/ """


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def isValidBST(
        self,
        root: TreeNode,
        lower_bound: int = -(2 ** 31) - 1,
        upper_bound: int = 2 ** 31,
    ):
        if root.val >= upper_bound or root.val <= lower_bound:
            return False
        left_status = right_status = True
        if root.left:
            left_status = self.isValidBST(root.left, lower_bound, root.val)
        if left_status and root.right:
            right_status = self.isValidBST(root.right, root.val, upper_bound)
        return left_status and right_status


assert Solution().isValidBST(TreeNode(2147483647))
assert Solution().isValidBST(TreeNode(-2147483648))
assert not Solution().isValidBST(TreeNode(1, TreeNode(1)))
assert not Solution().isValidBST(
    TreeNode(5, TreeNode(1), TreeNode(4, TreeNode(3), TreeNode(6)))
)
assert Solution().isValidBST(TreeNode(2, TreeNode(1), TreeNode(3)))
assert not Solution().isValidBST(
    TreeNode(5, TreeNode(1), TreeNode(6, TreeNode(3), TreeNode(7)))
)

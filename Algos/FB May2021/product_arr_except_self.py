''' https://leetcode.com/problems/product-of-array-except-self/ '''


from typing import List


class Solution:
    def productExceptSelf(self, nums: List[int]):
        n_len = len(nums)
        answer = [1] * n_len
        start_index = 1
        while start_index < n_len:
            answer[start_index] = answer[start_index - 1] * \
                nums[start_index - 1]
            start_index += 1

        start_index = n_len - 1
        right = 1
        while start_index >= 0:
            answer[start_index] *= right
            right *= nums[start_index]
            start_index -= 1

        return answer


assert Solution().productExceptSelf([1, 2, 3, 4]) == [24, 12, 8, 6]

''' https://leetcode.com/problems/range-sum-of-bst/ '''


from binarytree import build, Node as TreeNode


class Solution:
    output = 0

    def sumRange(self, root: TreeNode, low: int, high: int):
        if not root:
            return
        if low <= root.val <= high:
            self.output += root.val
        if root.val >= low:
            self.sumRange(root.left, low, high)
        if root.val <= high:
            self.sumRange(root.right, low, high)

    def rangeSumBST(self, root: TreeNode, low: int, high: int):
        print(root)
        self.sumRange(root, low, high)
        return self.output


assert Solution().rangeSumBST(build([10, 5, 15, 3, 7, None, 18]), 7, 15) == 32
assert Solution().rangeSumBST(build([10, 5, 15, 3, 7, 13, 18, 1, None, 6]), 6, 10) == 23

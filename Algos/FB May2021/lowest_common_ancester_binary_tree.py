''' https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree-iii/ '''


from binarytree import build, Node


class Solution:

    def lowestCommonAncestor(self, p: Node, q: Node):
        pVals = set()

        def traverse_up(root):
            if root == None or root in pVals:
                return root
            pVals.add(root)
            return traverse_up(root.parent)

        return traverse_up(p) or traverse_up(q)


tree = build([3, 5, 1, 6, 2, 0, 8, None, None, 7, 4])
assert Solution(tree).lowestCommonAncestor(tree.left, tree.right) == tree
assert Solution(tree).lowestCommonAncestor(tree.left, tree.right) == tree

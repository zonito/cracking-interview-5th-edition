from typing import List


class Solution:

    def get_next_big_nums(self, arr: List[int], start: int, n: int):
        if n == 0:
            return 1
        counter = 0
        for index, num in enumerate(arr):
            if num > start:
                counter += self.get_next_big_nums(arr[index + 1:], arr[index], n - 1)
        return counter

    def get_next_small_nums(self, arr: List[int], start: int, n: int):
        if n == 0:
            return 1
        counter = 0
        for index, num in enumerate(arr):
            if num < start:
                counter += self.get_next_small_nums(arr[index + 1:], arr[index], n - 1)
        return counter

    def numTeams(self, rating: List[int]):
        i = 0
        counter = 0
        for i in range(len(rating)):
            temp = self.get_next_big_nums(rating[i + 1:], rating[i], 2)
            if not temp:
                temp = self.get_next_small_nums(rating[i + 1:], rating[i], 2)
            counter += temp
        return counter


assert Solution().numTeams([2, 5, 3, 4, 1]) == 3
assert Solution().numTeams([2, 1, 3]) == 0
assert Solution().numTeams([1, 2, 3, 4]) == 4

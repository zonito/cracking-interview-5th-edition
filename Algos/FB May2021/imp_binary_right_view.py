""" https://leetcode.com/problems/binary-tree-maximum-path-sum """


from binarytree import build, Node as TreeNode


class Solution:
    def __init__(self):
        self.levels = dict()

    def seeFromRight(self, root: TreeNode, root_level: int):
        if not root:
            return
        # print(root)
        self.levels[root_level] = root.val
        self.seeFromRight(root.left, root_level + 1)
        self.seeFromRight(root.right, root_level + 1)

    def rightSideView(self, root: TreeNode):
        self.seeFromRight(root, 0)
        return [self.levels[i] for i in range(len(self.levels))]


assert Solution().rightSideView(build([1, 2, 3, None, 5, None, 4])) == [1, 3, 4]
assert Solution().rightSideView(build([1, None, 3])) == [1, 3]
assert Solution().rightSideView(build([])) == []

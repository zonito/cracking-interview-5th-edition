''' https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/3018/ '''


class Solution:
    def validatedIPv4(self, IP: str):
        try:
            return 'IPv4' if len(list(
                filter(
                    lambda obj: obj < 256,
                    map(int, filter(
                        lambda obj: obj and (obj[0] != '0' or len(obj) == 1), IP.split('.')))
                )
            )) == 4 else 'Neither'
        except ValueError:
            return 'Neither'

    def validatedIPv6(self, IP: str):
        try:
            values = list(
                filter(lambda obj: obj and len(obj) < 5, IP.split(':')))
            if len(values) != 8:
                return 'Neither'
            values = map(lambda obj: int(obj, 16), values)
            return 'IPv6' if len(list(values)) == 8 else 'Neither'
        except ValueError:
            return 'Neither'

    def validIPAddress(self, IP: str):
        if len(IP.split('.')) == 4:
            return self.validatedIPv4(IP)
        elif len(IP.split(':')) == 8:
            return self.validatedIPv6(IP)
        return 'Neither'


assert Solution().validIPAddress('172.16.254.1') == 'IPv4'
assert Solution().validIPAddress('2001:0db8:85a3:0:0:8A2E:0370:7334') == 'IPv6'
assert Solution().validIPAddress('256.256.256.256') == 'Neither'
assert Solution().validIPAddress('2001:0db8:85a3:0:0:8A2E:0370:7334:') == 'Neither'
assert Solution().validIPAddress('1e1.4.5.6') == 'Neither'
assert Solution().validIPAddress('1.1.1.') == 'Neither'
assert Solution().validIPAddress('192.0.0.1') == 'IPv4'
assert Solution().validIPAddress('20EE:FGb8:85a3:0:0:8A2E:0370:7334') == 'Neither'

'''
Implement next permutation, which rearranges numbers into the lexicographically next greater permutation of numbers.

If such an arrangement is not possible, it must rearrange it as the lowest possible order (i.e., sorted in ascending order).

The replacement must be in place and use only constant extra memory.

[1,3,2,5,4,3] --> [1,3,3,2,4,5]

'''

from typing import List


class Solution:

    def next_perm(self, arr: List[int]):
        start_index = -1
        for index in range(len(arr) - 1, 0, -1):
            if arr[index - 1] < arr[index]:
                start_index = index - 1
                break

        # Swap
        if start_index == -1:
            return
        index = start_index + 1
        while index < len(arr) and arr[start_index] < arr[index]:
            index += 1
        arr[start_index], arr[index - 1] = arr[index - 1], arr[start_index]

        # Reverse
        i = start_index + 1
        j = len(arr) - 1
        while i < j:
            arr[i], arr[j] = arr[j], arr[i]
            j -= 1
            i += 1
        print(arr)


for num_set in [
    [[1, 3, 2, 5, 4, 3], [1, 3, 3, 2, 4, 5]],
    [[1, 2, 3], [1, 3, 2]],
    [[3, 2, 1], [1, 2, 3]],
    [[1, 3, 2], [2, 1, 3]],
    [[1, 1, 5], [1, 5, 1]],
    [[1, 2, 3, 4, 5, 4, 3, 2, 1], [1, 2, 3, 5, 1, 2, 3, 4, 4]],
    [[1], [1]]
]:
    Solution().next_perm(num_set[0])
    assert num_set[0] == num_set[1]

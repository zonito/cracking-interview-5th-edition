# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")
# Question 3: Telegram: "Hail Zalando!!" --> Date: 30th Aug 1 AM


def solution(A):
# write your code in Python 3.6
hash_table = {}
size = len(A)
for i in range(size):
if not hash_table.get(A[i]):
hash_table[A[i]] = 0
hash_table[A[i]] += 1
counter = 0
for it in hash_table:
total = hash_table[it]
counter += (total * (total - 1)) // 2
if counter > 1000000000:
return 1000000000
return counter

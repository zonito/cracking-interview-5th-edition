## Key rules

1. Assumptions
  * Are you understood question correctly?
  * Did you considered all points?
2. Design Pattern
  * Creational
    * Factory
    * Prototype
    * Singleton
    * Builder
    * Mixing
  * Structural
    * Decorator
    * Facade
    * Flyweight
    * Adapter
    * Proxy
    * Bridge
    * Composite
    * MVC & MVP
    * MVVM
  * Behavioral
    * Interpreter
    * Template Method
    * Chain of responsibilities
    * Command
    * Iterator
    * Mediator
    * Observer
    * State
    * Strategy
    * Visitor
3. Pseudo code first
  * If needed write brute-force
  * Optimize it till you can think
4. Data Structure
  * Hash table
  * Linked list
  * Trees
  * Tries
  * Stack & Queue
  * Vectors
  * List / Array
  * Priority Queue
  * Sets
  * Graphs
5. Algorithms
  * In case of new:
    * Pattern Matching
    * Examplify
    * Simplify & Generalise
    * Base Case & Build
    * Recursion
  * Binary Search
  * Depth first Search
  * BFS
  * Sorts
    * Merge
    * bubble
    * Quick
    * Heap
  * Search
    * Binary
    * Linear
  * Dynamic
  * Greedy
6. Space & time complexity
7. Big-Oh notation
8. Good Code
  * Modularity
  * Docstring
  * Simple
  * Readable
  * Efficient
  * Naming convention
9. Test
  * all Edge Cases
  * Validate inputs


## Last moment

* GCD / LCM
* Graph
* Binary Search / Delete

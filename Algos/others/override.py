class student:

    name = 'zonito'

    def get_name(self):
        return self.name


class student2(student):

    test = 'zonito2'

    # def get_name(self):
    #     return self.test


class obj(student2):
    name = 'love'


# This line will generate an error
# st = student("rahul")

# This line will call the second constructor
st = student()
print(st.name)
print(obj().get_name())
print(obj().get_name())

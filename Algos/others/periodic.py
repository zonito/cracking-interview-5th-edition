"""
  Find whether string S is periodic. Periodic indicates S = nP. e.g.
  if S = abcabc, then n = 3, and P = abc
  if S = xxxx, n = 1, and P = x
  follow up, given string S, find out repetitive pattern of P
"""


def factors(n):
    f = []
    for i in range(1, n):
        if n % i == 0:
            f.append(i)
    return f


def periodic(s):
    f = factors(len(s))
    for i in f:
        m = len(s) / i
        if s[:i] * int(m) == s:
            print(i, s[:i], s)
            return True
    return False
# driver
if __name__ == "__main__":
    periodic('abcabc')
    periodic('abcabcd')
    periodic('xxxxxx')
    periodic('aabbaaabba')

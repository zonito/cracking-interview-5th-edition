class Solution:
    # @param A : string
    # @param B : string
    # @return an integer

    def strStr(self, A, B):
        ln_b = len(B)
        ln_a = len(A)
        counter = 0
        a_counter = 0
        while a_counter < ln_a:
            if A[a_counter] != B[0] and counter == 0:
                a_counter += 1
                continue
            # print a_counter, A[a_counter], B[counter]
            if A[a_counter] == B[counter]:
                counter += 1
                if counter == ln_b:
                    return a_counter - ln_b + 1
            else:
                # print 'fail', counter
                a_counter -= counter
                counter = 0
            a_counter += 1
        return -1

print Solution().strStr('bbbbbbbbab', 'bbba')
print Solution().strStr('bbbbbbbbab', 'baba')

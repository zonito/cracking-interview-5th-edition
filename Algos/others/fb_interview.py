"""
                            1                                         1
           2                                3                         3
    4             5                               6                   6
       7                                                              7

Given a binary tree, output the nodes that can be seen on from the right side
"""


class BinaryTreeMisc(object):

    def get_right_view_values(self, tree):

        queue = Queue()
        result = []
        # Level 1
        queue.enqueue(tree)  # 1
        size = 1
        counter = 0
        while queue.is_empty() or counter < 2 ** size:  # 2 ** 1 = 2, 2 ** 2 = 4
            node = queue.dequeue()  # 1, 2
            if node:
                if node.left:
                    queue.enqueue(node.left)  # 2
                if node.right:
                    queue.enqueue(node.right)  # 3
            counter += 2
            if 2 ** size <= counter:
                # Append Right most value in result
                result.append(queue.getNumbers()[-1])
                size += 1
                counter = 0

    def get_appr_2(self, node):
        if not node:
            return []
        if not node.left and not node.right:
            return [node.data]
        right_part = self.get_appr_2(node.right)
        left_part = self.get_appr_2(node.left)
        if right_part:    # [6]
            return right_part[-1]
        if left_part:
            return left_part[-1]
        return []


#


# Queue: [4,5,6]
# Size = 3 = 8
# Result = [1, 3, 6, 7]
# Space : O(2^n)
# Time: O(2^n)


[1]
1 - -> [3]

[2][3] - -> [6]
[4, 5][6] - -> [7]
[7][]

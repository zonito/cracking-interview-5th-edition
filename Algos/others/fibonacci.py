def fibonacci(num):
    prev = 0
    nxt = 1
    if num < 0:
        print "Incorrect input"
    elif num == 0:
        return prev
    elif num == 1:
        return nxt
    for _ in range(2, num):
        temp = prev + nxt
        prev = nxt
        nxt = temp
    return nxt

print fibonacci(20)

"""Closest Number"""


def get_inputs():
    """Read inputs_trains.txt file and return array of inputs."""
    inputs = []
    with open('vm_closest.txt', 'r') as file_obj:
        inputs = file_obj.read().split('\n')
    return inputs[:len(inputs) - 1]


def _get_minimal_difference(numbers):
    """Return minimal difference number from given numbers"""
    differences = []
    total_numbers = len(numbers)
    for i in range(total_numbers - 1):
        difference = numbers[i + 1] - numbers[i]
        if difference and difference not in differences:
            differences.append(difference)
    return min(differences)


def _get_pairs(numbers, minimal_difference):
    """Return pairs"""
    pairs = []
    total_numbers = len(numbers)
    for i in range(total_numbers - 1):
        diff = numbers[i + 1] - numbers[i]
        if diff == minimal_difference:
            pairs.append('%s %s' % (numbers[i], numbers[i + 1]))
    return pairs


def closestNumbers(numbers):
    """Find closest number from given numbers."""
    numbers = sorted(numbers)
    minimal_difference = _get_minimal_difference(numbers)
    pairs = _get_pairs(numbers, minimal_difference)
    print '\n'.join(pairs)


if __name__ == "__main__":
    _INPUTS = map(int, get_inputs())
    _number_cnt = len(_INPUTS)
    closestNumbers(_INPUTS)

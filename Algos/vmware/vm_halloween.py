"""Closest Number"""


def get_inputs():
    """Read inputs_trains.txt file and return array of inputs."""
    inputs = []
    with open('vm_halloween.txt', 'r') as file_obj:
        inputs = file_obj.read().split('\n')
    return inputs[:len(inputs) - 1]


def max_Chocolates(arr):
    """Find closest number from given numbers."""
    results = []
    for number_of_cuts in arr:
        horizontal = vertical = number_of_cuts // 2
        if number_of_cuts % 2 != 0:
            horizontal += 1
        results.append(horizontal * vertical)
    return results


if __name__ == "__main__":
    max_Chocolates(map(int, get_inputs()))

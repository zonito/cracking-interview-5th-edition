10
^ ["Train (noun)"](http://www.askoxford.com/concise_oed/train?view=uk). (definition – Compact OED). Oxford University Press. Retrieved 2008-03-18.
^ Hello
^ World
^ C is a programming language.
^
^ Atchison, Topeka and Santa Fe Railway (1948). <em>Rules: Operating Department</em>. p.&nbsp;7.
^ [Hydrogen trains](http://www.hydrogencarsnow.com/blog2/index.php/hydrogen-vehicles/i-hear-the-hydrogen-train-a-comin-its-rolling-round-the-bend/)
^ [Vehicle Projects Inc. Fuel cell locomotive](http://www.bnsf.com/media/news/articles/2008/01/2008-01-09a.html)
^ Central Japan Railway (2006). <em>Central Japan Railway Data Book 2006</em>. p.&nbsp;16.
^ ["Overview Of the existing Mumbai Suburban Railway"](http://web.archive.org/web/20080620033027/http://www.mrvc.indianrail.gov.in/overview.htm). _Official webpage of Mumbai Railway Vikas Corporation_. Archived from [the original](http://www.mrvc.indianrail.gov.in/overview.htm) on 2008-06-20. Retrieved 2008-12-11.

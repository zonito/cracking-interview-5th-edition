"""Closest Number"""


def get_inputs():
    """Read inputs_trains.txt file and return array of inputs."""
    inputs = []
    with open('vm_detect_domain.txt', 'r') as file_obj:
        inputs = file_obj.read().split('\n')
    return inputs[:len(inputs) - 1]


def get_raw_inputs():
    """Get raw inputs."""
    total_inputs = input()
    return [total_inputs] + [raw_input() for _ in range(total_inputs * 2)]


def printDomains(arr):
    """Print available domains in given text arr"""
    # domains = []
    # for line in arr:
    #     if 'http://' in line:
    #         temp_arr = line.split('http://')
    #         for value in temp_arr[1:]:
    #             first_part = value.split('/')[0].lower()
    #             for char in first_part:
    #                 if ((ord(char) >= 97 and ord(char) < 123) or
    #                         (ord(char) >= 48 and ord(char) < 58) or
    #                         ord(char) in [46]):
    #                     continue
    #                 break
    #             else:
    #                 if first_part.find('www.') == 0:
    #                     domain = first_part[4:]
    #                 elif first_part.find('web.') == 0:
    #                     domain = first_part[4:]
    #                 elif first_part.find('ww2.') == 0:
    #                     domain = first_part[4:]
    #                 domains.append(domain)
    # domains = sorted(set(domains))
    # print ','.join(domains)
    # Enter your code here. Read input from STDIN. Print output to STDOUT
    import re
    tags = set()
    for i in range(int(arr[0])):
        val = arr[i + 1]
        # print val
        t = re.findall(
            r"[=\'\"](?:https{0,1}\:\/\/(?:ww[w0-9]\.){0,1})([0-9a-zA-Z][0-9a-zA-Z_\-\.]+\.[a-zA-Z]+)", val)
        # print t
        for tag in t:
            if tag not in tags:
                tags.add(tag)
    taglist = sorted(list(tags))
    print ';'.join(taglist)


if __name__ == "__main__":
    printDomains(get_inputs())

"""Closest Number"""


def get_inputs():
    """Read inputs_trains.txt file and return array of inputs."""
    inputs = []
    with open('vm_sherlock_gcd.txt', 'r') as file_obj:
        inputs = file_obj.read().split('\n')
    return inputs[:len(inputs) - 1]


def get_raw_inputs():
    """Get raw inputs."""
    total_inputs = input()
    return [total_inputs] + [raw_input() for _ in range(total_inputs * 2)]


def gcd(num1, num2):
    """
    Recursive function to find gcd of given 2 numbers.
    Euclid's algorithm to find gcd.
    """
    if num1 % num2 == 0:
        return num2
    else:
        return gcd(num2, num1 % num2)


def multiple_gcd(numbers):
    """Find GCD for all available numbers to calculate relative prime"""
    return reduce(gcd, numbers)


def find_sherlock_gcd(arr):
    """Find closest number from given numbers."""
    for i in range(1, int(arr[0]) * 2, 2):
        total_numbers = int(arr[i])
        if total_numbers < 2:
            print 'NO'
            continue
        values = map(int, arr[i + 1].split(' '))
        if multiple_gcd(values) == 1:
            print "YES"
        else:
            print "NO"


if __name__ == "__main__":
    find_sherlock_gcd(get_raw_inputs())

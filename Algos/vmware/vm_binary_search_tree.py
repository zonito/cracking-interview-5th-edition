"""Closest Number"""


def get_inputs():
    """Read inputs_trains.txt file and return array of inputs."""
    inputs = []
    with open('vm_binary_search_tree.txt', 'r') as file_obj:
        inputs = file_obj.read().split('\n')
    return inputs[:len(inputs) - 1]


def get_raw_inputs():
    """Get raw inputs."""
    total_inputs = input()
    return [total_inputs] + [raw_input() for _ in range(total_inputs * 2)]


def find_bs_tree(arr):
    """Find binary search tree"""
    print arr


if __name__ == "__main__":
    find_bs_tree(get_inputs())

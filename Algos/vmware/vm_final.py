"""Closest Number"""


def get_inputs():
    """Read inputs_trains.txt file and return array of inputs."""
    inputs = []
    with open('vm_final.txt', 'r') as file_obj:
        inputs = file_obj.read().split('\n')
    return inputs[:len(inputs) - 1]


def get_raw_inputs():
    """Get raw inputs."""
    total_inputs = input()
    return [total_inputs] + [raw_input() for _ in range(total_inputs * 2)]


def printDomains(arr):
    arr = map(int, arr)
    numbers = arr[1:]
    bucket = {}
    for number in numbers:
        if number in bucket:
            bucket[number] += 1
            continue
        bucket[number] = 1
    for key, value in bucket.items():
        if value % 2 != 0:
            return key
    return


def compress_str(value):
    count = 0
    last_char = None
    result = []
    value += '-'
    for char in value:
        if char != last_char:
            if last_char:
                if count > 1:
                    result.append(last_char + str(count))
                else:
                    result.append(last_char)
            last_char = char
            count = 1
        else:
            count += 1
    return ''.join(result)


if __name__ == "__main__":
    print compress_str(get_inputs()[0])

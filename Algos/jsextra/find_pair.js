/**
Check if an array can be divided into pairs whose sum is divisible by k
Given an array of integers and a number k, write a function that returns true
if given array can be divided into pairs such that sum of every pair is
divisible by k.

Examples:

Input: arr[] = {9, 7, 5, 3}, k = 6
Output: True
We can divide array into (9, 3) and (7, 5).
Sum of both of these pairs is a multiple of 6.

Input: arr[] = {92, 75, 65, 48, 45, 35}, k = 10
Output: True
We can divide array into (92, 48), (75, 65) and
(45, 35). Sum of all these pairs is a multiple of 10.

Input: arr[] = {91, 74, 66, 48}, k = 10
Output: False
*/
"use strict";

function isPairs(arr, k) {
  if (arr.length % 2 !== 0) {
    return false;
  }
  var freq = {},
    sum = 0;
  arr.forEach(function (item) {
    var rem = item % k;
    freq[rem] = (freq[rem] || 0) + 1;
    sum += item;
  });
  if (sum % k !== 0) {
    return false;
  }
  for (var i = 0; i < arr.length; i += 1) {
    var rem = arr[i] % k;
    if (2 * rem === k) {
      if (freq[rem] % 2 !== 0) {
        return false;
      }
    } else if (freq[rem] !== freq[k - rem]) {
      return false;
    }
  }
  return true;
}

console.log(isPairs([15, 7, 5, 3], 6));

/**
Find the maximum subarray XOR in a given array
Given an array of integers. find the maximum XOR subarray value in given array.
Expected time complexity O(n).

Examples:

Input: arr[] = {1, 2, 3, 4}
Output: 7
The subarray {3, 4} has maximum XOR value

Input: arr[] = {8, 1, 2, 12, 7, 6}
Output: 15
The subarray {1, 2, 12} has maximum XOR value

Input: arr[] = {4, 6}
Output: 6
The subarray {6} has maximum XOR value
*/
"use strict";

function findMaxXOR(arr) {
  var i, curXOR = 0,
    maxXOR = 0;
  arr.forEach(function (_, index) {
    curXOR = 0;
    for (i = index; i < arr.length; i += 1) {
      curXOR = curXOR ^ arr[i];
      maxXOR = Math.max(maxXOR, curXOR);
    }
  });
  return maxXOR;
}

console.log(findMaxXOR([1, 2, 3, 4]));
console.log(findMaxXOR([8, 1, 2, 12, 7, 6]));
console.log(findMaxXOR([4, 6]));

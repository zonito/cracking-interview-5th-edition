/**
Sum of bit differences among all pairs
Given an integer array of n integers, find sum of bit differences in all pairs that
can be formed from array elements. Bit difference of a pair (x, y) is count of
different bits at same positions in binary representations of x and y.
For example, bit difference for 2 and 7 is 2. Binary representation of 2 is
010 and 7 is 111 ( first and last bits differ in two numbers).

Examples:

Input: arr[] = {1, 2}
Output: 4
All pairs in array are (1, 1), (1, 2)
                       (2, 1), (2, 2)
Sum of bit differences = 0 + 2 +
                         2 + 0
                      = 4

Input:  arr[] = {1, 3, 5}
Output: 8
All pairs in array are (1, 1), (1, 3), (1, 5)
                       (3, 1), (3, 3) (3, 5),
                       (5, 1), (5, 3), (5, 5)
Sum of bit differences =  0 + 1 + 1 +
                          1 + 0 + 2 +
                          1 + 2 + 0
                       = 8
*/
"use strict";
var bd = {
  bitHash: {},
  combination: function (arr) {
    var i, j, result = [];
    for (i = 0; i < arr.length; i++) {
      for (j = 0; j < arr.length; j++) {
        result.push([arr[i], arr[j]]);
      }
    }
    return result;
  },
  difference: function (pair) {
    var diff = 0,
      i;
    for (i = 0; i < pair[0].length; i++) {
      if (pair[0].charAt(i) !== pair[1].charAt(i)) {
        diff += 1;
      }
    }
    return diff;
  },
  toBin: function (num, len) {
    var result = [];
    while (num) {
      result.push(num % 2);
      num = Math.floor(num / 2);
    }
    var remaining = len - result.length + 1;
    return (remaining > 0 ? (new Array(len - result.length + 1)).join("0") : "") + result.reverse().join("");
  },
  get: function (arr) {
    arr.forEach(function (item) {
      if (!bd.bitHash.hasOwnProperty(item)) {
        bd.bitHash[item] = bd.toBin(item, 5);
      }
    });
    var sum = 0,
      arrComb = this.combination(arr);
    arrComb.forEach(function (pair) {
      sum += bd.difference(
        [bd.bitHash[pair[0]], bd.bitHash[pair[1]]]
      );
    });
    return sum;
  }
};

console.log(bd.get([1, 2]));
console.log(bd.get([1, 3, 5]));
console.log(bd.get([1, 3, 5, 10, 11, 4, 6, 5]));

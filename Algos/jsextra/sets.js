function Sets() {
  this.dataStore = [];
  this.add = function (element) {
    if (this.dataStore.indexOf(element) === -1) {
      this.dataStore.push(element);
      return true;
    }
    return false;
  };
  this.remove = function (element) {
    this.dataStore = this.dataStore.filter(function (item) {
      return item !== element;
    });
  };
  this.size = function () {
    return this.dataStore.length;
  };
  this.union = function (otherSet) {
    var obj = new Sets();
    otherSet.dataStore.forEach(function (item) {
      obj.add(item);
    });
    this.dataStore.forEach(function (item) {
      obj.add(item);
    });
    return obj;
  };
  this.intersect = function (otherSet) {
    var obj = new Sets();
    this.dataStore.forEach(function (item) {
      if (otherSet.dataStore.indexOf(item) > -1) {
        obj.add(item);
      }
    });
    return obj;
  };
  this.subset = function (otherSet) {
    if (this.size() > otherSet.size()) {
      return false;
    }
    return this.dataStore.every(function (item) {
      return otherSet.dataStore.indexOf(item) > -1;
    });
  };
  this.difference = function (otherSet) {
    return this.dataStore.filter(function (item) {
      return otherSet.indexOf(item) === -1;
    });
  };
  this.contains = function (element) {
    return this.dataStore.indexOf(element);
  };
  this.show = function () {
    console.log(this.dataStore.join());
  };
}

var names = new Sets();
names.add("David");
names.add("Jennifer");
names.add("Cynthia");
names.add("Mike");
names.add("Raymond");
if (names.add("Mike")) {
  console.log("Mike added");
} else {
  console.log("Can't add Mike.");
}
names.show();
names.remove("Mike");
names.show();
var temps = new Sets();
temps.add("Love");
temps.add("David");
var result = names.union(temps);
result.show();
result = names.intersect(temps);
result.show();
temps.remove("Love");
temps.show();
console.log(temps.subset(names));

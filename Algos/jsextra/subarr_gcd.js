/**
Count number of subsets of a set with GCD equal to a given number
Given a set of positive integer elements, find count of subsets with GCDs
equal to given numbers.

Examples:

Input:  arr[] = {2, 3, 4}, gcd[] = {2, 3}
Output: Number of subsets with gcd 2 is 2
        Number of subsets with gcd 3 is 1
The two subsets with GCD equal to 2 are {2} and {2, 4}.
The one subset with GCD equal to 3 ss {3}.

Input:  arr[] = {6, 3, 9, 2}, gcd = {3, 2}
Output: Number of subsets with gcd 3 is 5
        Number of subsets with gcd 2 is 2
The five subsets with GCD equal to 3 are {3}, {6, 3},
{3, 9}, {6, 9) and {6, 3, 9}.
The two subsets with GCD equal to 2 are {2} and {2, 6}
*/
"use strict";

function findGCD2(arr, gcd) {
  var arrMax = 0,
    freq = {},
    subsets = {};
  arr.forEach(function (item) {
    arrMax = arrMax > item ? arrMax : item;
    freq[item] = freq.hasOwnProperty(item) ? freq[item] + 1 : 1;
  });
  // console.log(freq);
  var i, sub = 0,
    add, j;
  for (i = arrMax; i >= 1; i -= 1) {
    sub = 0;
    add = freq[i] || 0;
    // console.log(i, add, sub);
    for (j = 2; j * i <= arrMax; j += 1) {
      // console.log(j, j * i);
      add += freq[j * i] || 0;
      sub += subsets[j * i] || 0;
      // console.log('-->', add, sub);
    }
    subsets[i] = Math.pow(2, add) - 1 - sub;
  }
  for (i = 0; i < gcd.length; i += 1) {
    console.log("Number of subsets with gcd " + gcd[i] + " is " + subsets[gcd[i]]);
  }
}
findGCD2([3, 9, 2, 15, 75, 6], [3, 2]);

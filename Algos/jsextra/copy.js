"use strict";

function x() {
  this.a = 1;
  var b = 1;
}

var y = {
  a: {
    b: 1
  }
};

Object.prototype.clone = function (obj) {
  if (null === obj || "object" !== typeof obj) {
    return obj;
  }
  var copy = obj.constructor();
  // console.log(copy);
  Object.keys(obj).forEach(function (item) {
    if (typeof obj[item] === "object") {
      copy[item] = Object.clone(obj[item]);
    } else {
      copy[item] = obj[item];
    }
  });
  return copy;
};

var xObj = new x();
xObj.a = 2;
console.log(xObj);
var x1Obj = new x();
console.log(x1Obj);
var yObj = Object.clone(y);
yObj.a.b = 2;
console.log(yObj);
var y1Obj = Object.clone(y);
console.log(y1Obj);

var d1 = new Date();

/* Wait for 5 seconds. */
var start = (new Date()).getTime();
while ((new Date()).getTime() - start < 5000);

var d2 = Object.clone(d1);
console.log("d1 = " + d1.toString() + "\nd2 = " + d2.toString());

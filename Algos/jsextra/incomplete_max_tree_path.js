// Given a triangle like the following
//       3
//     4   5
//   6   7   8
// 1. How many nodes would you have, for 20 rows?
// 2. How to find the largest sum from the top of the triangle to the one of the
// nodes at the bottom. In other words, if you consider it as a tree, find the
// max sum of all path from root to the leaf.

"use strict";

function Node() {
  var pub = {},
    priv = {};
  pub.left = null;
  pub.right = null;
  priv.data = null;
  pub.setData = function (data) {
    priv.data = data;
  };
  pub.getData = function () {
    return priv.data;
  };
  return pub;
}

var node = new Node();
node.setData(10);
console.log(node.getData());

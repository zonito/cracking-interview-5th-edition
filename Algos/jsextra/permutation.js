// Permutation
"use strict";

var allArrays = [
  ['a', 'b'],
  ['c', 'z'],
  ['d', 'e', 'f']
];

function getPermutation(array, prefix) {
  prefix = prefix || '';
  if (!array.length) {
    return prefix;
  }

  var result = array[0].reduce(function (result, value) {
    return result.concat(getPermutation(array.slice(1), prefix + value));
  }, []);
  return result;
}

console.log(getPermutation(allArrays));

// Actual permutation logic

function permutation(input) {
  var permArr = [],
    usedChars = [];

  function permute(input) {
    var i;
    for (i = 0; i < input.length; i += 1) {
      usedChars.push(input.splice(i, 1)[0]);
      if (input.length === 0) {
        permArr.push(usedChars.slice());
      }
      permute(input);
      input.splice(i, 0, usedChars.pop());
    }
    return permArr;
  }
  return permute(input);
}
var x = [1, 2, 3];
// console.log(permutation(x));
// console.log(x.splice(2, 1)) // start, length
// console.log(x.slice()); // start, end
// console.log(x)

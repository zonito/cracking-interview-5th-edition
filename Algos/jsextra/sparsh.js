/***
Find Next Sparse Number
A number is Sparse if there are no two adjacent 1s in its binary
representation. For example 5 (binary representation: 101) is sparse,
but 6 (binary representation: 110) is not sparse. Given a number x, find the
smallest Sparse number which greater than or equal to x.

Examples:

Input: x = 6
Output: Next Sparse Number is 8

Input: x = 4
Output: Next Sparse Number is 4

Input: x = 38
Output: Next Sparse Number is 40

Input: x = 44
Output: Next Sparse Number is 64
*/
"use strict";

function findNextSparse(number) {
  // Start converting number in binary representation
  var lastBinNumber, temp;
  while (true) {
    temp = number;
    while (temp > 0) {
      lastBinNumber = temp % 2;
      temp = Math.floor(temp / 2);
      // Check if it has adjacent 1s.
      if (lastBinNumber === 1 && (temp % 2) === lastBinNumber) {
        temp = -1;
      }
    }
    // Check whether we got our number or not
    if (temp !== -1) {
      break;
    }
    number += 1;
  }
  return number;
}

console.log(findNextSparse(6));
console.log(findNextSparse(4));
console.log(findNextSparse(38));
console.log(findNextSparse(44));

import base64
import heapq
import bisect
def p(x): return print(x)


# x = [2, 1, 7, 4, 2]
# x = list(map(lambda x: -x, x))
# heapq.heapify(x)
# print(x)
# print(heapq.nlargest(1, x))
# print(heapq.heappop(x))
y = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
p(bisect.bisect_left(y, -1))


p(base64.b64encode(b'https://www.educative.io/courses/grokking-the-system-design-interview/m2ygV4E81AR'))

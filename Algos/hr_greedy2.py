"""
8 1
4 2
5 6
3 1
4 3
"""

INPUTS = [
    '8 1',
    '4 2',
    '5 6',
    '3 1',
    '4 3'
]

INPUTS = [
    '1 3',
    '2 3',
    '3 3'
]

OBJ = {}
ORDER = {}
for i, line in enumerate(INPUTS):
    key = line.replace(' ', '_')
    OBJ[key] = sum(map(int, line.split(' ')))
    ORDER[OBJ[key]] = i + 1

# print OBJ, ORDER
SORTED = {}

MAXI = 0
for key, val in OBJ.items():
    if val > MAXI:
        MAXI = val
    SORTED[val] = key
    # print key, val

# print MAXI
RESULT = []
for i in range(MAXI):
    if SORTED.get(i + 1):
        RESULT.append(str(ORDER[i + 1]))
print ' '.join(RESULT)

"""
Implement a MyQueue class which implements a queue using two stacks.
"""

import stack


class MyQueue(object):
    """Custom Queue from 2 stacks."""
    actual_stack = stack.Stack()
    mock_queue = stack.Stack()

    def add(self, data):
        """Add data to stack, instead of queue / mock_queue."""
        self.actual_stack.push(data)

    def populate_mock_queue(self):
        """Move stack data to mock queue stack."""
        if not self.mock_queue.size():
            while self.actual_stack.size():
                self.mock_queue.push(self.actual_stack.pop())

    def peek(self):
        """Return peek of queue."""
        self.populate_mock_queue()
        return self.mock_queue.peek()

    def remove(self):
        """Remove data from queue."""
        self.populate_mock_queue()
        return self.mock_queue.pop()

    def display(self):
        """Just display."""
        self.populate_mock_queue()
        return self.mock_queue.display(True)


QUEUE = MyQueue()
QUEUE.add(1)
QUEUE.add(2)
# print QUEUE.display()
print QUEUE.peek()
print QUEUE.remove()
QUEUE.add(3)
print QUEUE.peek()
print QUEUE.remove()
print QUEUE.peek()

def solution(A):
    # write your code in Python 2.7
    if not A:
        return -1
    if len(A) == 1:
        return 0
    total = sum(A)
    first_part = 0
    second_part = 0
    for index, value in enumerate(A):
        equilibrium_number = A[index]
        if index == 0:
            second_part = total - value
            first_part = 0
        else:
            first_part += last_value
            second_part -= value
        if first_part == second_part:
            return index
        last_value = value
    return -1

print solution([-2, 4, -12, 5, 1, -6, 2, 1])
print solution([])
print solution([1])

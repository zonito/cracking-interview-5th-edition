"""
How would you design a stack which, in addition to push and pop, also has a
function min which returns the minimum element? Push, pop and min should
all operate in O(1) time.
"""

import random
import stack as st

STACK = st.Stack()
for _ in range(10):
    STACK.push(random.randint(1, 100), True)
STACK.display()
print STACK.peek()

"""
Write a program to sort a stack in ascending order (with biggest items on top).
You may use at most one additional stack to hold items, but you may not copy
the elements into any other data structure (such as an array). The stack
supports the following operations: push, pop, peek, and isEmpty.
"""

import random
import stack


def sort_stack(unsorted_stack):
    """Sort given unsorted stack."""
    sorted_stack = stack.Stack()
    if unsorted_stack.is_empty():
        return sorted_stack
    while not unsorted_stack.is_empty():
        temp = unsorted_stack.pop()
        while not sorted_stack.is_empty() and sorted_stack.peek() > temp:
            unsorted_stack.push(sorted_stack.pop())
        sorted_stack.push(temp)
    return sorted_stack

UNSORTED_STACK = stack.Stack()
for _ in range(5):
    UNSORTED_STACK.push(random.randint(1, 100))
UNSORTED_STACK.display()

print sort_stack(UNSORTED_STACK).display(True)

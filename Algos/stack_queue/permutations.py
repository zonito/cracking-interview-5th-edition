"""Permutations"""


# def get_permutation(arr, container, index=0):
#     """Return set of permutation of given arr"""
#     if index == len(arr):
#         print(' '.join(container))
#         return
#     if index > len(arr):
#         return
#     for k in arr[index]:
#         container[index] = k
#         get_permutation(arr, container, index + 1)
# ARR = [
#     ['a', 'b'],
#     ['c', 'z'],
#     ['d', 'e', 'f']
# ]
# get_permutation(ARR, ['' for i in range(len(ARR))])


def all_perms(elements):
    if len(elements) <= 1:
        yield elements
        return
    for perm in all_perms(elements[1:]):
        for j in range(len(elements)):
            # nb elements[0:1] works in both string and list contexts
            yield perm[:j] + elements[0:1] + perm[j:]

result = []
for p in all_perms([1, 2, 3, 4]):
    result.append(int(''.join(map(str, p))))

print(sorted(result))

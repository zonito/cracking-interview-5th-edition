"""
An animal shelter holds only dogs and cats, and operates on a strictly
"first in, first out" basis. People must adopt either the "oldest"
(based on arrival time) of all animals at the shelter, or they can select
whether they would prefer a dog or a cat (and will receive the oldest animal of
that type). They cannot select which specific animal they would like.
Create the data structures to maintain this system and implement operations
such as enqueue, dequeueAny, dequeueDog and dequeueCat. You may
use the built-in LinkedList data structure.
"""

import linked_list


class Animal(object):
    """Animal Base class."""
    order = 0
    name = None

    def __init__(self, name):
        """Constructor to set name."""
        self.name = name

    def set_order(self, order):
        """Set order of either cat / dog."""
        self.order = order

    def get_order(self):
        """Return order."""
        return self.order

    def __str__(self):
        return self.name


class Cat(Animal):
    """To Create Cat Instance."""

    def __init__(self, name):
        super(Cat, self).__init__(name)


class Dog(Animal):
    """To create dog instance."""

    def __init__(self, name):
        super(Dog, self).__init__(name)


class AnimalQueue(object):
    """Return Dog / Cat whichever comes first."""
    dog_queue = None
    cat_queue = None
    order = 0

    def __init__(self, data):
        """Initialize queue."""
        self.enqueue(data)

    def enqueue(self, data):
        """Enqueue."""
        data.set_order(self.order)
        if isinstance(data, Dog):
            if not self.dog_queue:
                self.dog_queue = linked_list.Node(data)
            else:
                self.dog_queue.append_to_tail(data)
            # linked_list.Node.display(self.dog_queue)
        elif isinstance(data, Cat):
            if not self.cat_queue:
                self.cat_queue = linked_list.Node(data)
            else:
                self.cat_queue.append_to_tail(data)
            # linked_list.Node.display(self.cat_queue)
        self.order += 1

    def dequeue_any(self):
        """Dequeue any either Dog / Cat."""
        if not self.dog_queue:
            return self.dequeue_cat()
        elif not self.cat_queue:
            return self.dequeue_dog()
        if self.cat_queue.peek().get_order() > self.dog_queue.peek().get_order():
            return self.dequeue_dog()
        return self.dequeue_cat()

    def dequeue_dog(self):
        """Dequeue dog"""
        if self.dog_queue:
            dog = self.dog_queue.peek()
            # linked_list.Node.display(self.dog_queue)
            self.dog_queue = self.dog_queue.next_node
            return dog

    def dequeue_cat(self):
        """Dequeue cat"""
        if self.cat_queue:
            cat = self.cat_queue.peek()
            # linked_list.Node.display(self.cat_queue)
            self.cat_queue = self.cat_queue.next_node
            return cat

QUEUE = AnimalQueue(Dog('dog_1'))
QUEUE.enqueue(Cat('cat_2'))
QUEUE.enqueue(Cat('cat_3'))
QUEUE.enqueue(Dog('dog_4'))
QUEUE.enqueue(Dog('dog_5'))
QUEUE.enqueue(Dog('dog_6'))
QUEUE.enqueue(Cat('cat_7'))

# for _ in range(7):
#     print QUEUE.dequeue_any().name
print QUEUE.dequeue_cat().name
print QUEUE.dequeue_cat().name
print QUEUE.dequeue_cat().name
print QUEUE.dequeue_dog().name
print QUEUE.dequeue_dog().name
print QUEUE.dequeue_dog().name
print QUEUE.dequeue_dog().name

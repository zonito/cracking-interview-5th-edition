"""
Google Interview:
A cricketer can score 1, 2, 4 or 6 in a ball. Find in how many ways the
player can score a total of "n" runs without hitting 3 consecutive boundaries.
Note: Scoring 4 or 6 is considered as a boundary.
"""


def max_runs(runs):
    """
      Return number of ways a player can hit given runs
      without 3 consecutive boundaries.
    """
    run_set = []
    # find set
    while runs > 0:
        if runs > 6:
            run_set.append(6)
            runs -= 6
        elif runs > 4:
            run_set.append(4)
            runs -= 4
        elif runs > 2:
            run_set.append(2)
            runs -= 2
        elif runs > 1:
            run_set.append(1)
            runs -= 1
        print runs
    return run_set

print max_runs(15)

"""
Imagine a (literal) stack of plates. If the stack gets too high, it might topple.
Therefore, in real life, we would likely start a new stack when the previous
stack exceeds some threshold. Implement a data structure SetOfStacks that mimics
this. SetOfStacks should be composed of several stacks and should create a
new stack once the previous one exceeds capacity. SetOfStacks.push() and
SetOfStacks.pop() should behave identically to a single stack (that is, popO
should return the same values as it would if there were just a single stack).
FOLLOW UP
Implement a function popAt(int index) which performs a pop operation on
a specific sub-stack.
-- Follow Up: Implement popAt(int index)
    This is a bit trickier to implement, but we can imagine a "rollover" system.
    If we pop an element from stack 1, we need to remove the bottom of stack 2
    and push it onto stack 1. We then need to rollover from stack 3 to stack 2,
    stack 4 to stack 3, etc. You could make an argument that, rather than
    "rolling over," we should be OK with some stacks not being at full capacity.
    This would improve the time complexity (by a fair amount, with a large
    number of elements), but it might get us into tricky situations later on if
    someone assumes that all stacks (other than the last) operate at full
    capacity. There's no "right answer" here; you should discuss this trade-off
    with your interviewer.
"""

import stack as st


class MultipleStack(object):
    """Literal stack implementation of plates."""
    capacity = 0
    counter = 0
    last_stack_index = 0
    stacks = []

    def __init__(self, capacity):
        """Initialize"""
        self.capacity = capacity
        self.stacks.append(st.Stack())

    def display_plates(self):
        """Display stack."""
        print self.stacks
        self.stacks[self.last_stack_index].display()

    def push_plate(self, data):
        """Push data to current stack."""
        if self.is_full():
            self.stacks.append(st.Stack())
            self.last_stack_index += 1
            self.counter = 0
        self.stacks[self.last_stack_index].push(data)
        self.counter += 1

    def pop_plate(self):
        """Pop data from current stack."""
        self.counter -= 1
        data = self.stacks[self.last_stack_index].pop()
        if not self.counter:
            self.last_stack_index -= 1
        return data

    def peek(self):
        """Return peek data."""
        return self.stacks[self.last_stack_index].peek()

    def is_full(self):
        """Return True, if current stack is full."""
        return self.counter >= self.capacity


STACK = MultipleStack(4)
for i in range(10):
    STACK.push_plate(i)
STACK.display_plates()
print STACK.pop_plate()
print STACK.peek()

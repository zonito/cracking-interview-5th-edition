"""
Shortest Superstring Problem
Given a set of n strings arr[], find the smallest string that contains each
string in the given set as substring. We may assume that no string in arr[]
is substring of another string.

Examples:

Input:  arr[] = {"geeks", "quiz", "for"}
Output: geeksquizfor

Input:  arr[] = {"catg", "ctaagt", "gcta", "ttca", "atgcatc"}
Output: gctaagttcatgcatc
"""

import copy


def get_shortest_superstring(arr):
    """Return super string from given arr."""
    temp = copy.deepcopy(arr)
    t_len = len(temp)
    for i in range(t_len):
        length = len(temp[i])
        temp_len = length
        while length > 0:
            length -= 1
            merged_str = None
            for j in range(i + 1, t_len):
                l_index = temp[j].find(temp[i][:length])
                r_index = temp[j].find(temp[i][temp_len - length:])
                if l_index == abs(len(temp[j]) - length):
                    merged_str = temp[j] + temp[i][length:]
                    temp[j] = merged_str
                    del temp[i]
                elif r_index == 0:
                    merged_str = temp[i] + temp[j][length:]
                    temp[i] = merged_str
                    del temp[j]
                if merged_str:
                    print i, j, merged_str, temp
                    if len(temp) == 1:
                        return temp[0]
                    return get_shortest_superstring(temp)
    return


print get_shortest_superstring(['catg', 'ctaagt', 'gcta', 'ttca', 'atgcatc'])
print get_shortest_superstring(['geeks', 'quiz', 'for'])

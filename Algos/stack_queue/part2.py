"""
You are given a list "A" containing "n" integers. It is given that there
exists at least one subset in "A" such that doing a binary OR operation
of all the elements gives a value "X". Find the minimum number of elements
that should be removed from "A" such that no such subset exists.
"""


def get_subset(arr):
    """
    Return subset of minimum elements which is have exact binary OR
    for given arr.
    """
    # Find OR of all elements
    arr_or = reduce(lambda v1, v2: v1 | v2, arr)

    # Reverse the arr to start removing from last and find subset
    arr.reverse()
    result = []
    val = 0
    for num in arr:
        val = val | num
        result.append(num)
        if val != arr_or:
            continue
        break
    return result

SET = get_subset(range(10))
print 'Minimum of %d elements required. Elements are:' % (10 - len(SET)), SET

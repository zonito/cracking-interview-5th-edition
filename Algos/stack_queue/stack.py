"""Stack Implementation."""


class Node(object):
    """just node."""
    data = None
    next_node = None

    def __init__(self, data):
        self.data = data


class Stack(object):
    """Actual stack."""
    top = None
    counter = 0

    def display(self, is_return=False):
        """Display Stack."""
        node = self.top
        result = []
        while node:
            result.append(str(node.data))
            node = node.next_node
        if is_return:
            return ' | '.join(result)
        print ' | '.join(result)

    def push(self, data, is_min=False):
        """Push data to top of stack."""
        node = Node(data)
        if is_min and self.top and self.top.data < data:
            old_data = self.pop()
            self.push(data, True)
            self.push(old_data)
            return
        node.next_node = self.top
        self.top = node
        self.counter += 1

    def pop(self):
        """Pop data from stack"""
        if self.top:
            item = self.top.data
            self.top = self.top.next_node
            self.counter -= 1
            return item
        return None

    def peek(self):
        """Return peek data."""
        if not self.top:
            return None
        return self.top.data

    def size(self):
        """Return size of stack."""
        return self.counter

    def is_empty(self):
        """Return whether stack is empty or not."""
        return not bool(self.counter)

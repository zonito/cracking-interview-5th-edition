"""
Describe how you could use a single array to implement three stacks.
"""

import stack as st


def push_to_stack(array, stack):
    """Push array element to stack."""
    for data in array:
        stack.push(data)


def spread_array(array, no_of_stacks):
    """Divide given array in N number of stacks"""
    # If array is empty or number of stacks is less than 1
    if not array or no_of_stacks < 1:
        return
    # If we have only one stack to spread.
    if no_of_stacks == 1:
        new_stack = st.Stack()
        push_to_stack(array, new_stack)
        new_stack.display()
        return

    total_data = len(array)
    data_per_stack = total_data // no_of_stacks
    new_stack = None
    i = 0
    till = 0
    for i in range(no_of_stacks):
        new_stack = st.Stack()
        till = data_per_stack * (i + 1)
        push_to_stack(array[i * data_per_stack:till], new_stack)
        new_stack.display()
    if till < total_data:
        push_to_stack(array[till:], new_stack)
        new_stack.display()

spread_array(range(12), 2)

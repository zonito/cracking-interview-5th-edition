# pylint: disable=C0111


class LinkedList(object):

    tail = None
    head = None
    prev = None
    nxt = None

    def __init__(self, data):
        self.data = data
        self.tail = self
        self.head = self

    def append(self, data):
        node = LinkedList(data)
        self.tail.nxt = node
        node.prev = self.tail
        self.tail = node

    def find(self, num):
        index = -1
        counter = 0
        node = self.head
        while index == -1:
            if node is None:
                break
            if node.data == num:
                index = counter
            node = node.nxt
            counter += 1
        return index


LIST_OBJ = LinkedList(1)
LIST_OBJ.append(10)
LIST_OBJ.append(20)
LIST_OBJ.append(30)
print LIST_OBJ.tail.data, LIST_OBJ.tail.nxt
print LIST_OBJ.tail.prev.data, LIST_OBJ.tail.prev.nxt.data
print LIST_OBJ.tail.prev.prev.data, LIST_OBJ.tail.prev.prev.nxt.data
print LIST_OBJ.tail.prev.prev.prev.data, LIST_OBJ.tail.prev.prev.prev.nxt.data
print LIST_OBJ.head.data
print LIST_OBJ.head.nxt.data
print LIST_OBJ.head.nxt.nxt.data
print LIST_OBJ.head.nxt.nxt.nxt.data
print LIST_OBJ.head.nxt.nxt.nxt.nxt
print LIST_OBJ.find(20)

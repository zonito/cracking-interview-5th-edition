"""
Check if two given strings are isomorphic to each other
Two strings str1 and str2 are called isomorphic if there is a one to one
mapping possible for every character of str1 to every character of str2.
And all occurrences of every character in "str1" map to same character in
"str2"

Examples:

Input:  str1 = "aab", str2 = "xxy"
Output: True
'a' is mapped to 'x' and 'b' is mapped to 'y'.

Input:  str1 = "aab", str2 = "xyz"
Output: False
One occurrence of 'a' in str1 has 'x' in str2 and
other occurrence of 'a' has 'y'.
"""


def is_isomorphic(string_1, string_2):
    """Return True if given 2 strings are isomorphic, else False."""
    if len(string_1) != len(string_2):
        return False
    mappigs = {}
    for i in range(len(string_2)):
        if ((mappigs.get(string_2[i]) and mappigs[string_2[i]] != string_1[i])
                or (mappigs.get(string_1[i]) and
                    mappigs[string_1[i]] != string_2[i])):
            return False
        elif not mappigs.get(string_2[i]):
            mappigs[string_2[i]] = string_1[i]
            mappigs[string_1[i]] = string_2[i]
    return True

print is_isomorphic('aab', 'xxy')
print is_isomorphic('aab', 'xyz')

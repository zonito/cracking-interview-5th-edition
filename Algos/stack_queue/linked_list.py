"""
Very basic single linked list.
"""

import random


class Node(object):
    """Node representation for linked list."""
    next_node = None
    data = None

    def __init__(self, data):
        self.data = data

    def append_to_tail(self, data):
        """Append data to the end of linked list."""
        end = Node(data)
        current = self
        while current.next_node:
            current = current.next_node
        current.next_node = end

    def append_to_head(self, data):
        """Append data to the head of linked list."""
        start = Node(data)
        start.next_node = Node(self.data)
        start.next_node.next_node = self.next_node
        return start

    def peek(self):
        """Return head node data."""
        return self.data

    @staticmethod
    def length(head):
        """Return length of given linked list."""
        count = 1
        while head.next_node:
            head = head.next_node
            count += 1
        return count

    @staticmethod
    def display(head, counter_enabled=None):
        """Print node data."""
        arr = [str(head.data)]
        if counter_enabled:
            counter = 0
        while head.next_node:
            head = head.next_node
            arr.append(str(head.data))
            if counter_enabled:
                counter += 1
                if counter > counter_enabled:
                    break
        print ' -> '.join(arr)

    @staticmethod
    def delete_node(head_node, data):
        """Delete a node and return from singly linked list."""
        if head_node.data == data:
            return head_node.next

        # find and delete it.
        temp = head_node
        while temp:
            if temp.next_node.data == data:
                temp.next_node = temp.next_node.next_node
                return head_node
            temp = temp.next_node

        # Not found
        return head_node


def add_some_values(count=10, is_random=False, special_number=0):
    """For testing: add some values to linked list."""
    node = Node(random.randint(1, special_number or count) if is_random else 0)
    for i in range(count - 1):
        node.append_to_tail(random.randint(1, special_number or count)
                            if is_random else (i + 1))
    return node

# NODE = Node(1)
# NODE.append_to_tail(2)
# NODE.append_to_tail(4)
# NODE.append_to_tail(5)
# print NODE.next_node.next_node.next_node.data
# NODE = Node.delete_node(NODE, 4)
# print NODE.next_node.next_node.data

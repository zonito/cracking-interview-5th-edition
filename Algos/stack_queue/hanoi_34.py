"""
In the classic problem of the Towers of Hanoi, you have 3 towers and N disks of
different sizes which can slide onto any tower. The puzzle starts with disks
sorted in ascending order of size from top to bottom (i.e., each disk sits on
top of an even larger one). You have the following constraints:
(T) Only one disk can be moved at a time.
(2) A disk is slid off the top of one tower onto the next rod.
(3) A disk can only be placed on top of a larger disk.
Write a program to move the disks from the first tower to the last using Stacks.

For Test: http://vornlocher.de/tower.html
"""

import stack as st


def move_disk_2(no_of_disks, origin, destination, buffer_stack):
    """Move given number of disks from origin to destination rod."""
    if no_of_disks <= 0:
        return
    # print 'O --> B, D'
    # print no_of_disks, 'Origin: ', origin.display(True), 'Buffer: ',
    # buffer_stack.display(True), 'Destination: ', destination.display(True)
    move_disk_2(no_of_disks - 1, origin, buffer_stack, destination)
    destination.push(origin.pop())
    # print '%d, B (%s) --> D (%s), O (%s)' % (no_of_disks,
    # buffer_stack.display(True), destination.display(True),
    # origin.display(True))
    move_disk_2(no_of_disks - 1, buffer_stack, destination, origin)

ORIGIN = st.Stack()
TOTAL = 5
for i in range(TOTAL):
    ORIGIN.push(TOTAL - i)
DESTINATION = st.Stack()
ORIGIN.display()
# print 'O --> D, B'
move_disk_2(TOTAL, ORIGIN, DESTINATION, st.Stack())
# print '-' * 10
ORIGIN.display()
DESTINATION.display()

function Queue() {
  this.dataStore = [];
  this.enqueue = function (element) {
    this.dataStore.push(element);
  };
  this.dequeue = function () {
    return this.dataStore.shift();
  };
  this.front = function () {
    return this.dataStore[0];
  };
  this.back = function () {
    return this.dataStore[this.dataStore.length - 1];
  };
  this.toString = function () {
    return this.dataStore.join('\n');
  };
  this.empty = function () {
    return this.dataStore.length <= 0;
  };
}

var queue = new Queue();
queue.enqueue(10);
queue.enqueue(20);
console.log(queue.front());
console.log(queue.back());
console.log(queue.toString());
console.log(queue.empty());
console.log(queue.dequeue());
console.log(queue.dequeue());
console.log(queue.empty());

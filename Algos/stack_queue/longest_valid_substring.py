"""
Length of the longest valid substring
Given a string consisting of opening and closing parenthesis,
find length of the longest valid parenthesis substring.

Examples:

Input : ((()
Output : 2
Explanation : ()

Input: )()())
Output : 4
Explanation: ()()

Input:  ()(()))))
Output: 6
Explanation:  ()(()))
"""

import stack


def get_length(string):
    """Return the longest valid substring length."""
    stk = stack.Stack()
    length = 0
    for char in string:
        if char == '(':
            stk.push(char)
        elif not stk.is_empty():
            if stk.peek() == '(':
                stk.pop()
                length += 2
            else:
                stk.push(char)
    return length

print get_length('(()')
print get_length(')()())')
print get_length('()(()))))')

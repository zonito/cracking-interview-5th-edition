"""
Given a string SS, find the number of "unordered anagrammatic pairs" of
substrings.

Input Format
First line contains TT, the number of testcases. Each testcase consists of
string SS in one line.

Constraints
1<=T<=10
2<=length(S)<=100
String SS contains only the lowercase letters of the English alphabet.

Output Format
For each testcase, print the required answer in one line.

Sample Input#00

2
abba
abcd
Sample Output#00

4
0
Sample Input#01

5
ifailuhkqq
hucpoltgty
ovarjsnrbf
pvmupwjjjf
iwwhrlkpek
Sample Output#01

3
2
2
6
3
Explanation

Sample 00
Let's say S[i,j]S[i,j] denotes the substring Si,Si+1,...,SjSi,Si+1,...,Sj.

testcase 1:
For S=abba, anagrammatic pairs are: {S[1,1],S[4,4]}{S[1,1],S[4,4]},
{S[1,2],S[3,4]}{S[1,2],S[3,4]}, {S[2,2],S[3,3]}{S[2,2],S[3,3]} and
{S[1,3],S[2,4]}{S[1,3],S[2,4]}.

testcase 2:
No anagrammatic pairs.

Sample01
Left as an exercise to you.
"""

INPUTS = ['ifailuhkqq', 'hucpoltgty', 'ovarjsnrbf', 'pvmupwjjjf', 'iwwhrlkpek']
# INPUTS = [raw_input() for _ in range(input())]


def get_anagram_pairs(obj):
    """Return number of anagram pairs."""
    alpha_set = {}
    for index, char in enumerate(obj):
        if char in alpha_set:
            alpha_set[char].append(index)
        else:
            alpha_set[char] = [index]

    # print alpha_set
    result = 0
    for val in alpha_set.values():
        length = len(val)
        counter = 1
        prev_diff = 0
        while length > counter:
            if prev_diff:
                result += 2
            diff = val[counter] - val[counter - 1]
            # print diff, val[counter], val[counter - 1], key
            if diff > 1:
                result += 2
            elif diff == 1:
                result += 1
            counter += 1
            prev_diff = diff
    return result


for obj in INPUTS:
    print get_anagram_pairs(obj)

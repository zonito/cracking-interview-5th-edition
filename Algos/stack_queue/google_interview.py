"""
Google Interview:
A cricketer can score 1, 2, 4 or 6 in a ball. Find in how many ways the
player can score a total of "n" runs without hitting 3 consecutive boundaries.
Note: Scoring 4 or 6 is considered as a boundary.
"""

combos = {}


def all_perms(elements):
    """Return all possible permutations for given elements."""
    if len(elements) <= 1:
        yield elements
    else:
        for perm in all_perms(elements[1:]):
            for j in range(len(elements)):
                # nb elements[0:1] works in both string and list contexts
                yield perm[:j] + elements[0:1] + perm[j:]


def max_runs(runs):
    """
      Return global set of combination a player can hit given runs
      without 3 consecutive boundaries.
    """
    run_set = []
    # find set
    while runs > 0:
        for run in [6, 4, 2, 1]:
            if runs >= run:
                run_set.append(run)
                runs -= run
                break
        # print runs
    return run_set


def validate_set(run_set):
    """
    Return True if given run set is unique and no 3 consecutive boundaries.
    """
    hash_str = reduce(lambda v1, v2: str(v1) + str(v2), run_set)
    if not combos.get(hash_str):
        print hash_str
        combos[hash_str] = True
        boundaries = 0
        for run in run_set:
            if run in [6, 4]:
                boundaries += 1
            else:
                boundaries = 0
            if boundaries == 3:
                return False
        return True
    return False


def find_ways(runs):
    """Return number or ways."""
    sets = max_runs(runs)
    # starts with 1, considering player can hit only ones to achieve runs.
    ways = 1
    # considering if runs are even, player can hit all 2s.
    if runs % 2 == 0:
        ways += 1

    # Permute and validate the sets
    for perm in all_perms(sets):
        if validate_set(perm):
            ways += 1
    return ways

print find_ways(15)
# print validate_set([6, 6, 4, 1])

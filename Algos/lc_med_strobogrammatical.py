
from typing import List


class Solution:

    def find_strobogrammatic(self, n: int) -> List[str]:
        even_mid_candidate = ["11", "69", "88", "96", "00"]
        old_mid_candidate = ["0", "1", "8"]
        if n == 1:
            return old_mid_candidate
        if n == 2:
            return even_mid_candidate[:-1]
        if n % 2:
            pre, mid_candidate = self.find_strobogrammatic(n - 1), old_mid_candidate
        else:
            pre, mid_candidate = self.find_strobogrammatic(n - 2), even_mid_candidate
        premid = (n - 1) // 2
        return [p[:premid] + c + p[premid:] for c in mid_candidate for p in pre]

# print(Solution().find_strobogrammatic(5))
# print(Solution().find_strobogrammatic(7))
print(Solution().find_strobogrammatic(10))

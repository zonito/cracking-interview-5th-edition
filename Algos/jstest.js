/***
 * you are on a biz trip and travelling from one city to another.
 * you have a stack of unsorted flight boarding passes. only departure city
 * and destination city are on the boarding pass. how do you find the first
 * departure city and your final destination city, write the solution in
 * javascript.
 ***/

(function () {
  'use strict';

  var boardingPasses = [
    ['C', 'D'],
    ['A', 'B'],
    ['B', 'C'],
    ['D', 'E']
  ];

  // Create hash with City and increase / decrease depending on depart of arrive.
  var cityHash = {};
  boardingPasses.forEach(function (pass) {
    if (cityHash.hasOwnProperty(pass[0])) {
      cityHash[pass[0]] += 1;
    } else {
      cityHash[pass[0]] = 1;
    }
    if (cityHash.hasOwnProperty(pass[1])) {
      cityHash[pass[1]] -= 1;
    } else {
      cityHash[pass[1]] = -1;
    }
  });

  // Check City counter - Departed City: Which is 1 and Arrived with is -1.
  var departedCity = '',
    destination = '';
  Object.keys(cityHash).forEach(function (city) {
    if (cityHash[city] === 1) {
      departedCity = city;
    } else if (cityHash[city] === -1) {
      destination = city;
    }
  });

  console.log(departedCity);
  console.log(destination);
}());

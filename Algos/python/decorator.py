# https://stackoverflow.com/a/309000/687692


def smart_div(func):
    def inner(*args, **kwargs):
        # if a < b:
        #     a, b = b, a
        return func(*args, **kwargs)

    return inner


@smart_div
def div(a, b):
    print(a / b)


# sdiv = smart_div(div)
# div(2, 4)

# def logged(func):
#     def with_logging(*args, **kwargs):
#         print(func.__name__ + " was called")
#         return func(*args, **kwargs)

#     return with_logging

# @logged
# def f(x):
#     """does some math"""
#     return x + x * x

# def f(x):
#     """does some math"""
#     return x + x * x

# f = logged(f)
# print(f.__name__)

import functools


def logged(func):
    print('abc')

    @functools.wraps(func)
    def with_logging(*args, **kwargs):
        print(func.__name__ + " was called")
        return func(*args, **kwargs)

    return with_logging


# @logged
# def f(x):
#     """does some math"""
#     return x + x * x

# print(f.__name__)  # prints 'f'
# print(f.__doc__)  # prints 'does some math'

"""
Merge Sort
"""


def merge_sort(lst):
    """Return sorted list."""
    lst_len = len(lst)
    if lst_len == 1:
        return lst
    mid = lst_len // 2
    left = merge_sort(lst[0:mid])
    right = merge_sort(lst[mid:])
    len_left = len(left)
    len_right = len(right)
    result = []
    left_counter = 0
    right_counter = 0
    for i in range(len_left + len_right):
        if right_counter < len_right and left_counter < len_left:
            if right[right_counter] >= left[left_counter]:
                result.append(left[left_counter])
                left_counter += 1
            elif left[left_counter] > right[right_counter]:
                result.append(right[right_counter])
                right_counter += 1
    for j in range(len_left - left_counter):
        result.append(left[left_counter + j])
    for i in range(len_right - right_counter):
        result.append(right[right_counter + i])
    return result


print (merge_sort(['a', 'c', 'b']))

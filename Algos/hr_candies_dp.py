"""
Alice is a kindergarden teacher. She wants to give some candies to the
children in her class.  All the children sit in a line ( their positions
are fixed), and each  of them has a rating score according to his or her
performance in the class.  Alice wants to give at least 1 candy to each child.
If two children sit next to each other, then the one with the higher
rating must get more candies. Alice wants to save money, so she needs to
minimize the total number of candies given to the children.

Input Format

The first line of the input is an integer N, the number of children in
Alice's class. Each of the following N lines contains an integer that
indicates the rating of each child.

1 <= N <= 105
1 <= ratingi <= 105

Output Format

Output a single line containing the minimum number of candies Alice must buy.

Sample Input

3
1
2
2
Sample Output

4
Explanation

Here 1, 2, 2 is the rating. Note that when two children have equal rating,
they are allowed to have different number of candies. Hence optimal
distribution will be 1, 2, 1.
"""


def get_total_negatives(children, index):
    """Return total number of candies should receive by higher grade."""
    counter = 0
    cur_child = children[index]
    if index + 1 in children:
        for child in children[index + 1:]:
            # print cur_child, child
            if child <= cur_child:
                counter += 1
                cur_child = child
            else:
                break
    return counter


def get_total():
    """Return total candies required"""
    children = [input() for _ in range(input())]
    candies = 0
    negs = 0
    prev = 0
    offset = -1
    for index, child in enumerate(children):
        # print 'c: ', child, prev, candy
        if child > prev:
            negs = get_total_negatives(children, index)
            print negs, ': negs'
            offset += 1
            candies += negs + 1 + offset
        else:
            negs -= 1
            candies += negs + 1
            offset = -1
        print child, 'return: ', negs + 1 + offset, candies
        prev = child
    return candies

# print get_total()


def get_total2():
    """new method."""
    children = [input() for _ in range(input())]
    candies = [1] * len(children)
    for index in range(1, len(children)):
        if children[index] > children[index - 1]:
            candies[index] = candies[index - 1] + 1
    for index in range(len(children) - 2, 0, -1):
        if children[index] > children[index + 1]:
            candies[index] = max(candies[index], candies[index + 1] + 1)
    print candies
    return sum(candies)

print get_total2()

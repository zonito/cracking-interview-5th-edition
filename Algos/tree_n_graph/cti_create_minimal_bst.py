"""
4.2 Minimal Tree: Given a sorted (increasing order) array with unique integer
elements, write an algorithm to create a binary search tree with minimal height
"""


class Tree(object):
    data = None
    left = None
    right = None

    def __init__(self, data=None):
        self.data = data

    def create_minimal_bst(self, arr):
        # print arr
        mid_index = len(arr) / 2
        node = Tree(arr[mid_index])
        if len(arr) == 3:
            node.left = Tree(arr[mid_index - 1])
            node.right = Tree(arr[mid_index + 1])
        elif len(arr) == 2:
            node.left = Tree(arr[mid_index - 1])
        else:
            node.left = self.create_minimal_bst(arr[:mid_index])
            node.right = self.create_minimal_bst(arr[mid_index:])
        return node

NODE = Tree().create_minimal_bst(range(10))
print NODE.data
print NODE.left.data, NODE.right.data
print NODE.left.left.data, NODE.left.right.data, NODE.right.left.data, NODE.right.right.data
print NODE.left.left.left.data, NODE.left.right.right.data, NODE.right.right.right.data

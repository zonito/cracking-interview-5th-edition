import abc
import priority_dict as pdict
import numpy as np
from queue import Queue


class Graph(abc.ABC):

    def __init__(self, num_vertices, directed=False):
        self.num_vertices = num_vertices
        self.directed = directed

    @abc.abstractmethod
    def add_edge(self, ver1, ver2, weight):
        pass

    @abc.abstractmethod
    def get_adjacency_vertices(self, vertex):
        pass

    @abc.abstractmethod
    def get_indegree(self, vertex):
        pass

    @abc.abstractmethod
    def get_edge_weight(self, ver1, ver2):
        pass

    @abc.abstractmethod
    def display(self):
        pass


# Approach 1: to store Graph using Adjacency matrix
class AdjacencyMatrixGraph(Graph):

    def __init__(self, num_vertices, directed=False):
        super(AdjacencyMatrixGraph, self).__init__(num_vertices, directed)
        self.matrix = np.zeros((num_vertices, num_vertices))

    def add_edge(self, ver1, ver2, weight=1):
        if ver1 >= self.num_vertices or ver2 >= self.num_vertices or ver1 < 0 or ver2 < 0:
            raise ValueError(
                'Vertices %d and %d are out of bounds' % (ver1, ver2))
        if weight < 1:
            raise ValueError('An edge cannot have weight < 1')
        self.matrix[ver1][ver2] = weight
        if not self.directed:
            self.matrix[ver2][ver1] = weight

    def get_adjacency_vertices(self, vertex):
        if vertex < 0 or vertex >= self.num_vertices:
            raise ValueError('Cannot access vertex %d' % vertex)
        adjacent_vertices = []
        for i in range(self.num_vertices):
            if self.matrix[vertex][i] > 0:
                adjacent_vertices.append(i)
        return adjacent_vertices

    def get_indegree(self, vertex):
        if vertex < 0 or vertex >= self.num_vertices:
            raise ValueError('Cannot access vertex %d' % vertex)
        indegree = 0
        for i in range(self.num_vertices):
            if self.matrix[i][vertex] > 0:
                indegree = indegree + 1
        return indegree

    def get_edge_weight(self, ver1, ver2):
        return self.matrix[ver1][ver2]

    def display(self):
        for i in range(self.num_vertices):
            for vertex in self.get_adjacency_vertices(i):
                print(i, '-->', vertex)


def run(directed=False):
    graph = AdjacencyMatrixGraph(4, directed=directed)
    graph.add_edge(0, 1)
    graph.add_edge(0, 2)
    graph.add_edge(2, 3)

    for i in range(4):
        print('Adjacent to: ', i, graph.get_adjacency_vertices(i))
        # Output        Undirected      Directed
        # Adjacent to:  0 [1, 2]        0 [1, 2]
        # Adjacent to:  1 [0]           1 []
        # Adjacent to:  2 [0, 3]        2 [3]
        # Adjacent to:  3 [2]           3 []

    for i in range(4):
        print('Indegree: ', i, graph.get_indegree(i))
        # Output     Undirected         Directed
        # Indegree:  0 2                0 0
        # Indegree:  1 1                1 1
        # Indegree:  2 2                2 1
        # Indegree:  3 1                3 1

    for i in range(4):
        for j in graph.get_adjacency_vertices(i):
            print('Edge weight: ', i, ' ', j, ' weight: ',
                  graph.get_edge_weight(i, j))
            # Output
            # Edge weight:  0   1  weight:  1.0
            # Edge weight:  0   2  weight:  1.0
            # Edge weight:  1   0  weight:  1.0
            # Edge weight:  2   0  weight:  1.0
            # Edge weight:  2   3  weight:  1.0
            # Edge weight:  3   2  weight:  1.0

            # Directed Output
            # Edge weight:  0   1  weight:  1.0
            # Edge weight:  0   2  weight:  1.0
            # Edge weight:  2   3  weight:  1.0

    graph.display()
    # Output
    # 0 --> 1
    # 0 --> 2
    # 1 --> 0
    # 2 --> 0
    # 2 --> 3
    # 3 --> 2

    # Directed Output
    # 0 --> 1
    # 0 --> 2
    # 2 --> 3


# run()
# run(True)


# Approach 2: Using adjacency sets
class Node(object):

    def __init__(self, vertex_id):
        self.vertex_id = vertex_id
        self.adjacency_set = set()

    def add_edge(self, vertex):
        if self.vertex_id == vertex:
            raise ValueError(
                'The vertex %d cannot be adjacent to itself' % vertex)
        self.adjacency_set.add(vertex)

    def get_adjacent_vertices(self):
        return sorted(self.adjacency_set)


class AdjacencySetGraph(Graph):

    def __init__(self, num_vertices, directed=False):
        super(AdjacencySetGraph, self).__init__(num_vertices, directed)
        self.vertex_list = []
        for i in range(num_vertices):
            self.vertex_list.append(Node(i))

    def add_edge(self, ver1, ver2, weight=1):
        if ver1 >= self.num_vertices or ver2 >= self.num_vertices or ver1 < 0 or ver2 < 0:
            raise ValueError(
                'Vertices %d and %d are out of bounds' % (ver1, ver2))
        if weight != 1:
            raise ValueError(
                'An adjacency set cannot represent edge weights > 1')

        self.vertex_list[ver1].add_edge(ver2)
        if not self.directed:
            self.vertex_list[ver2].add_edge(ver1)

    def get_adjacency_vertices(self, vertex):
        if vertex < 0 or vertex >= self.num_vertices:
            raise ValueError('Cannot access vertex %d' % vertex)
        return self.vertex_list[vertex].get_adjacent_vertices()

    def get_indegree(self, vertex):
        if vertex < 0 or vertex >= self.num_vertices:
            raise ValueError('Cannot access vertex %d' % vertex)
        indegree = 0
        for i in range(self.num_vertices):
            if vertex in self.get_adjacency_vertices(i):
                indegree += 1
        return indegree

    def get_edge_weight(self, ver1, ver2):
        return 1

    def display(self):
        for i in range(self.num_vertices):
            for vertex in self.get_adjacency_vertices(i):
                print(i, '-->', vertex)


def runset(directed=False):
    graph = AdjacencySetGraph(4, directed=directed)
    graph.add_edge(0, 1)
    graph.add_edge(0, 2)
    graph.add_edge(2, 3)

    for i in range(4):
        print('Adjacent to: ', i, graph.get_adjacency_vertices(i))

    for i in range(4):
        print('Indegree: ', i, graph.get_indegree(i))

    for i in range(4):
        for j in graph.get_adjacency_vertices(i):
            print('Edge weight: ', i, ' ', j, ' weight: ',
                  graph.get_edge_weight(i, j))

    graph.display()
    # Same output like Adjacency Matrix


# runset()
# runset(True)


def breadth_first(graph, start=0):
    queue = Queue()
    queue.put(start)
    visited = np.zeros(graph.num_vertices)
    while not queue.empty():
        vertex = queue.get()
        if visited[vertex] == 1:
            continue

        print('Visit: ', vertex)
        visited[vertex] = 1
        for vertice in graph.get_adjacency_vertices(vertex):
            if visited[vertice] != 1:
                queue.put(vertice)


def depth_first(graph, visited, current=0):
    if visited[current] == 1:
        return
    visited[current] = 1
    print('Visit: ', current)
    for vertex in graph.get_adjacency_vertices(current):
        depth_first(graph, visited, vertex)


def topological_sort(graph):
    queue = Queue()
    indegree_map = {}
    for i in range(graph.num_vertices):
        indegree_map[i] = graph.get_indegree(i)
        # queue all nodes which have no dependencies i.e.,
        # no edges coming in
        if indegree_map[i] == 0:
            queue.put(i)
    sorted_list = []
    while not queue.empty():
        vertex = queue.get()
        sorted_list.append(vertex)
        for vertice in graph.get_adjacency_vertices(vertex):
            indegree_map[vertice] = indegree_map[vertice] - 1
            if indegree_map[vertice] == 0:
                queue.put(vertice)
    if len(sorted_list) != graph.num_vertices:
        raise ValueError('This graph has a cycle')
    print('Sorted List: ', sorted_list)


def run_search(directed=False):
    graph = AdjacencyMatrixGraph(9, directed=directed)
    graph.add_edge(0, 1)
    graph.add_edge(1, 2)
    graph.add_edge(2, 7)
    graph.add_edge(2, 4)
    graph.add_edge(2, 3)
    graph.add_edge(1, 5)
    graph.add_edge(5, 6)
    graph.add_edge(6, 3)
    graph.add_edge(3, 4)
    graph.add_edge(6, 8)
    # Uncomment below line to add cycle in directed graphs
    # graph.add_edge(2, 0)
    # breadth_first(graph, 2)
    # depth_first(graph, np.zeros(graph.num_vertices), 2)
    topological_sort(graph)
    print('-' * 50)


# run_search(True)
# run_search()


def build_distance_table(graph, source):
    # A dictionary mapping from the vertex number to a tuple of
    # (distance from source, last vertex on path from source)
    distance_table = {}
    for i in range(graph.num_vertices):
        distance_table[i] = (None, None)

    # The distance to the source from itself is 0
    distance_table[source] = (0, source)

    queue = Queue()
    queue.put(source)
    while not queue.empty():
        current_vertex = queue.get()
        # The distance of the current vertex from the source
        current_distance = distance_table[current_vertex][0]
        for neighbor in graph.get_adjacency_vertices(current_vertex):
            # Only update the distance table if no current distance from
            # the source is set.
            if distance_table[neighbor][0] is None:
                distance_table[neighbor] = (1 + current_distance,
                                            current_vertex)

                # Enqueue the neighbor only if it has other adjacent vertices
                # to explore
                if graph.get_adjacency_vertices(neighbor):
                    queue.put(neighbor)
    return distance_table


def shortest_path(graph, source, destination):
    distance_table = build_distance_table(graph, source)
    path = [destination]
    previous_vertex = distance_table[destination][1]
    while previous_vertex is not None and previous_vertex is not source:
        path = [previous_vertex] + path
        previous_vertex = distance_table[previous_vertex][1]
    if previous_vertex is None:
        print('There is no path from %d to %d' % (source, destination))
    else:
        path = [source] + path
        print('Shortest path is: ', path)


def run_shortest_path(directed=False):
    graph = AdjacencySetGraph(8, directed=directed)
    graph.add_edge(0, 1)
    graph.add_edge(1, 2)
    graph.add_edge(1, 3)
    graph.add_edge(2, 3)
    graph.add_edge(1, 4)
    graph.add_edge(3, 5)
    graph.add_edge(5, 4)
    graph.add_edge(3, 6)
    graph.add_edge(6, 7)
    graph.add_edge(0, 7)
    shortest_path(graph, 0, 5)
    shortest_path(graph, 0, 6)
    shortest_path(graph, 7, 4)
    print('-' * 50)


# run_shortest_path()
# run_shortest_path(True)


def build_distance_table_dijsktra(graph, source):
    # A dictionary mapping from the vertex number to a tuple of
    # (distance from source, last vertex on path from source)
    distance_table = {}

    for i in range(graph.num_vertices):
        distance_table[i] = (None, None)

    # the distance to the source from itself is 0
    distance_table[source] = (0, source)

    # Holds mapping of vertex id to distance from source
    # access the highest priority (lowest distance) item
    # first
    priority_queue = pdict.PriorityDict()
    priority_queue[source] = 0
    while priority_queue.keys():
        current_vertex = priority_queue.pop_smallest()
        # the distance of the current node from the source
        current_distance = distance_table[current_vertex][0]
        for neighbor in graph.get_adjacency_vertices(current_vertex):
            # The distance to the neighbor is only the weight of the edge
            # connecting the neighbor
            distance = current_distance + graph.get_edge_weight(
                current_vertex, neighbor)
            # The last recorded distance of this neighbor from the source
            neighbor_distance = distance_table[neighbor][0]

            # If there is a currently recorded distance from the source and
            # this is greater than the distance of the new path found, update
            # the current distance from the source in the distance table.
            if neighbor_distance is None or neighbor_distance > distance:
                distance_table[neighbor] = (distance, current_vertex)
                priority_queue[neighbor] = distance
    return distance_table


def shortest_path_dijsktra(graph, source, destination):
    distance_table = build_distance_table_dijsktra(graph, source)
    path = [destination]
    previous_vertex = distance_table[destination][1]
    while previous_vertex is not None and previous_vertex is not source:
        path = [previous_vertex] + path
        previous_vertex = distance_table[previous_vertex][1]
    if previous_vertex is None:
        print('There is no path from %d to %d' % (source, destination))
    else:
        path = [source] + path
        print('Shortest path is: ', path)


def run_shortest_path_dijsktra(directed=False):
    graph = AdjacencyMatrixGraph(8, directed=directed)
    graph.add_edge(0, 1, 1)
    graph.add_edge(1, 2, 2)
    graph.add_edge(1, 3, 6)
    graph.add_edge(2, 3, 2)
    graph.add_edge(1, 4, 3)
    graph.add_edge(3, 5, 1)
    graph.add_edge(5, 4, 5)
    graph.add_edge(3, 6, 1)
    graph.add_edge(6, 7, 1)
    graph.add_edge(0, 7, 8)
    shortest_path_dijsktra(graph, 0, 6)
    shortest_path_dijsktra(graph, 4, 7)
    shortest_path_dijsktra(graph, 7, 0)
    print('-' * 50)


# run_shortest_path_dijsktra()
# run_shortest_path_dijsktra(True)


def prim_spanning_tree(graph, source):
    # A dictionary mapping from the vertex number to a tuple of
    # (distance from source, last vertex on path from source)
    distance_table = {}

    for i in range(graph.num_vertices):
        distance_table[i] = (None, None)

    # the distance to the source from itself is 0
    distance_table[source] = (0, source)

    # Holds mapping of vertex id to distance from source
    # access the highest priority (lowest distance) item
    # first
    priority_queue = pdict.PriorityDict()
    priority_queue[source] = 0

    visited_vertices = set()

    # Set of edges where each edge is represented as a string
    # "1->2": is an edge between vertices 1 and 2.
    spanning_tree = set()
    while priority_queue.keys():
        current_vertex = priority_queue.pop_smallest()

        # If we've visited a vertex earlier then we have all
        # outbound edges from it, we do not process it again
        if current_vertex in visited_vertices:
            continue

        visited_vertices.add(current_vertex)

        # If the current vertex is the source, we haven't traversed an
        # edge yet, no edge to add to our spanning tree
        if current_vertex != source:
            # the current vertex is connected by the lowest weighted edge
            last_vertex = distance_table[current_vertex][1]
            edge = str(last_vertex) + '-->' + str(current_vertex)
            if edge not in spanning_tree:
                spanning_tree.add(edge)
        for neighbor in graph.get_adjacency_vertices(current_vertex):
            # The distance to the neighbor is only the weight of the edge
            # connecting the neighbor
            distance = graph.get_edge_weight(current_vertex, neighbor)

            # The last recorded distance of this neighbor from the source
            neighbor_distance = distance_table[neighbor][0]

            # If this neighbor has been seen for the first time or the new edge
            # connecting this neighbor is of a lower weight than the last
            if neighbor_distance is None or neighbor_distance > distance:
                distance_table[neighbor] = (distance, current_vertex)
                priority_queue[neighbor] = distance

    for edge in spanning_tree:
        print(edge)


def run_prim_spanning(directed=False):
    graph = AdjacencyMatrixGraph(8, directed=directed)
    graph.add_edge(0, 1, 1)
    graph.add_edge(1, 2, 2)
    graph.add_edge(1, 3, 2)
    graph.add_edge(2, 3, 2)
    graph.add_edge(1, 4, 3)
    graph.add_edge(3, 5, 1)
    graph.add_edge(5, 4, 3)
    graph.add_edge(3, 6, 1)
    graph.add_edge(6, 7, 1)
    graph.add_edge(0, 7, 1)
    prim_spanning_tree(graph, 1)
    print('-' * 50)
    prim_spanning_tree(graph, 3)
    print('-' * 50)


# run_prim_spanning()


def kruskal_spanning_tree(graph):
    # Holds mapping of vertex id to distance from source
    # access the highest priority (lowest distance) item
    # first
    priority_queue = pdict.PriorityDict()
    for vertex in range(graph.num_vertices):
        for neighbor in graph.get_adjacency_vertices(vertex):
            priority_queue[(vertex, neighbor)] = graph.get_edge_weight(
                vertex, neighbor)

    visited_vertices = set()

    # Maps a node to all its adjacent nodes which are in the
    # minimum spanning tree
    spanning_tree = {}

    for vertex in range(graph.num_vertices):
        spanning_tree[vertex] = set()

    # Number of edge we have got so far
    num_edges = 0
    while priority_queue.keys() and num_edges < graph.num_vertices - 1:
        ver1, ver2 = priority_queue.pop_smallest()
        if ver1 in spanning_tree[ver2]:
            continue

        # Arrange the spanning tree so the node with the smaller
        # vertex id is always first. This greatly simplifies the
        # code to find cycles in this tree.
        vertex_pair = sorted([ver1, ver2])

        spanning_tree[vertex_pair[0]].add(vertex_pair[1])

        # check if adding the current edge cause a cycle
        if has_cycle(spanning_tree):
            spanning_tree[vertex_pair[0]].remove(vertex_pair[1])
            continue

        num_edges = num_edges + 1
        visited_vertices.add(ver1)
        visited_vertices.add(ver2)

    print('Visited vertices : ', visited_vertices)
    if len(visited_vertices) != graph.num_vertices:
        print('Minimum spanning tree not found')
    else:
        print('Minimum spanning tree:')
        for key in spanning_tree:
            for value in spanning_tree[key]:
                print(key, '-->', value)


def has_cycle(spanning_tree):
    for source in spanning_tree:
        queue = []
        queue.append(source)

        visited_vertices = set()
        while queue:
            vertex = queue.pop(0)

            # If we've see the vertex before in this spanning tree
            # there is a cycle
            if vertex in visited_vertices:
                return True
            visited_vertices.add(vertex)

            # Add all vertices connected by edges in this spanning tree
            queue.extend(spanning_tree[vertex])
    return False


def run_kruskal_spanning(directed=False):
    graph = AdjacencyMatrixGraph(8, directed=directed)
    graph.add_edge(0, 1, 1)
    graph.add_edge(1, 2, 2)
    graph.add_edge(1, 3, 2)
    graph.add_edge(2, 3, 2)
    graph.add_edge(1, 4, 3)
    graph.add_edge(3, 5, 1)
    graph.add_edge(5, 4, 2)
    graph.add_edge(3, 6, 1)
    graph.add_edge(6, 7, 1)
    graph.add_edge(7, 0, 1)
    kruskal_spanning_tree(graph)
    print('-' * 50)


run_kruskal_spanning()

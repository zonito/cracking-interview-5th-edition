
# pylint: disable=C0111


class Node(object):
    left = None
    right = None
    data = None
    count = 0

    def __init__(self, data):
        self.data = data


class BinaryTree(object):

    root = None

    def insert(self, data):
        node = Node(data)
        if not self.root:
            self.root = node
            return
        root = self.root
        while root:
            if node.data == root.data:
                root.count += 1
                break

            parent = root
            if root.data > node.data:
                root = root.left
                if not root:
                    parent.left = node
                    break
            else:
                root = root.right
                if not root:
                    parent.right = node
                    break

    def remove(self, data):
        self.root = self._remove_node(self.root, data)

    def _remove_node(self, node, data):
        if not node:
            return
        if node.data == data:
            # Leaf
            if not node.left and not node.right:
                return None

            # has right node
            if not node.left:
                return node.right
            elif not node.right:
                return node.left

            # have 2 children
            temp_node = self.get_min(node.right)
            node.data = temp_node.data
            node.right = self._remove_node(node.right, temp_node.data)

        elif data < node.data:
            node.left = self._remove_node(node.left, data)
            return node
        else:
            node.right = self._remove_node(node.right, data)

    def get_min(self, node=None):
        if not node:
            node = self.root
        while node.left:
            node = node.left
        return node.data

    def get_max(self, node=None):
        if not node:
            node = self.root
        while node.right:
            node = node.right
        return node.data

    def find(self, data):
        node = self.root
        while node and node.data != data:
            if node.data > data:
                node = node.left
            elif node.data < data:
                node = node.right
        return node

    def in_order(self, node):
        if node:
            self.in_order(node.left)
            print(node.data)
            self.in_order(node.right)

    def pre_order(self, node):
        if node:
            print(node.data)
            self.pre_order(node.left)
            self.pre_order(node.right)

    def post_order(self, node):
        if node:
            self.post_order(node.left)
            self.post_order(node.right)
            print(node.data)

    def _right_view_util(self, root, level, max_level):
        # Base Case
        if root is None:
            return

        # If this is the last node of its level
        if max_level[0] < level:
            print(root.data)
            max_level[0] = level

        # Recur for right subtree first, then left subtree
        self._right_view_util(root.right, level + 1, max_level)
        self._right_view_util(root.left, level + 1, max_level)

    def right_view(self, root):
        max_level = [0]
        self._right_view_util(root, 1, max_level)

BTREE = BinaryTree()
BTREE.insert(10)
BTREE.insert(2)
BTREE.insert(5)
BTREE.insert(3)
BTREE.remove(10)

print('in order')
BTREE.in_order(BTREE.root)
print('pre order')
BTREE.pre_order(BTREE.root)
print('post order')
BTREE.post_order(BTREE.root)

print('Min:', BTREE.get_min())
print('Max:', BTREE.get_max())

BTREE.right_view(BTREE.root)

from typing import List


class Solution:

    def search(self, sorted_items: List[int], size: int, target: int) -> int:
        low = 0
        high = size
        while low < high:
            mid = (low + high) // 2
            if sorted_items[mid] == target:
                return mid
            if sorted_items[mid] > target:
                high = mid
            elif sorted_items[mid] < target:
                low = mid + 1
        return None


items = [3, 4, 7, 9, 10, 12, 15, 17]
for item in items:
    print(Solution().search(items, 8, item))

"""
Given a directed graph, design an algorithm to find out whether there is a
route between two nodes.
"""

import tree as tr


def find_route(first_node, second_node):
    """Find route between given 2 nodes."""
    if first_node == second_node:
        return True
    if not first_node or not second_node:
        return False
    # print first_node.data, second_node.data
    left_node = find_route(first_node.left, second_node)
    if left_node:
        return True
    right_node = find_route(first_node.right, second_node)
    if right_node:
        return True
    return False


def is_route_exists(first_node, second_node):
    """Return True, if route exists between 2 nodes, otherwise False."""
    if not find_route(first_node, second_node):
        print 'Reverse'
        return find_route(second_node, first_node)
    return True


# Another method using BFS (Queue)
class LinkedList(object):
    """Node representation for linked list."""
    next_node = None
    data = None

    def __init__(self, data):
        self.data = data

    def enqueue(self, data):
        """Append data to the end of linked list."""
        end = LinkedList(data)
        current = self
        while current.next_node:
            current = current.next_node
        current.next_node = end

    @staticmethod
    def remove_first(head_node):
        """Remove first."""
        return head_node.data, head_node.next_node

    @staticmethod
    def delete_node(head_node, data):
        """Delete a node and return from singly linked list."""
        if head_node.data == data:
            return head_node.next

        # find and delete it.
        temp = head_node
        while temp:
            if temp.next_node.data == data:
                temp.next_node = temp.next_node.next_node
                return head_node
            temp = temp.next_node

        # Not found
        return head_node


def find_route_bfs(first_node, second_node):
    """Find route between 2 nodes using BFS method"""
    if first_node == second_node:
        return True
    if not first_node or not second_node:
        return False
    first_node.state = 'visiting'
    queue = LinkedList(first_node)
    while queue:
        node, queue = queue.remove_first(queue)
        if node and node.state == 'visiting':
            adjacents = node.get_adjacent()
            for adj in adjacents:
                if adj and not adj.state:
                    if adj == second_node:
                        return True
                    adj.state = 'visiting'
                    if queue:
                        queue.enqueue(adj)
                    else:
                        queue = LinkedList(adj)
            node.state = 'visited'
    return False


GRAPH = tr.get_graph()
print is_route_exists(GRAPH.right, GRAPH.left)
print find_route_bfs(GRAPH.left, GRAPH.right)

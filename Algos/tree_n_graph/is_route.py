
# pylint: disable=C0111


class LinkedNode(object):
    next_node = None
    data = None
    previous_node = None


class LinkedList(object):
    node = None
    head_node = None

    def insert(self, obj):
        print obj
        if not self.node:
            self.node = LinkedNode()
            self.head_node = self.node
        self.node.data = obj
        self.node.next_node = LinkedNode()
        temp = self.node
        self.node = self.node.next_node
        self.node.previous_node = temp

    def remove_first(self):
        data = self.head_node.data
        self.head_node.data = None
        self.head_node = self.head_node.next_node
        self.head_node.previous_node = None
        return data

    def is_empty(self):
        return self.head_node.data is None

# LL = LinkedList()
# LL.insert(1)
# LL.insert(2)
# LL.insert(3)
# LL.insert(4)
# print LL.remove_first()
# print LL.remove_first()


class Graph(object):

    vertices = {}
    visited = {}

    def __init__(self):
        self.visited = {}

    def add_vertext(self, from_node, to_node):
        if not self.vertices.get(from_node):
            self.vertices[from_node] = []
        self.vertices[from_node].append(to_node)

    def dfs(self, root_node, node):
        for vertex in self.vertices.get(root_node, []):
            if self.visited.get(vertex):
                continue
            self.visited[root_node] = True
            if vertex == node:
                return True
            is_route = self.dfs(vertex, node)
            if is_route:
                return True
        return False

    def bfs(self, root_node, node):
        if root_node == node:
            return True
        queue = LinkedList()
        queue.insert(root_node)
        while not queue.is_empty():
            vertex = queue.remove_first()  # Dequeue
            for child_node in self.vertices.get(vertex, []):
                if self.visited.get(child_node):
                    continue
                self.visited[child_node] = True
                if node == child_node:
                    return True
                queue.insert(child_node)
        return False


GRAPH = Graph()
GRAPH.add_vertext('A', 'B')
GRAPH.add_vertext('D', 'C')
GRAPH.add_vertext('C', 'D')
GRAPH.add_vertext('D', 'F')
GRAPH.add_vertext('E', 'A')
GRAPH.add_vertext('B', 'D')
GRAPH.add_vertext('F', 'E')
GRAPH.add_vertext('C', 'X')
GRAPH.add_vertext('D', 'E')
# print GRAPH.dfs('B', 'E')
print GRAPH.bfs('B', 'E')

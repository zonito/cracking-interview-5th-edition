"""
Implement a function to check if a binary tree is balanced. For the purposes of
this question, a balanced tree is defined to be a tree such that the heights of
the two subtrees of any node never differ by more than one.
"""

import tree as tr


def get_height(tree):
    """Return height of given tree"""
    if not tree:
        return -1
    left_heigth = get_height(tree.left)
    if left_heigth == -2:
        return -2
    right_heigth = get_height(tree.right)
    if right_heigth == -2:
        return -2
    if abs(right_heigth - left_heigth) > 1:
        return -2
    return max(left_heigth, right_heigth) + 1


def is_balanced(tree):
    """Return True, if given tree is balanced or not."""
    return get_height(tree) != -2

print is_balanced(tr.create_unbalanced_tree())
print is_balanced(tr.create_balanced_tree())

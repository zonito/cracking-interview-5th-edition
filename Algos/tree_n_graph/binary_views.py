
import tree


class Views(object):
    result = None

    def __init__(self):
        self.result = []

    def left_view(self, btree, level=0):
        if not btree:
            return None
        self.left_view(btree.left, level + 1)
        self.left_view(btree.right, level + 1)
        if len(self.result) <= level:
            self.result = [None for _ in range(level + 1)]
        self.result[level] = btree.data

    def right_view(self, btree, level=0):
        if not btree:
            return None
        self.right_view(btree.right, level + 1)
        self.right_view(btree.left, level + 1)
        if len(self.result) <= level:
            self.result = [None for _ in range(level + 1)]
        self.result[level] = btree.data

    def find_width(self, btree, width=0):
        if not btree:
            return 0
        lwidth = self.find_width(btree.left, width)
        rwidth = self.find_width(btree.right, width)
        return lwidth + 1 + rwidth

    def bottom_view(self, btree=None, level=0, overwrite=False):
        if not btree:
            return None
        self.bottom_view(btree.right, level + 1)
        self.bottom_view(btree.left, level + 1, True)
        if len(self.result) <= level:
            self.result = [None for _ in range(level + 1)]
        if (self.result[level] and overwrite) or not self.result[level]:
            self.result[level] = btree.data


BTREE = tree.create_unbalanced_tree()
VIEW = Views()
VIEW.left_view(BTREE)
print VIEW.result
VIEW = Views()
VIEW.right_view(BTREE)
print VIEW.result
print VIEW.find_width(BTREE)

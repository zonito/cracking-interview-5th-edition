"""https://youtu.be/uZj0hetLFHU / https://www.geeksforgeeks.org/binary-heap/"""

from typing import List


def heapify(arr: List[int]):  # O(N)
    total_arr = len(arr)  # O(N)
    for child_index in range(total_arr - 1, 0, -1):  # O(N)
        parent_index = int((child_index - 1) / 2)  # O(1)
        if arr[child_index] > arr[parent_index]:  # O(1)
            arr[parent_index], arr[child_index] = arr[child_index], arr[
                parent_index]  # O(1)
        print(arr)
    return arr


def normalize_heap(arr: List[int], total_arr: int, parent_i: int = 0):
    left_i = parent_i * 2 + 1
    if total_arr <= left_i:
        return
    right_i = parent_i * 2 + 2
    if total_arr > right_i:
        if arr[left_i] > arr[right_i] and arr[left_i] > arr[parent_i]:
            arr[parent_i], arr[left_i] = arr[left_i], arr[parent_i]
            normalize_heap(arr, total_arr, left_i)
        elif arr[parent_i] < arr[right_i]:
            arr[parent_i], arr[right_i] = arr[right_i], arr[parent_i]
            normalize_heap(arr, total_arr, right_i)
    elif arr[parent_i] < arr[left_i]:
        arr[parent_i], arr[left_i] = arr[left_i], arr[parent_i]
        normalize_heap(arr, total_arr, left_i)


def heap_sort(arr: List[int]):
    total_arr = len(arr)  # O(N)
    for last_index in range(total_arr - 1, 0, -1):  # O(N)
        arr[0], arr[last_index] = arr[last_index], arr[0]  # O(1)
        normalize_heap(arr, last_index)  # O(log N)
    return arr


print(heapify([10, 15, 5, 7, 9, 12]))
print(heap_sort(heapify([10, 15, 5, 7, 9, 12]))[::-1])

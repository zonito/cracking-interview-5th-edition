"""Tree Implementation Class."""


class Node(object):
    """The Node"""
    data = None
    left = None
    right = None
    state = None

    def __init__(self, data):
        """Constructor to set value of data."""
        self.data = data

    def get_adjacent(self):
        """Return tuple of left and right node."""
        return (self.left, self.right)


def create_unbalanced_tree():
    """Return unbalanced tree
                    A
            B               C
        D       E               F
            G       H       I
                J
    """
    root = Node('A')
    root.left = Node('B')
    root.right = Node('C')
    root.left.left = Node('D')
    root.left.right = Node('E')
    root.right.right = Node('F')
    root.left.left.right = Node('G')
    root.left.right.right = Node('H')
    root.right.right.left = Node('I')
    root.left.left.right.right = Node('J')
    return root


def create_balanced_tree():
    """Return balanced tree"""
    root = Node('A')
    root.left = Node('B')
    root.right = Node('C')
    root.left.left = Node('D')
    return root


def get_graph():
    """
    Return graph data structure.
            A
          /  |
        B  C
       /  | /
     D  E
    """
    node = Node('A')
    node.left = Node('B')
    node.right = Node('C')
    node.left.left = Node('D')
    node.left.right = Node('E')
    node.left.right.left = Node('F')
    node.left.right.left.left = node.right
    return node

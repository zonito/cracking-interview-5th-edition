
from collections import defaultdict


class TrieNode:

    def __init__(self):
        self.nodes = defaultdict(TrieNode)
        self.is_end = False


class Trie:

    def __init__(self):
        self.root = TrieNode()

    def insert(self, word: str) -> None:
        """
        Inserts a word into the trie.
        """
        curr = self.root
        for char in word:
            curr = curr.nodes[char]
        curr.is_end = True

    def _get_node(self, word: str) -> TrieNode:
        """
        Inserts a word into the trie.
        """
        curr = self.root
        for char in word:
            if char not in curr.nodes:
                return None
            curr = curr.nodes[char]
        return curr

    def search(self, word: str) -> bool:
        """
        Returns if the word is in the trie.
        """
        obj = self._get_node(word)
        if not obj:
            return False
        return obj.is_end

    def starts_with(self, prefix: str) -> bool:
        """
        Returns if there is any word in the trie that starts with the given prefix.
        """
        return bool(self._get_node(prefix))


# Your Trie object will be instantiated and called as such:
tobj = Trie()
tobj.insert('love')
print(tobj.search('love'))
print(tobj.starts_with('lov'))
print(tobj.starts_with('kumar'))

# database.py tests

import package
import unittest
import database_tests


class MockResponse:
    data = b'Package: _test_package\nVersion: 1.0'
    status = 200

    def __init__(self):
        pass

    def read(self):
        zip_data = ''
        with open('_fake_test_package.tar.gz', 'rb') as file_obj:
            zip_data = file_obj.read()
        return zip_data


class MockHttp:

    def __init__(self):
        pass

    def request(self, *args, **kwargs):
        return MockResponse()


class TestPackage(unittest.TestCase):

    def setUp(self):
        package.database.pymysql.connect = database_tests.MockConnection
        package.urllib3.PoolManager = MockHttp

    def test_get_packages(self):
        packages = package.Package().get_packages(1)
        self.assertEqual(len(packages), 1)

if __name__ == '__main__':
    unittest.main()

# Manage Open Source Packages

## Instruction to run

1. This assignment is done using `python3`. Please setup your `virtualenv` and run `pip install -r requirements.txt`.
2. Run test first `python3 database_tests.py` and `python3 package_tests.py`.
3. Run `python3 package.py` - This should start reading packages from [R-PACKAGES](https://cran.r-project.org/src/contrib/PACKAGES) and update database.

## Explanation

- There are 2 main files
    * `package.py` - Read Package, Extract Package information from `.tar.gz` files.
    * `database.py` - Connect to Free MySQL, Save Package information provided by `package.py`
- There are 2 test files
    * `package_tests.py` - Test `get_packages` public method.
    * `database_tests.py` - Test `get_package_info`, `save_package`, `add_authors` and `add_maintainers` public methods.
- There is one fake `_fake_test_package.tar.gz` file for local unittest.
- Added `database_tables.sql` schema file to setup database locally or any other server. Currently, it is hosted to free mysql https://remotemysql.com/.
- Provided list of python libraries used in this assignment in `requirements.txt`

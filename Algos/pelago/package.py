
import urllib3
import tarfile
import os
import database
import shutil
import logging
import debian.deb822

_LOGGER = logging.getLogger()
_LOGGER.setLevel(logging.DEBUG)


class Package:

    PACKAGE_URL = 'https://cran.r-project.org/src/contrib/PACKAGES'
    TAR_PACKAGE = 'https://cran.r-project.org/src/contrib/'

    def __init__(self):
        self.http = urllib3.PoolManager()

    @staticmethod
    def _parse_pkg(package):
        if len(package) < 10:
            return {}
        package_detail = {}
        pkg_generate = debian.deb822.Deb822.iter_paragraphs(package)
        package_detail = list(pkg_generate)[0]
        return package_detail

    def _extract_package(self, package):
        filename = '%s_%s.tar.gz' % (package['Package'], package['Version'])
        response = self.http.request(
            'GET', self.TAR_PACKAGE + filename, preload_content=False)
        if response.status != 200:
            return False
        with open(filename, 'wb') as file_obj:
            file_obj.write(response.read())
        tar = tarfile.open(filename)
        tar.extractall()
        tar.close()
        os.remove(filename)
        return True

    def _save_package(self, package):
        if not self._extract_package(package):
            return False
        desc_filename = package['Package'] + '/DESCRIPTION'
        if not os.path.isfile(desc_filename):
            logging.error('No DESCRIPTION file found in package %s',
                          package['Package'])
            return False

        # Open DESCRIPTION and Save required information in database
        with open(desc_filename, 'r') as file_obj:
            package_detail = self._parse_pkg(file_obj.read().encode('utf-8'))
            package_id = database.Database().save_package(package_detail)
            if not package_id:
                package_id = database.Database().get_package_info(
                    package_detail).get('id', 0)

            # Still fail to generate information from Database
            if not package_id:
                return False

            # Save Maintainer and Authors
            if package_detail.get('Maintainer'):
                database.Database().add_maintainers(
                    package_detail['Maintainer'], package_id)
            if package_detail.get('Author'):
                database.Database().add_authors(
                    package_detail['Author'], package_id)

        shutil.rmtree(package['Package'])
        return True

    def get_packages(self, limit=50):
        response = self.http.request('GET', self.PACKAGE_URL)
        logging.info('Status: %s', response.status)
        packages = []
        for section in response.data.decode('utf-8').split('\n\n')[:limit]:
            package = self._parse_pkg(section)
            if not package:
                logging.error('Error: Fail to parse package!')
                continue
            package_name = '%s_%s' % (package['Package'], package['Version'])
            if not self._save_package(package):
                logging.error('Error: Failed to save %s package!',
                              package_name)
                continue
            logging.info('Package %s is saved!', package_name)
            packages.append(package_name)
        return packages

if __name__ == '__main__':
    PACKAGE = Package()
    print(PACKAGE.get_packages())

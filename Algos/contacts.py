"""
We are going to make our own contacts application. You are given the number of
operations to perform, N. In any contacts application, two basic operations are
add and find. The input will be one of the following:

add name
find partial
For the find operation, you will have to print the number of contacts who have
a name starting with that partial name.

Input Format

The first line contains the integer N, the number of operations to be performed.
The next N lines contains one of the two operations defined above.

Constraints

1<=N<=105
1<=Length(name)<=21
1<=Length(partial)<=21

The entire input consists of lowercase characters only.

Output Format

For each operation of type find partial, print the number of contacts starting
with the string partial.

Sample Input

4
add hack
add hackerrank
find hac
find hak
Sample Output

2
0
Explanation

The names hack and hackerrank both start with the string hac. We have no name
starting with the string hak.
"""

# INPUTS = """add hack
# add hackerrank
# find hac
# find hak"""
# INPUTS = [line for line in INPUTS.split('\n')]
INPUTS = [raw_input() for _ in xrange(input())]

TRIE = {}
RESULTS = {}


def add_word(word):
    """Add words into trie."""
    obj = TRIE
    for char in word:
        if char not in obj:
            obj[char] = [1, {}]
        else:
            obj[char] = [obj[char][0] + 1, obj[char][1]]
        obj = obj[char][1]
    # print TRIE


def find(word):
    """Find word in trie."""
    obj = TRIE
    prev = [0]
    for char in word:
        if not obj.get(char, None):
            return 0
        prev = obj[char]
        obj = obj[char][1]
    return prev[0]

for inp in INPUTS:
    command, wrd = inp.split(' ')
    if command == 'add':
        add_word(wrd)
    else:
        if wrd not in RESULTS:
            RESULTS[wrd] = find(wrd)
        print RESULTS[wrd]

print TRIE

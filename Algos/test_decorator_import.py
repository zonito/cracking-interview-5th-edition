from python import decorator


@decorator.logged
def add(a, b):
    print(a + b)


# add(10, 20)
